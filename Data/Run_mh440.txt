ihixs results 
Result
mh                        = 4.40000000e+02
Etot                      = 13000
PDF set                   = PDF4LHC15_nnlo_100
PDF member                = 0
mur                       = 2.20000000e+02
muf                       = 2.20000000e+02
as_at_mz                  = 1.18002301e-01
as_at_mur                 = 1.04174617e-01
mt_used                   = 1.58995370e+02
mb_used                   = 2.66849498e+00
mc_used                   = 5.89498883e-01
eftlo                     = 9.99965436e-01 [9.72558577e-05]
eftnlo                    = 1.96400379e+00 [1.20559298e-04]
eftnnlo                   = 2.35465122e+00 [2.25360268e-04]
eftn3lo                   = 2.43188545e+00 [3.02664665e-04]
R_LO                      = 3.06237623e+00
R_LO*eftlo                = 3.06227038e+00 [2.97834027e-04]
R_LO*eftnlo               = 6.01451854e+00 [3.69197929e-04]
R_LO*eftnnlo              = 7.21082792e+00 [6.90137929e-04]
R_LO*eftn3lo              = 7.44734819e+00 [9.26873075e-04]
ggnlo/eftnlo              = 9.83547482e-01 [8.58263385e-05]
qgnlo/eftnlo              = 1.57429581e-02 [1.84141805e-06]
ggnnlo/eftn2lo            = 9.84741634e-01 [1.34140844e-04]
qgnnlo/eftn2lo            = 1.36248735e-02 [1.91408172e-06]
ggn3lo/eftn3lo            = 9.88547154e-01 [1.63079747e-04]
qgn3lo/eftn3lo            = 9.13764432e-03 [4.40884762e-06]
R_LO*gg channel           = 7.36205486e+00 [7.97189014e-04]
R_LO*qg channel           = 6.80512189e-02 [3.17230967e-05]
R_LO*qqbar channel        = 6.75810252e-03 [5.37939383e-05]
R_LO*qq channel           = 3.38462601e-03 [2.68135066e-05]
R_LO*q1q2 channel         = 7.09938651e-03 [4.67936179e-04]
ew rescaled as^2          = 6.46701844e-02 [6.28977165e-06]
ew rescaled as^3          = 5.89335730e-02 [4.60829372e-06]
ew rescaled as^4          = 2.17134750e-02 [1.23137417e-05]
ew rescaled as^5          = 3.43239193e-03 [1.30670723e-05]
mixed EW-QCD              = 8.40794399e-02 [2.05605311e-05]
ew rescaled               = 1.48749624e-01 [1.95748362e-05]
hard ratio from eft       = 3.22316592e-01 [3.98308661e-05]
WC                        = 1.10049105e+00 [0.00000000e+00]
WC^2                      = 1.21108055e+00 [0.00000000e+00]
WC^2_trunc                = 1.21090325e+00 [0.00000000e+00]
n2                        = 9.99965436e-01 [9.72558577e-05]
n3                        = 7.81665703e-01 [7.01322637e-05]
n4                        = 2.22165800e-01 [1.90180928e-04]
n5                        = 1.38515730e-02 [2.00539285e-04]
n                         = 2.01764851e+00 [3.01267367e-04]
sigma factorized          = 2.44353486e+00 [3.06337347e-04]
exact LO t+b+c            = 3.05426670e+00 [2.97055595e-04]
exact NLO t+b+c           = 5.77501534e+00 [5.12324579e-04]
exact LO t                = 3.06227038e+00 [2.97834027e-04]
exact NLO t               = 5.78448295e+00 [5.13268246e-04]
NLO quark mass effects    = -2.39503196e-01 [6.31493139e-04]
NLO quark mass effects / eft % = -3.98208426e+00
Higgs XS                  = 7.35659462e+00 [1.12172209e-03]
delta EW                  = 7.35659462e-02 [1.12172209e-05]
delta EW %                = 1.00000000e+00 [0.00000000e+00]
R_LO*eftnnlo (with NLO PDF) = 7.26861855e+00 [6.78036203e-04]
delta PDF-TH %            = 4.00721169e-01 [6.70865309e-03]
rEFT(low)                 = 7.60077564e+00 [9.42768460e-04]
rEFT(high)                = 6.84448726e+00 [1.16245277e-03]
delta(scale)+             = 1.53427446e-01
delta(scale)-             = -6.02860932e-01
delta(scale)+(%)          = 2.06016211e+00
delta(scale)-(%)          = -8.09497444e+00
delta(scale)+ pure eft    = 1.08574902e-02
delta(scale)- pure eft    = -7.17226670e-02
delta(scale)+(%) pure eft = 4.46463883e-01
delta(scale)-(%) pure eft = -2.94926174e+00
R_LO*eftn3lo_central      = 7.44749180e+00 [7.29645191e-04]
deltaPDF+                 = 1.53466882e-01
deltaPDF-                 = 1.53466882e-01
deltaPDFsymm              = 1.53466882e-01
deltaPDF+(%)              = 2.06065190e+00
deltaPDF-(%)              = 2.06065190e+00
rEFT(as+)                 = 7.62338446e+00 [9.52679697e-04]
rEFT(as-)                 = 7.27941167e+00 [8.88509054e-04]
delta(as)+                = 1.76036270e-01
delta(as)-                = -1.67936519e-01
delta(as)+(%)             = 2.36374432e+00
delta(as)-(%)             = -2.25498413e+00
Theory Uncertainty  +     = 2.54603153e-01 [4.95052929e-04]
Theory Uncertainty  -     = -6.98559832e-01 [5.04891876e-04]
Theory Uncertainty % +    = 3.46088328e+00 [6.70865309e-03]
Theory Uncertainty % -    = -9.49569561e+00 [6.70865309e-03]
delta(PDF+a_s) +          = 2.30691987e-01 [3.51755550e-05]
delta(PDF+a_s) -          = -2.24722469e-01 [-3.42653322e-05]
delta(PDF+a_s) + %        = 3.13585291e+00 [0.00000000e+00]
delta(PDF+a_s) - %        = -3.05470779e+00 [0.00000000e+00]
Total Uncertainty +       = 4.85295140e-01 [4.96301039e-04]
Total Uncertainty -       = -9.23282301e-01 [5.06053277e-04]
Total Uncertainty + %     = 6.59673619e+00 [6.70865309e-03]
Total Uncertainty - %     = -1.25504034e+01 [6.70865309e-03]

note: delta_tbc was requested (an estimate of the uncertainty due to the lack of light quark mass effects at NNLO) but either the with_mt_expansion is set to false or the qcd perturbative order is less than NNLO. In either case we cannot estimate delta_tbc, so it is set to zero




------------------------------------------------------------------
Results in Mathematica format

<|"mh"->4.40000000e+02,"Etot"->13000,"PDF set"->PDF4LHC15_nnlo_100,"PDF member"->0,"mur"->2.20000000e+02,"muf"->2.20000000e+02,"as_at_mz"->1.18002301e-01,"as_at_mur"->1.04174617e-01,"mt_used"->1.58995370e+02,"mb_used"->2.66849498e+00,"mc_used"->5.89498883e-01,"eftlo"->9.99965436e-01,"eftnlo"->1.96400379e+00,"eftnnlo"->2.35465122e+00,"eftn3lo"->2.43188545e+00,"R_LO"->3.06237623e+00,"R_LO*eftlo"->3.06227038e+00,"R_LO*eftnlo"->6.01451854e+00,"R_LO*eftnnlo"->7.21082792e+00,"R_LO*eftn3lo"->7.44734819e+00,"ggnlo/eftnlo"->9.83547482e-01,"qgnlo/eftnlo"->1.57429581e-02,"ggnnlo/eftn2lo"->9.84741634e-01,"qgnnlo/eftn2lo"->1.36248735e-02,"ggn3lo/eftn3lo"->9.88547154e-01,"qgn3lo/eftn3lo"->9.13764432e-03,"R_LO*gg channel"->7.36205486e+00,"R_LO*qg channel"->6.80512189e-02,"R_LO*qqbar channel"->6.75810252e-03,"R_LO*qq channel"->3.38462601e-03,"R_LO*q1q2 channel"->7.09938651e-03,"ew rescaled as^2"->6.46701844e-02,"ew rescaled as^3"->5.89335730e-02,"ew rescaled as^4"->2.17134750e-02,"ew rescaled as^5"->3.43239193e-03,"mixed EW-QCD"->8.40794399e-02,"ew rescaled"->1.48749624e-01,"hard ratio from eft"->3.22316592e-01,"WC"->1.10049105e+00,"WC^2"->1.21108055e+00,"WC^2_trunc"->1.21090325e+00,"n2"->9.99965436e-01,"n3"->7.81665703e-01,"n4"->2.22165800e-01,"n5"->1.38515730e-02,"n"->2.01764851e+00,"sigma factorized"->2.44353486e+00,"exact LO t+b+c"->3.05426670e+00,"exact NLO t+b+c"->5.77501534e+00,"exact LO t"->3.06227038e+00,"exact NLO t"->5.78448295e+00,"NLO quark mass effects"->-2.39503196e-01,"NLO quark mass effects / eft %"->-3.98208426e+00,"delta sigma t NLO"->2.72221256e+00,"delta sigma t+b+c NLO"->2.72074864e+00,"delta tbc ratio"->5.37770124e-04,"Higgs XS"->7.35659462e+00,"delta EW"->7.35659462e-02,"delta EW %"->1.00000000e+00,"R_LO*eftnnlo (with NLO PDF)"->7.26861855e+00,"delta PDF-TH %"->4.00721169e-01,"rEFT(low)"->7.60077564e+00,"rEFT(high)"->6.84448726e+00,"delta(scale)+"->1.53427446e-01,"delta(scale)-"->-6.02860932e-01,"delta(scale)+(%)"->2.06016211e+00,"delta(scale)-(%)"->-8.09497444e+00,"delta(scale)+ pure eft"->1.08574902e-02,"delta(scale)- pure eft"->-7.17226670e-02,"delta(scale)+(%) pure eft"->4.46463883e-01,"delta(scale)-(%) pure eft"->-2.94926174e+00,"R_LO*eftn3lo_central"->7.44749180e+00,"deltaPDF+"->1.53466882e-01,"deltaPDF-"->1.53466882e-01,"deltaPDFsymm"->1.53466882e-01,"deltaPDF+(%)"->2.06065190e+00,"deltaPDF-(%)"->2.06065190e+00,"rEFT(as+)"->7.62338446e+00,"rEFT(as-)"->7.27941167e+00,"delta(as)+"->1.76036270e-01,"delta(as)-"->-1.67936519e-01,"delta(as)+(%)"->2.36374432e+00,"delta(as)-(%)"->-2.25498413e+00,"Theory Uncertainty  +"->2.54603153e-01,"Theory Uncertainty  -"->-6.98559832e-01,"Theory Uncertainty % +"->3.46088328e+00,"Theory Uncertainty % -"->-9.49569561e+00,"delta(PDF+a_s) +"->2.30691987e-01,"delta(PDF+a_s) -"->-2.24722469e-01,"delta(PDF+a_s) + %"->3.13585291e+00,"delta(PDF+a_s) - %"->-3.05470779e+00,"Total Uncertainty +"->4.85295140e-01,"Total Uncertainty -"->-9.23282301e-01,"Total Uncertainty + %"->6.59673619e+00,"Total Uncertainty - %"->-1.25504034e+01|>


------------------------------------------------------------------
User defined options (including command line ones)
Etot                        = 13000.0               #    COM energy of the collider in GeV
m_higgs                     = 440                   #    higgs mass in GeV
mur                         = 220.                  #    mur
muf                         = 220.                  #    muf
pdf_member                  = 0                     #    pdf member id (the range depends on the pdf set)
pdf_set                     = PDF4LHC15_nnlo_100    #    choose a specific pdf set name (LHAPDF6 list at lhapdf.hepforge.org/pdfsets.html). This set will be used irrespectively of order.
pdf_set_for_nlo             = PDF4LHC15_nlo_100     #    pdf set used when computing PDF-TH error.
with_eft                    = true                  #    compute the cross section in the EFT approximation
with_exact_qcd_corrections  = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_ew_corrections         = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_mt_expansion           = false                 #    include NNLO 1/mt terms
with_delta_pdf_th           = true                  #    compute PDF-TH uncertainty
with_scale_variation        = true                  #    estimate scale variation (mur and muf should be at mh/2)
with_indiv_mass_effects     = false                 #    compute separately light quark contributions
with_pdf_error              = true                  #    whether or not to compute error due to pdfs
with_a_s_error              = true                  #    compute a_s uncertainty
with_resummation            = false                 #    include threshold resummation
resummation_log_order       = 3                     #    0:LL, 1:NLL, 2:NNLL, 3:N3LL
resummation_matching_order  = 3                     #    0:L0, 1:NL0, 2:NNL0, 3:N3L0
resummation_type            = log                   #    variant of threshold resummation, i.e. log:classical, psi, AP2log, AP2psi 
with_scet                   = false                 #    include scet resummation
verbose                     = minimal               #    level of verbosity: minimal or medium. Medium shows channel breakdown EFT cross section.
input_filename              = pheno.card            #    filename to use as runcard
output_filename             = mHRuns/Run_mh440.txt  #    filename to write output
help                        = false                 #    print all options and help messages per option.
make_runcard                = false                 #    create default runcard file as default_card.
make_pheno_card             = false                 #    create pheno runcard file as pheno_card.
write_documentation         = false                 #    print the help message in a TeX form.
with_eft_channel_info       = false                 #    print eft cross section per channel per order
with_resummation_info       = false                 #    info from resummation: true, false
mt_msbar                    = 162.7                 #    MSbar top mass
mt_msbar_ref_scale          = 162.7                 #    reference scale for the top mass in MSbar
mt_on_shell                 = 172.5                 #    On Shell top mass
mb_msbar                    = 4.18                  #    MSbar bottom mass
mb_msbar_ref_scale          = 4.18                  #    reference scale for the bottom mass in MSbar
mb_on_shell                 = 4.92                  #    On Shell bottom mass
mc_msbar                    = 0.986                 #    MSbar charm mass
mc_msbar_ref_scale          = 3.0                   #    reference scale for the charm mass in MSbar
mc_on_shell                 = 1.67                  #    On Shell charm mass
top_scheme                  = msbar                 #    msbar or on-shell
bottom_scheme               = msbar                 #    msbar or on-shell
charm_scheme                = msbar                 #    msbar or on-shell
y_top                       = 1.0                   #    factor multiplying the Yt. Set to zero to remove the top quark
y_bot                       = 1.0                   #    factor multiplying the Yb. Set to zero to remove the bottom quark
y_charm                     = 1.0                   #    factor multiplying the Yc. Set to zero to remove the charm quark
gamma_top                   = 0.0                   #    width of top quark
gamma_bot                   = 0.0                   #    width of bottom quark
gamma_charm                 = 0.0                   #    width of charm quark
qcd_perturbative_order      = N3LO                  #    LO, NLO, NNLO, N3LO : ihixs will compute up to this order in a_s
with_fixed_as_at_mz         = 0.0                   #    set the value of a_s(mZ) by hand. Beware: this might not be compatible with your pdf choice.
qcd_order_evol              = 3                     #    used for a_s and quark mass evolution 0:L0, 1:NL0, 2:NNL0, 3:N3L0
with_lower_ord_scale_var    = false                 #    also compute scale variation for lower than the current order
epsrel                      = 0.0001                #    cuba argument: target relative error
epsabs                      = 0.0                   #    cuba argument: target absolute error
mineval                     = 50000                 #    cuba argument: minimum points to be evaluated
maxeval                     = 50000000              #    cuba argument: maximum points to be evaluated
nstart                      = 10000                 #    cuba argument: number of points for first iteration
nincrease                   = 1000                  #    cuba argument: number of points for step increase
cuba_verbose                = 0                     #    cuba argument: verbosity level: 0=silent, 2=iterations printed out


------------------------------------------------------------------
Runcard used (options may have been overwritten by command line ones in runtime)
runcard_name="pheno.card"
