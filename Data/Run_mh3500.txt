ihixs results 
Result
mh                        = 3.50000000e+03
Etot                      = 13000
PDF set                   = PDF4LHC15_nnlo_100
PDF member                = 0
mur                       = 1.75000000e+03
muf                       = 1.75000000e+03
as_at_mz                  = 1.18002301e-01
as_at_mur                 = 8.17863038e-02
mt_used                   = 1.38925664e+02
mb_used                   = 2.33165525e+00
mc_used                   = 5.15087466e-01
eftlo                     = 1.46432788e-04 [1.42871079e-08]
eftnlo                    = 2.61393882e-04 [2.03813122e-08]
eftnnlo                   = 3.08090985e-04 [3.29253338e-08]
eftn3lo                   = 3.21066510e-04 [4.49256440e-08]
R_LO                      = 1.32918502e-02
R_LO*eftlo                = 1.94636269e-06 [1.89902098e-10]
R_LO*eftnlo               = 3.47440834e-06 [2.70905349e-10]
R_LO*eftnnlo              = 4.09509923e-06 [4.37638607e-10]
R_LO*eftn3lo              = 4.26756797e-06 [5.97144932e-10]
ggnlo/eftnlo              = 1.07490766e+00 [1.14205046e-04]
qgnlo/eftnlo              = -7.51482909e-02 [9.51356752e-06]
ggnnlo/eftn2lo            = 1.12488495e+00 [1.59837256e-04]
qgnnlo/eftn2lo            = -1.27267799e-01 [2.01683669e-05]
ggn3lo/eftn3lo            = 1.14968392e+00 [2.08078778e-04]
qgn3lo/eftn3lo            = -1.54170856e-01 [3.43052881e-05]
R_LO*gg channel           = 4.90635429e-06 [5.63210644e-10]
R_LO*qg channel           = -6.57934606e-07 [1.13831139e-10]
R_LO*qqbar channel        = 2.31541424e-09 [1.97840009e-11]
R_LO*qq channel           = 8.40352266e-09 [7.57883730e-11]
R_LO*q1q2 channel         = 8.42934869e-09 [1.42418498e-10]
ew rescaled as^2          = 0.00000000e+00 [0.00000000e+00]
ew rescaled as^3          = 0.00000000e+00 [0.00000000e+00]
ew rescaled as^4          = 0.00000000e+00 [0.00000000e+00]
ew rescaled as^5          = 0.00000000e+00 [0.00000000e+00]
mixed EW-QCD              = 0.00000000e+00 [0.00000000e+00]
ew rescaled               = 0.00000000e+00 [0.00000000e+00]
hard ratio from eft       = 1.93283486e-02 [2.33617143e-05]
WC                        = 1.08919385e+00 [0.00000000e+00]
WC^2                      = 1.18634324e+00 [0.00000000e+00]
WC^2_trunc                = 1.18551353e+00 [0.00000000e+00]
n2                        = 1.46432788e-04 [1.42871079e-08]
n3                        = 9.39943146e-05 [1.44631957e-08]
n4                        = 2.83964503e-05 [2.58157002e-08]
n5                        = 4.44515770e-06 [3.04517865e-08]
n                         = 2.73268711e-04 [4.48003031e-08]
sigma factorized          = 3.24190487e-04 [4.52833641e-08]
exact LO t+b+c            = 1.95089486e-06 [1.90344292e-10]
exact NLO t+b+c           = 4.01956777e-06 [5.52475540e-10]
exact LO t                = 1.94636269e-06 [1.89902098e-10]
exact NLO t               = 4.00800496e-06 [5.48114897e-10]
NLO quark mass effects    = 5.45159436e-07 [6.15320185e-10]
NLO quark mass effects / eft % = 1.56907128e+01
Higgs XS                  = 4.81272741e-06 [8.57438628e-10]
delta EW                  = 4.81272741e-08 [8.57438628e-12]
delta EW %                = 1.00000000e+00 [0.00000000e+00]
R_LO*eftnnlo (with NLO PDF) = 3.74218909e-06 [4.02675103e-10]
delta PDF-TH %            = 4.30893269e+00 [7.27577415e-03]
rEFT(low)                 = 4.46028642e-06 [7.00712200e-10]
rEFT(high)                = 3.67503291e-06 [5.82457820e-10]
delta(scale)+             = 1.92718443e-07
delta(scale)-             = -5.92535062e-07
delta(scale)+(%)          = 4.51588456e+00
delta(scale)-(%)          = -1.38846075e+01
delta(scale)+ pure eft    = 1.95181524e-06
delta(scale)- pure eft    = -1.15777322e-05
delta(scale)+(%) pure eft = 6.07916172e-01
delta(scale)-(%) pure eft = -3.60602301e+00
R_LO*eftn3lo_central      = 4.26781935e-06 [7.97383800e-10]
deltaPDF+                 = 8.46942761e-07
deltaPDF-                 = 8.46942761e-07
deltaPDFsymm              = 8.46942761e-07
deltaPDF+(%)              = 1.98448597e+01
deltaPDF-(%)              = 1.98448597e+01
rEFT(as+)                 = 4.60504470e-06 [6.38792808e-10]
rEFT(as-)                 = 4.04488547e-06 [5.61349151e-10]
delta(as)+                = 3.37476728e-07
delta(as)-                = -2.22682500e-07
delta(as)+(%)             = 7.90794031e+00
delta(as)-(%)             = -5.21801882e+00
Theory Uncertainty  +     = 4.72841672e-07 [3.60154033e-10]
Theory Uncertainty  -     = -9.23732768e-07 [3.86908860e-10]
Theory Uncertainty % +    = 9.82481724e+00 [7.27577415e-03]
Theory Uncertainty % -    = -1.91935402e+01 [7.27577415e-03]
delta(PDF+a_s) +          = 1.02811615e-06 [1.83169839e-10]
delta(PDF+a_s) -          = -9.87543256e-07 [-1.75941345e-10]
delta(PDF+a_s) + %        = 2.13624431e+01 [0.00000000e+00]
delta(PDF+a_s) - %        = -2.05194097e+01 [0.00000000e+00]
Total Uncertainty +       = 1.50095783e-06 [4.04057072e-10]
Total Uncertainty -       = -1.91127602e-06 [4.25033908e-10]
Total Uncertainty + %     = 3.11872603e+01 [7.27577415e-03]
Total Uncertainty - %     = -3.97129499e+01 [7.27577415e-03]

note: delta_tbc was requested (an estimate of the uncertainty due to the lack of light quark mass effects at NNLO) but either the with_mt_expansion is set to false or the qcd perturbative order is less than NNLO. In either case we cannot estimate delta_tbc, so it is set to zero




------------------------------------------------------------------
Results in Mathematica format

<|"mh"->3.50000000e+03,"Etot"->13000,"PDF set"->PDF4LHC15_nnlo_100,"PDF member"->0,"mur"->1.75000000e+03,"muf"->1.75000000e+03,"as_at_mz"->1.18002301e-01,"as_at_mur"->8.17863038e-02,"mt_used"->1.38925664e+02,"mb_used"->2.33165525e+00,"mc_used"->5.15087466e-01,"eftlo"->1.46432788e-04,"eftnlo"->2.61393882e-04,"eftnnlo"->3.08090985e-04,"eftn3lo"->3.21066510e-04,"R_LO"->1.32918502e-02,"R_LO*eftlo"->1.94636269e-06,"R_LO*eftnlo"->3.47440834e-06,"R_LO*eftnnlo"->4.09509923e-06,"R_LO*eftn3lo"->4.26756797e-06,"ggnlo/eftnlo"->1.07490766e+00,"qgnlo/eftnlo"->-7.51482909e-02,"ggnnlo/eftn2lo"->1.12488495e+00,"qgnnlo/eftn2lo"->-1.27267799e-01,"ggn3lo/eftn3lo"->1.14968392e+00,"qgn3lo/eftn3lo"->-1.54170856e-01,"R_LO*gg channel"->4.90635429e-06,"R_LO*qg channel"->-6.57934606e-07,"R_LO*qqbar channel"->2.31541424e-09,"R_LO*qq channel"->8.40352266e-09,"R_LO*q1q2 channel"->8.42934869e-09,"ew rescaled as^2"->0.00000000e+00,"ew rescaled as^3"->0.00000000e+00,"ew rescaled as^4"->0.00000000e+00,"ew rescaled as^5"->0.00000000e+00,"mixed EW-QCD"->0.00000000e+00,"ew rescaled"->0.00000000e+00,"hard ratio from eft"->1.93283486e-02,"WC"->1.08919385e+00,"WC^2"->1.18634324e+00,"WC^2_trunc"->1.18551353e+00,"n2"->1.46432788e-04,"n3"->9.39943146e-05,"n4"->2.83964503e-05,"n5"->4.44515770e-06,"n"->2.73268711e-04,"sigma factorized"->3.24190487e-04,"exact LO t+b+c"->1.95089486e-06,"exact NLO t+b+c"->4.01956777e-06,"exact LO t"->1.94636269e-06,"exact NLO t"->4.00800496e-06,"NLO quark mass effects"->5.45159436e-07,"NLO quark mass effects / eft %"->1.56907128e+01,"delta sigma t NLO"->2.06164227e-06,"delta sigma t+b+c NLO"->2.06867291e-06,"delta tbc ratio"->3.41021473e-03,"Higgs XS"->4.81272741e-06,"delta EW"->4.81272741e-08,"delta EW %"->1.00000000e+00,"R_LO*eftnnlo (with NLO PDF)"->3.74218909e-06,"delta PDF-TH %"->4.30893269e+00,"rEFT(low)"->4.46028642e-06,"rEFT(high)"->3.67503291e-06,"delta(scale)+"->1.92718443e-07,"delta(scale)-"->-5.92535062e-07,"delta(scale)+(%)"->4.51588456e+00,"delta(scale)-(%)"->-1.38846075e+01,"delta(scale)+ pure eft"->1.95181524e-06,"delta(scale)- pure eft"->-1.15777322e-05,"delta(scale)+(%) pure eft"->6.07916172e-01,"delta(scale)-(%) pure eft"->-3.60602301e+00,"R_LO*eftn3lo_central"->4.26781935e-06,"deltaPDF+"->8.46942761e-07,"deltaPDF-"->8.46942761e-07,"deltaPDFsymm"->8.46942761e-07,"deltaPDF+(%)"->1.98448597e+01,"deltaPDF-(%)"->1.98448597e+01,"rEFT(as+)"->4.60504470e-06,"rEFT(as-)"->4.04488547e-06,"delta(as)+"->3.37476728e-07,"delta(as)-"->-2.22682500e-07,"delta(as)+(%)"->7.90794031e+00,"delta(as)-(%)"->-5.21801882e+00,"Theory Uncertainty  +"->4.72841672e-07,"Theory Uncertainty  -"->-9.23732768e-07,"Theory Uncertainty % +"->9.82481724e+00,"Theory Uncertainty % -"->-1.91935402e+01,"delta(PDF+a_s) +"->1.02811615e-06,"delta(PDF+a_s) -"->-9.87543256e-07,"delta(PDF+a_s) + %"->2.13624431e+01,"delta(PDF+a_s) - %"->-2.05194097e+01,"Total Uncertainty +"->1.50095783e-06,"Total Uncertainty -"->-1.91127602e-06,"Total Uncertainty + %"->3.11872603e+01,"Total Uncertainty - %"->-3.97129499e+01|>


------------------------------------------------------------------
User defined options (including command line ones)
Etot                        = 13000.0               #    COM energy of the collider in GeV
m_higgs                     = 3500                  #    higgs mass in GeV
mur                         = 1750.                 #    mur
muf                         = 1750.                 #    muf
pdf_member                  = 0                     #    pdf member id (the range depends on the pdf set)
pdf_set                     = PDF4LHC15_nnlo_100    #    choose a specific pdf set name (LHAPDF6 list at lhapdf.hepforge.org/pdfsets.html). This set will be used irrespectively of order.
pdf_set_for_nlo             = PDF4LHC15_nlo_100     #    pdf set used when computing PDF-TH error.
with_eft                    = true                  #    compute the cross section in the EFT approximation
with_exact_qcd_corrections  = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_ew_corrections         = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_mt_expansion           = false                 #    include NNLO 1/mt terms
with_delta_pdf_th           = true                  #    compute PDF-TH uncertainty
with_scale_variation        = true                  #    estimate scale variation (mur and muf should be at mh/2)
with_indiv_mass_effects     = false                 #    compute separately light quark contributions
with_pdf_error              = true                  #    whether or not to compute error due to pdfs
with_a_s_error              = true                  #    compute a_s uncertainty
with_resummation            = false                 #    include threshold resummation
resummation_log_order       = 3                     #    0:LL, 1:NLL, 2:NNLL, 3:N3LL
resummation_matching_order  = 3                     #    0:L0, 1:NL0, 2:NNL0, 3:N3L0
resummation_type            = log                   #    variant of threshold resummation, i.e. log:classical, psi, AP2log, AP2psi 
with_scet                   = false                 #    include scet resummation
verbose                     = minimal               #    level of verbosity: minimal or medium. Medium shows channel breakdown EFT cross section.
input_filename              = pheno.card            #    filename to use as runcard
output_filename             = mHRuns/Run_mh3500.txt #    filename to write output
help                        = false                 #    print all options and help messages per option.
make_runcard                = false                 #    create default runcard file as default_card.
make_pheno_card             = false                 #    create pheno runcard file as pheno_card.
write_documentation         = false                 #    print the help message in a TeX form.
with_eft_channel_info       = false                 #    print eft cross section per channel per order
with_resummation_info       = false                 #    info from resummation: true, false
mt_msbar                    = 162.7                 #    MSbar top mass
mt_msbar_ref_scale          = 162.7                 #    reference scale for the top mass in MSbar
mt_on_shell                 = 172.5                 #    On Shell top mass
mb_msbar                    = 4.18                  #    MSbar bottom mass
mb_msbar_ref_scale          = 4.18                  #    reference scale for the bottom mass in MSbar
mb_on_shell                 = 4.92                  #    On Shell bottom mass
mc_msbar                    = 0.986                 #    MSbar charm mass
mc_msbar_ref_scale          = 3.0                   #    reference scale for the charm mass in MSbar
mc_on_shell                 = 1.67                  #    On Shell charm mass
top_scheme                  = msbar                 #    msbar or on-shell
bottom_scheme               = msbar                 #    msbar or on-shell
charm_scheme                = msbar                 #    msbar or on-shell
y_top                       = 1.0                   #    factor multiplying the Yt. Set to zero to remove the top quark
y_bot                       = 1.0                   #    factor multiplying the Yb. Set to zero to remove the bottom quark
y_charm                     = 1.0                   #    factor multiplying the Yc. Set to zero to remove the charm quark
gamma_top                   = 0.0                   #    width of top quark
gamma_bot                   = 0.0                   #    width of bottom quark
gamma_charm                 = 0.0                   #    width of charm quark
qcd_perturbative_order      = N3LO                  #    LO, NLO, NNLO, N3LO : ihixs will compute up to this order in a_s
with_fixed_as_at_mz         = 0.0                   #    set the value of a_s(mZ) by hand. Beware: this might not be compatible with your pdf choice.
qcd_order_evol              = 3                     #    used for a_s and quark mass evolution 0:L0, 1:NL0, 2:NNL0, 3:N3L0
with_lower_ord_scale_var    = false                 #    also compute scale variation for lower than the current order
epsrel                      = 0.0001                #    cuba argument: target relative error
epsabs                      = 0.0                   #    cuba argument: target absolute error
mineval                     = 50000                 #    cuba argument: minimum points to be evaluated
maxeval                     = 50000000              #    cuba argument: maximum points to be evaluated
nstart                      = 10000                 #    cuba argument: number of points for first iteration
nincrease                   = 1000                  #    cuba argument: number of points for step increase
cuba_verbose                = 0                     #    cuba argument: verbosity level: 0=silent, 2=iterations printed out


------------------------------------------------------------------
Runcard used (options may have been overwritten by command line ones in runtime)
runcard_name="pheno.card"
