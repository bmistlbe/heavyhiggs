\section{Introduction}
In this note we briefly summarise inclusive cross section predictions for the production of a CP even scalar boson out of the fusion of two initial state gluons. 
This summary is relying on state of the art predictions for the Standard Model (SM) Higgs boson. 
The aim is to provide a concise collection of cross section predictions serving as flexible reference for broad searches of a heavy CP even scalar.
For more details we refer the reader for example to refs.~\cite{deFlorian:2016spz,Anastasiou:2016hlm,Harlander:2016hcx,Brooijmans:2016vro}

In order to provide numerical predictions for this study we use the following inputs
\begin{table}[h!]
\centering
\begin{tabular}{| c | c |}
\hline 
$m_h$ & 125.09 GeV \\
$m_t$ & 162.7 GeV \\
$m_b$ & 4.18 GeV \\
$m_c$ & 0.986 GeV \\
$m_Z$ & 91.1876 GeV \\
PDF & \texttt{PDF4LHC15\_nnlo\_100}~\cite{Butterworth:2015oua} \\
v & 246.221 GeV \\
$\alpha_S$ & 0.118 \\
\hline
\end{tabular}
\end{table}
Masses are taken from the PDG~\cite{Tanabashi:2018oca} and YR4~\cite{deFlorian:2016spz} and are evaluated in the $\overline{\text{MS}}$ scheme.
We make the choice 
\beq
\label{eq:scales}
\mu_F^{\text{cent.}}=\mu_R^{\text{cent.}}=\frac{m_S}{2}.
\eeq
Here, $m_S$ is the mass of the heavy scalar boson, $\mu_F^{\text{cent.}}$ is the factorisation and $\mu_R^{\text{cent.}}$ the renormalisation scale.
Throughout we assume that the heavy scalar that is produced has a narrow width,
\beq
\frac{\Gamma_S}{m_S}<<1.
\eeq


\section{Production Mechanisms}

\subsection{Production via a virtual top quark loop}
 \begin{figure*}[!h]
 \begin{center}
\includegraphics[scale=.4]{./Figures/ggF.eps}\hspace{3cm}
\includegraphics[scale=.4]{./Figures/ggF-tb.eps}
\end{center}
\caption{\label{fig:ggF} 
Feynman diagrams for the LO production cross section of a scalar boson via a quark loop. 
Doubled lines represent the heavy boson. The dashed line separates the interfered amplitude and complex conjugate amplitude.
The interference of two top quark loops is shown on the left and the interference of a top and a bottom quark loop on the right.
}
\end{figure*}
Figure~\ref{fig:ggF} shows example Feynman diagrams for the main production mechanism of the SM Higgs boson at the LHC.
The predominant contribution to the so-called gluon fusion cross section arises from virtual top quark loops. 
Sub-leading contributions are given by virtual bottom or charm quark loops. 
We decompose the cross section according to its quark-loop content
\beq
\label{eq:ggfdec}
\sigma_{SM}=y_t^2 \sigma_{tt}+y_t y_b \sigma_{tb}+y_t y_c \sigma_{tc}.
\eeq
Here, $y_i$ are modification factors of the SM Yukawa couplings of quarks to the Higgs boson, i.e. in the SM we have $y_i=1$.
This cross section is currently known through Next-to-leading order (NLO)~\cite{Graudenz:1992pv,Spira:1995rr}.
We neglect further sub-leading contributions, like terms proportional to $y_b^2$.

In section~\ref{sec:predictions} we present predictions for the individual terms in eq.~\eqref{eq:ggfdec}.
In this predictions we assume that the heavy scalar is identical to the SM Higgs boson but the mass of the boson is varied.


\subsection{Production via a dim. 5 effective theory}

 \begin{figure*}[!h]
 \begin{center}
\includegraphics[scale=.4]{./Figures/EFT.eps}
\end{center}
\caption{\label{fig:EFT} 
Feynman diagrams for the LO production cross section of a scalar boson via a dimension five operator. 
Doubled lines represent the heavy boson. The dashed line separates the interfered amplitude and complex conjugate amplitude.
}
\end{figure*}
Figure~\ref{fig:EFT} shows a Feynman diagram for the leading order production cross section of a scalar boson via the fusion of two gluons mediated by an effective operator~\cite{Inami1983,Shifman1978,Spiridonov:1988md,Wilczek1977}.
This effective operator couples the scalar boson directly to gluons. 
The lowest dimensional effective interaction is given by the following dimension five operator.
\beq
\label{eq:eftLagr}
\mathcal{L}_{eff.}=-\frac{1}{4v} C_t S (G_{\mu\nu} )^2.
\eeq
Here, S is the heavy boson, G the field strength tensor of gluons and $C_S$ is a Wilson coefficient.
In the SM Higgs boson it is possible to relate gluon fusion cross section via a virtual top quark loop to this effective theory by integrating out the degrees of freedom of the top quark.
This leads to the same effective interaction with $S\rightarrow H$ and the Wilson coefficient $C_H$~\cite{Chetyrkin:1997un,Schroder:2005hy,Chetyrkin:2005ia,Kramer:1996iq}.
Essentially this effective theory amounts to performing an expansion of the SM cross section in the parameter
\beq
\tau=\frac{m_h^2}{4m_t^2}
\eeq
and maintaining only the first term. 
Clearly, this effective theory is only valid as long as the expansion parameter is smaller than one.
\beq
m_h< 2 m_t.
\eeq
One advantage of this effective theory is the reduction of complexity in computing cross sections, as can be simply seen from the structure of Feynman diagrams in fig.~\ref{fig:ggF} and fig.~\ref{fig:EFT}.
As a consequence, we currently have predictions at Next-to-next-to-next-to-leading order (N$^3$LO)~\cite{Anastasiou:2015ema,Mistlberger:2018etf} in QCD available for the effective theory cross section $\sigma_{EFF}$.
Sub-leading terms in the expansion in $\tau$ of the SM cross section at NNLO were computed in refs.~\cite{Harlander:2009mq,Pak:2009dg}.

For heavy scalar cross sections the effective theory predictions have the advantage that they do not rely on the underlying interactions of a heavy scalar with the top quark. 
This computation is valid whenever new physics can be represented by an effective theory as in eq.~\eqref{eq:eftLagr}. 
Only the value of the Wilson coefficient needs to be adjusted in order to fit the new physics model under investigation. 
This was explored for example in ref.~\cite{Anastasiou:2016hlm}. 
An example could be a theory with new, heavy, colour charged fermions that couple to the heavy scalar and where the effective theory is the result of integrating out the heavy fermions.
Once the Wilson coefficient for this scenario is computed cross section predictions can simply be made using the effective theory computation of SM like scalar boson.
\beq
\sigma_{S,EFF}=\left|\frac{C_S}{C_t}\right|^2\sigma_{EFF}.
\eeq



\subsection{Improved predictions for production via a virtual top quark loop }

It was observed that for a Standard Model Higgs boson the perturbative corrections to the production cross section in the effective theory and in the full SM including the top quark are very similar in their relative size.
Commonly, this observation is used to estimate the size of perturbative corrections to the full SM cross section.
We refer to the ratio of cross sections predicted at order $i$ and order $j$ as K-factor
\beq
K_{ij}=\frac{\sigma^{N^{i}LO}}{\sigma^{N^jLO}}.
\eeq

 \begin{figure*}[!h]
 \begin{center}
\includegraphics[width=.45\textwidth]{./Plots/KFacRatio.pdf}
\includegraphics[width=.45\textwidth]{./Plots/KEff31.pdf}
\end{center}
\caption{\label{fig:KFacs}
The left panel shows the ratio of the K-factor $K_{10}$ based on full QCD predictions over the same K-factor computed with the effective theory as a function of the mass of the scalar boson.. 
The right panel shows the K-factor $K_{31}^{EFF}$ computed in the effective theory as a function of the mass of the scalar boson.
} 
\end{figure*}
Figure~\ref{fig:KFacs}  shows on the left the ratio of the K-factor in the full standard model with massive top quarks $K_{10}^{t}$  and in the effective theory $K_{10}^{EFF}$ as a function of the Higgs boson mass.
We can see that the K-factors stay within $\sim 15\%$ of each other even though there nominal value is around 2. 
This observation can be understood from the fact that some contributions to the Higgs boson cross sections that are in part responsible for the large perturbative corrections factorise from the exact structure of the interaction of the Higgs with gluons.

The effective theory for a SM like Higgs boson coupling to the top quark loses its validity as the Higgs mass reaches the level of $m_h\sim 2 m_t$. 
However, it is possible to assume that perturbative corrections to the full SM predictions will continue to follow a similar perturbative pattern as the one observed for the effective theory cross section in this regime.
In Fig.~\ref{fig:KFacs} on the right we study the size of perturbative corrections in the effective theory on beyond NLO by showing the ration $K^{EFF}_{31}$. 
This $K_{31}^{EFF}$ can be multiplied with NLO cross sections based on the full SM with massive top quarks in order to achieve improved predictions.
However, in order to quantify differences in perturbative corrections between the full SM and the effective theory we recommend to assign an additional $15\%$ uncertainty to $K_{31}^{EFF}$ for $m_h>\sim m_t$.

\subsection{Electro-Weak Corrections }
 \begin{figure*}[!h]
 \begin{center}
\includegraphics[scale=.4]{./Figures/EWK.eps}
\end{center}
\caption{\label{fig:EWK} 
Feynman diagrams for the LO electroweak corrections to the  production cross section of a scalar boson.
Doubled lines represent the heavy boson. 
The dashed line separates the interfered amplitude and complex conjugate amplitude. 
Here, top quark loop diagrams are interfered with electro-weak gauge boson loop diagrams.
}
\end{figure*}
Figure~\ref{fig:EWK} shows a Feynman diagram for the leading electroweak corrections to the production cross section of a scalar boson via the fusion of two gluons.
This corrections are typically included in precision predictions for the SM Higgs boson and are of the order of  five percent~\cite{Aglietti:2004nj,Actis:2008ug,Actis:2008ts,Anastasiou:2008tj,Bonetti:2018ukf,Anastasiou:2018adr}. 
In this note we derive predictions for a heavy scalar boson using the same formulae and and that the couplings of the heavy boson are identical to ones of the SM Higgs boson. 
We only consider the leading order electro-weak corrections with light quarks in the loop on the right hand side of the diagram in fig.~\ref{fig:EWK}, $\sigma_{EWK}$.
It was observed that electroweak corrections are affected by higher order QCD corrections in a similar fashion as the effective theory cross section.
Consequently, we multiply the leading order prediction with $K_{30}^{EFF}$ in order to capture higher order effects. 
In summary we may denote the cross section for electro-weak production mechanism by
\beq
\sigma_{S,\, EWK}=y_t \lambda_{EWK} K_{30}^{EFF}\sigma_{EWK}.
\eeq 
Here, $\lambda_{EWK}$ is a factor modifying the coupling of the heavy scalar to electro-weak gauge bosons relativ to the SM Higgs boson couplings.

\subsection{Summary}
In this note we present predictions for the production cross section of a CP-even scalar boson with mass $m_S$ through different production mechanism. 
The cross section can be summarised as 
\beq
\sigma_S=\left|\frac{C_S}{C_H}\right|^2\sigma_{EFF} +y_t^2 K_{31}^{EFF} \sigma_{tt}+y_t y_b \sigma_{tb}+y_t y_c \sigma_{tc} +y_t \lambda_{EWK} K_{30}^{EFF}\sigma_{EWK}.
\eeq
We provide explicit predictions for the above contributions in the following sections.
Which cross sections should be considered in a search for a heavy scalar boson depends on the assumptions of that search.
For example, if we assume that the heavy scalar boson only couples to the SM via the top quark via a Yukawa interaction all other couplings should be set to zero:
\beq
C_S=y_b=y_c=\lambda_{EWK}=0,
\eeq
and $y_t$ is the ratio of the Yukawa coupling of the scalar to the the Yukawa coupling of the Higgs boson to the top quark.
However, it is not the objective of this note to discuss the optimal input for a search for a heavy scalar or the interpretation of such a search in a particular theory.
This note simply provides the necessary cross section predictions for such searches.






