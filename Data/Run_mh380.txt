ihixs results 
Result
mh                        = 3.80000000e+02
Etot                      = 13000
PDF set                   = PDF4LHC15_nnlo_100
PDF member                = 0
mur                       = 1.90000000e+02
muf                       = 1.90000000e+02
as_at_mz                  = 1.18002301e-01
as_at_mur                 = 1.06242057e-01
mt_used                   = 1.60763565e+02
mb_used                   = 2.69817151e+00
mc_used                   = 5.96054737e-01
eftlo                     = 1.45524240e+00 [1.37039241e-04]
eftnlo                    = 2.89899980e+00 [1.71695774e-04]
eftnnlo                   = 3.48967586e+00 [3.32684821e-04]
eftn3lo                   = 3.60398170e+00 [4.60572931e-04]
R_LO                      = 3.27888031e+00
R_LO*eftlo                = 4.77156565e+00 [4.49335270e-04]
R_LO*eftnlo               = 9.50547338e+00 [5.62969893e-04]
R_LO*eftnnlo              = 1.14422295e+01 [1.09083371e-03]
R_LO*eftn3lo              = 1.18170246e+01 [1.51016352e-03]
ggnlo/eftnlo              = 9.80478274e-01 [8.26525879e-05]
qgnlo/eftnlo              = 1.88186884e-02 [2.18489279e-06]
ggnnlo/eftn2lo            = 9.80232563e-01 [1.33294681e-04]
qgnnlo/eftn2lo            = 1.80555867e-02 [2.33601304e-06]
ggn3lo/eftn3lo            = 9.83611919e-01 [1.64747303e-04]
qgn3lo/eftn3lo            = 1.39537627e-02 [4.46668641e-06]
R_LO*gg channel           = 1.16233663e+01 [1.25843647e-03]
R_LO*qg channel           = 1.64891957e-01 [4.83941154e-05]
R_LO*qqbar channel        = 1.06801299e-02 [9.03318287e-05]
R_LO*qq channel           = 5.69141922e-03 [4.61198290e-05]
R_LO*q1q2 channel         = 1.23948509e-02 [8.27225871e-04]
ew rescaled as^2          = 2.53364443e-01 [2.38591667e-05]
ew rescaled as^3          = 2.37622991e-01 [1.80102575e-05]
ew rescaled as^4          = 8.84075700e-02 [4.96119015e-05]
ew rescaled as^5          = 1.36387802e-02 [5.54551555e-05]
mixed EW-QCD              = 3.39669341e-01 [8.36630390e-05]
ew rescaled               = 5.93033784e-01 [8.01888038e-05]
hard ratio from eft       = 3.38026903e-01 [4.08796516e-05]
WC                        = 1.10143994e+00 [0.00000000e+00]
WC^2                      = 1.21316994e+00 [0.00000000e+00]
WC^2_trunc                = 1.21304042e+00 [0.00000000e+00]
n2                        = 1.45524240e+00 [1.37039241e-04]
n3                        = 1.17308459e+00 [1.01856951e-04]
n4                        = 3.36243191e-01 [2.84633808e-04]
n5                        = 1.94392653e-02 [3.16298379e-04]
n                         = 2.98400945e+00 [4.58492815e-04]
sigma factorized          = 3.62011056e+00 [4.66497052e-04]
exact LO t+b+c            = 4.74534824e+00 [4.46866393e-04]
exact NLO t+b+c           = 8.95851967e+00 [7.95389311e-04]
exact LO t                = 4.77156565e+00 [4.49335270e-04]
exact NLO t               = 9.00043711e+00 [7.97626615e-04]
NLO quark mass effects    = -5.46953709e-01 [9.74463573e-04]
NLO quark mass effects / eft % = -5.75409228e+00
Higgs XS                  = 1.18631047e+01 [1.79905624e-03]
delta EW                  = 1.18631047e-01 [1.79905624e-05]
delta EW %                = 1.00000000e+00 [0.00000000e+00]
R_LO*eftnnlo (with NLO PDF) = 1.15521389e+01 [1.11750201e-03]
delta PDF-TH %            = 4.80279884e-01 [6.82418655e-03]
rEFT(low)                 = 1.17808437e+01 [1.47477088e-03]
rEFT(high)                = 1.14811102e+01 [1.96852357e-03]
delta(scale)+             = -3.61809724e-02
delta(scale)-             = -3.35914474e-01
delta(scale)+(%)          = -3.06176669e-01
delta(scale)-(%)          = -2.84263157e+00
delta(scale)+ pure eft    = 1.61398781e-02
delta(scale)- pure eft    = -1.06787783e-01
delta(scale)+(%) pure eft = 4.47834629e-01
delta(scale)-(%) pure eft = -2.96305008e+00
R_LO*eftn3lo_central      = 1.18167945e+01 [1.14782159e-03]
deltaPDF+                 = 2.27708559e-01
deltaPDF-                 = 2.27708559e-01
deltaPDFsymm              = 2.27708559e-01
deltaPDF+(%)              = 1.92699094e+00
deltaPDF-(%)              = 1.92699094e+00
rEFT(as+)                 = 1.20970262e+01 [1.57495943e-03]
rEFT(as-)                 = 1.15451682e+01 [1.45011206e-03]
delta(as)+                = 2.80001604e-01
delta(as)-                = -2.71856449e-01
delta(as)+(%)             = 2.36947635e+00
delta(as)-(%)             = -2.30054906e+00
Theory Uncertainty  +     = 1.39285094e-01 [8.09835915e-04]
Theory Uncertainty  -     = -5.12831513e-01 [8.13287440e-04]
Theory Uncertainty % +    = 1.17410321e+00 [6.82418655e-03]
Theory Uncertainty % -    = -4.32291146e+00 [6.82418655e-03]
delta(PDF+a_s) +          = 3.62314683e-01 [5.49455228e-05]
delta(PDF+a_s) -          = -3.56008196e-01 [-5.39891354e-05]
delta(PDF+a_s) + %        = 3.05413036e+00 [0.00000000e+00]
delta(PDF+a_s) - %        = -3.00096985e+00 [0.00000000e+00]
Total Uncertainty +       = 5.01599777e-01 [8.11697739e-04]
Total Uncertainty -       = -8.68839710e-01 [8.15077473e-04]
Total Uncertainty + %     = 4.22823358e+00 [6.82418655e-03]
Total Uncertainty - %     = -7.32388131e+00 [6.82418655e-03]

note: delta_tbc was requested (an estimate of the uncertainty due to the lack of light quark mass effects at NNLO) but either the with_mt_expansion is set to false or the qcd perturbative order is less than NNLO. In either case we cannot estimate delta_tbc, so it is set to zero




------------------------------------------------------------------
Results in Mathematica format

<|"mh"->3.80000000e+02,"Etot"->13000,"PDF set"->PDF4LHC15_nnlo_100,"PDF member"->0,"mur"->1.90000000e+02,"muf"->1.90000000e+02,"as_at_mz"->1.18002301e-01,"as_at_mur"->1.06242057e-01,"mt_used"->1.60763565e+02,"mb_used"->2.69817151e+00,"mc_used"->5.96054737e-01,"eftlo"->1.45524240e+00,"eftnlo"->2.89899980e+00,"eftnnlo"->3.48967586e+00,"eftn3lo"->3.60398170e+00,"R_LO"->3.27888031e+00,"R_LO*eftlo"->4.77156565e+00,"R_LO*eftnlo"->9.50547338e+00,"R_LO*eftnnlo"->1.14422295e+01,"R_LO*eftn3lo"->1.18170246e+01,"ggnlo/eftnlo"->9.80478274e-01,"qgnlo/eftnlo"->1.88186884e-02,"ggnnlo/eftn2lo"->9.80232563e-01,"qgnnlo/eftn2lo"->1.80555867e-02,"ggn3lo/eftn3lo"->9.83611919e-01,"qgn3lo/eftn3lo"->1.39537627e-02,"R_LO*gg channel"->1.16233663e+01,"R_LO*qg channel"->1.64891957e-01,"R_LO*qqbar channel"->1.06801299e-02,"R_LO*qq channel"->5.69141922e-03,"R_LO*q1q2 channel"->1.23948509e-02,"ew rescaled as^2"->2.53364443e-01,"ew rescaled as^3"->2.37622991e-01,"ew rescaled as^4"->8.84075700e-02,"ew rescaled as^5"->1.36387802e-02,"mixed EW-QCD"->3.39669341e-01,"ew rescaled"->5.93033784e-01,"hard ratio from eft"->3.38026903e-01,"WC"->1.10143994e+00,"WC^2"->1.21316994e+00,"WC^2_trunc"->1.21304042e+00,"n2"->1.45524240e+00,"n3"->1.17308459e+00,"n4"->3.36243191e-01,"n5"->1.94392653e-02,"n"->2.98400945e+00,"sigma factorized"->3.62011056e+00,"exact LO t+b+c"->4.74534824e+00,"exact NLO t+b+c"->8.95851967e+00,"exact LO t"->4.77156565e+00,"exact NLO t"->9.00043711e+00,"NLO quark mass effects"->-5.46953709e-01,"NLO quark mass effects / eft %"->-5.75409228e+00,"delta sigma t NLO"->4.22887146e+00,"delta sigma t+b+c NLO"->4.21317143e+00,"delta tbc ratio"->3.71258095e-03,"Higgs XS"->1.18631047e+01,"delta EW"->1.18631047e-01,"delta EW %"->1.00000000e+00,"R_LO*eftnnlo (with NLO PDF)"->1.15521389e+01,"delta PDF-TH %"->4.80279884e-01,"rEFT(low)"->1.17808437e+01,"rEFT(high)"->1.14811102e+01,"delta(scale)+"->-3.61809724e-02,"delta(scale)-"->-3.35914474e-01,"delta(scale)+(%)"->-3.06176669e-01,"delta(scale)-(%)"->-2.84263157e+00,"delta(scale)+ pure eft"->1.61398781e-02,"delta(scale)- pure eft"->-1.06787783e-01,"delta(scale)+(%) pure eft"->4.47834629e-01,"delta(scale)-(%) pure eft"->-2.96305008e+00,"R_LO*eftn3lo_central"->1.18167945e+01,"deltaPDF+"->2.27708559e-01,"deltaPDF-"->2.27708559e-01,"deltaPDFsymm"->2.27708559e-01,"deltaPDF+(%)"->1.92699094e+00,"deltaPDF-(%)"->1.92699094e+00,"rEFT(as+)"->1.20970262e+01,"rEFT(as-)"->1.15451682e+01,"delta(as)+"->2.80001604e-01,"delta(as)-"->-2.71856449e-01,"delta(as)+(%)"->2.36947635e+00,"delta(as)-(%)"->-2.30054906e+00,"Theory Uncertainty  +"->1.39285094e-01,"Theory Uncertainty  -"->-5.12831513e-01,"Theory Uncertainty % +"->1.17410321e+00,"Theory Uncertainty % -"->-4.32291146e+00,"delta(PDF+a_s) +"->3.62314683e-01,"delta(PDF+a_s) -"->-3.56008196e-01,"delta(PDF+a_s) + %"->3.05413036e+00,"delta(PDF+a_s) - %"->-3.00096985e+00,"Total Uncertainty +"->5.01599777e-01,"Total Uncertainty -"->-8.68839710e-01,"Total Uncertainty + %"->4.22823358e+00,"Total Uncertainty - %"->-7.32388131e+00|>


------------------------------------------------------------------
User defined options (including command line ones)
Etot                        = 13000.0               #    COM energy of the collider in GeV
m_higgs                     = 380                   #    higgs mass in GeV
mur                         = 190.                  #    mur
muf                         = 190.                  #    muf
pdf_member                  = 0                     #    pdf member id (the range depends on the pdf set)
pdf_set                     = PDF4LHC15_nnlo_100    #    choose a specific pdf set name (LHAPDF6 list at lhapdf.hepforge.org/pdfsets.html). This set will be used irrespectively of order.
pdf_set_for_nlo             = PDF4LHC15_nlo_100     #    pdf set used when computing PDF-TH error.
with_eft                    = true                  #    compute the cross section in the EFT approximation
with_exact_qcd_corrections  = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_ew_corrections         = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_mt_expansion           = false                 #    include NNLO 1/mt terms
with_delta_pdf_th           = true                  #    compute PDF-TH uncertainty
with_scale_variation        = true                  #    estimate scale variation (mur and muf should be at mh/2)
with_indiv_mass_effects     = false                 #    compute separately light quark contributions
with_pdf_error              = true                  #    whether or not to compute error due to pdfs
with_a_s_error              = true                  #    compute a_s uncertainty
with_resummation            = false                 #    include threshold resummation
resummation_log_order       = 3                     #    0:LL, 1:NLL, 2:NNLL, 3:N3LL
resummation_matching_order  = 3                     #    0:L0, 1:NL0, 2:NNL0, 3:N3L0
resummation_type            = log                   #    variant of threshold resummation, i.e. log:classical, psi, AP2log, AP2psi 
with_scet                   = false                 #    include scet resummation
verbose                     = minimal               #    level of verbosity: minimal or medium. Medium shows channel breakdown EFT cross section.
input_filename              = pheno.card            #    filename to use as runcard
output_filename             = mHRuns/Run_mh380.txt  #    filename to write output
help                        = false                 #    print all options and help messages per option.
make_runcard                = false                 #    create default runcard file as default_card.
make_pheno_card             = false                 #    create pheno runcard file as pheno_card.
write_documentation         = false                 #    print the help message in a TeX form.
with_eft_channel_info       = false                 #    print eft cross section per channel per order
with_resummation_info       = false                 #    info from resummation: true, false
mt_msbar                    = 162.7                 #    MSbar top mass
mt_msbar_ref_scale          = 162.7                 #    reference scale for the top mass in MSbar
mt_on_shell                 = 172.5                 #    On Shell top mass
mb_msbar                    = 4.18                  #    MSbar bottom mass
mb_msbar_ref_scale          = 4.18                  #    reference scale for the bottom mass in MSbar
mb_on_shell                 = 4.92                  #    On Shell bottom mass
mc_msbar                    = 0.986                 #    MSbar charm mass
mc_msbar_ref_scale          = 3.0                   #    reference scale for the charm mass in MSbar
mc_on_shell                 = 1.67                  #    On Shell charm mass
top_scheme                  = msbar                 #    msbar or on-shell
bottom_scheme               = msbar                 #    msbar or on-shell
charm_scheme                = msbar                 #    msbar or on-shell
y_top                       = 1.0                   #    factor multiplying the Yt. Set to zero to remove the top quark
y_bot                       = 1.0                   #    factor multiplying the Yb. Set to zero to remove the bottom quark
y_charm                     = 1.0                   #    factor multiplying the Yc. Set to zero to remove the charm quark
gamma_top                   = 0.0                   #    width of top quark
gamma_bot                   = 0.0                   #    width of bottom quark
gamma_charm                 = 0.0                   #    width of charm quark
qcd_perturbative_order      = N3LO                  #    LO, NLO, NNLO, N3LO : ihixs will compute up to this order in a_s
with_fixed_as_at_mz         = 0.0                   #    set the value of a_s(mZ) by hand. Beware: this might not be compatible with your pdf choice.
qcd_order_evol              = 3                     #    used for a_s and quark mass evolution 0:L0, 1:NL0, 2:NNL0, 3:N3L0
with_lower_ord_scale_var    = false                 #    also compute scale variation for lower than the current order
epsrel                      = 0.0001                #    cuba argument: target relative error
epsabs                      = 0.0                   #    cuba argument: target absolute error
mineval                     = 50000                 #    cuba argument: minimum points to be evaluated
maxeval                     = 50000000              #    cuba argument: maximum points to be evaluated
nstart                      = 10000                 #    cuba argument: number of points for first iteration
nincrease                   = 1000                  #    cuba argument: number of points for step increase
cuba_verbose                = 0                     #    cuba argument: verbosity level: 0=silent, 2=iterations printed out


------------------------------------------------------------------
Runcard used (options may have been overwritten by command line ones in runtime)
runcard_name="pheno.card"
