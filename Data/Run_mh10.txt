ihixs results 
Result
mh                        = 10.000000
Etot                      = 13000
PDF set                   = PDF4LHC15_nnlo_100
PDF member                = 0
mur                       = 5.000000
muf                       = 5.000000
as_at_mz                  = 0.118002
as_at_mur                 = 0.213131
mt_used                   = 241.199144
mb_used                   = 4.048175
mc_used                   = 0.894285
eftlo                     = 176.572537 [0.017439]
eftnlo                    = 939.397411 [0.053233]
eftnnlo                   = 1760.876398 [0.366428]
eftn3lo                   = 1927.157199 [2.699861]
R_LO                      = 1.000201
R_LO*eftlo                = 176.607955 [0.017442]
R_LO*eftnlo               = 939.585838 [0.053244]
R_LO*eftnnlo              = 1761.229599 [0.366501]
R_LO*eftn3lo              = 1927.543754 [2.700403]
ggnlo/eftnlo              = 0.886290 [0.000074]
qgnlo/eftnlo              = 0.112776 [0.000013]
ggnnlo/eftn2lo            = 0.801627 [0.000260]
qgnnlo/eftn2lo            = 0.185140 [0.000047]
ggn3lo/eftn3lo            = 0.778822 [0.001108]
qgn3lo/eftn3lo            = 0.200269 [0.000282]
R_LO*gg channel           = 1501.213580 [0.368552]
R_LO*qg channel           = 386.027066 [0.048075]
R_LO*qqbar channel        = 6.799060 [0.126694]
R_LO*qq channel           = 8.819266 [0.070465]
R_LO*q1q2 channel         = 24.684782 [2.670771]
ew rescaled as^2          = 0.000000 [0.000000]
ew rescaled as^3          = 0.000000 [0.000000]
ew rescaled as^4          = 0.000000 [0.000000]
ew rescaled as^5          = 0.000000 [0.000000]
mixed EW-QCD              = 0.000000 [0.000000]
ew rescaled               = 0.000000 [0.000000]
hard ratio from eft       = 0.746618 [0.000080]
WC                        = 1.187176 [0.000000]
WC^2                      = 1.409386 [0.000000]
WC^2_trunc                = 1.381725 [0.000000]
n                         = 1461.044315 [2.698125]
sigma factorized          = 2059.175381 [2.915554]
exact LO t+b+c            = 1357.755560 [0.134097]
exact NLO t+b+c           = 5373.127713 [1.095235]
exact LO t                = 176.607955 [0.017442]
exact NLO t               = 938.194674 [0.253329]
NLO quark mass effects    = 4433.541876 [1.096529]
NLO quark mass effects / eft % = 471.861292
NNLO mt exp gg            = -199.250925 [0.350131]
NNLO mt exp qg            = -144.434757 [0.045758]
NNLO top mass effects     = -343.685681 [0.353108]
Higgs XS                  = 6017.399948 [2.935853]
delta_tbc                 = 2654.622119 [3.116739]
delta_tbc %               = 44.115767 [0.056090]
delta(1/m_t)              = 60.173999 [0.029359]
delta(1/m_t) %            = 1.000000 [0.000000]
delta EW                  = 60.173999 [0.029359]
delta EW %                = 1.000000 [0.000000]
R_LO*eftnnlo (with NLO PDF) = 2015.125227 [0.410828]
delta PDF-TH %            = 7.207908 [0.015701]
rEFT(low)                 = 1963.464489 [2.893646]
rEFT(high)                = 1759.583684 [1.838133]
delta(scale)+             = 35.920736
delta(scale)-             = -167.960070
delta(scale)+(%)          = 1.863550
delta(scale)-(%)          = -8.713684
delta(scale)+ pure eft    = 35.943991
delta(scale)- pure eft    = -168.010857
delta(scale)+(%) pure eft = 1.865130
delta(scale)-(%) pure eft = -8.718067
R_LO*eftn3lo_central      = 1932.874089 [0.475575]
deltaPDF+                 = 202.239710
deltaPDF-                 = 202.239710
deltaPDFsymm              = 202.239710
deltaPDF+(%)              = 10.463160
deltaPDF-(%)              = 10.463160
rEFT(as+)                 = 2046.741450 [3.065640]
rEFT(as-)                 = 1868.379858 [2.368968]
delta(as)+                = 119.197697
delta(as)-                = -59.163896
delta(as)+(%)             = 6.183917
delta(as)-(%)             = -3.069393
Theory Uncertainty  +     = 3320.836025 [3.861259]
Theory Uncertainty  -     = -3733.035998 [3.949867]
Theory Uncertainty % +    = 55.187225 [0.058246]
Theory Uncertainty % -    = -62.037359 [0.058246]
delta(PDF+a_s) +          = 731.351886 [0.356822]
delta(PDF+a_s) -          = -656.141919 [-0.320128]
delta(PDF+a_s) + %        = 12.153952 [0.000000]
delta(PDF+a_s) - %        = -10.904077 [0.000000]
Total Uncertainty +       = 4052.187911 [3.877711]
Total Uncertainty -       = -4389.177917 [3.962819]
Total Uncertainty + %     = 67.341176 [0.058246]
Total Uncertainty - %     = -72.941436 [0.058246]





------------------------------------------------------------------
Results in Mathematica format

<|"mh"->10.000000,"Etot"->13000,"PDF set"->PDF4LHC15_nnlo_100,"PDF member"->0,"mur"->5.000000,"muf"->5.000000,"as_at_mz"->0.118002,"as_at_mur"->0.213131,"mt_used"->241.199144,"mb_used"->4.048175,"mc_used"->0.894285,"eftlo"->176.572537,"eftnlo"->939.397411,"eftnnlo"->1760.876398,"eftn3lo"->1927.157199,"R_LO"->1.000201,"R_LO*eftlo"->176.607955,"R_LO*eftnlo"->939.585838,"R_LO*eftnnlo"->1761.229599,"R_LO*eftn3lo"->1927.543754,"ggnlo/eftnlo"->0.886290,"qgnlo/eftnlo"->0.112776,"ggnnlo/eftn2lo"->0.801627,"qgnnlo/eftn2lo"->0.185140,"ggn3lo/eftn3lo"->0.778822,"qgn3lo/eftn3lo"->0.200269,"gg at order 4"->579.102714,"qg at order 4"->220.110574,"R_LO*gg channel"->1501.213580,"R_LO*qg channel"->386.027066,"R_LO*qqbar channel"->6.799060,"R_LO*qq channel"->8.819266,"R_LO*q1q2 channel"->24.684782,"ew rescaled as^2"->0.000000,"ew rescaled as^3"->0.000000,"ew rescaled as^4"->0.000000,"ew rescaled as^5"->0.000000,"mixed EW-QCD"->0.000000,"ew rescaled"->0.000000,"hard ratio from eft"->0.746618,"WC"->1.187176,"WC^2"->1.409386,"WC^2_trunc"->1.381725,"n"->1461.044315,"sigma factorized"->2059.175381,"exact LO t+b+c"->1357.755560,"exact NLO t+b+c"->5373.127713,"exact LO t"->176.607955,"exact NLO t"->938.194674,"NLO quark mass effects"->4433.541876,"NLO quark mass effects / eft %"->471.861292,"delta sigma t NLO"->761.586719,"delta sigma t+b+c NLO"->4015.372153,"delta tbc ratio"->4.272377,"NNLO mt exp gg"->-199.250925,"NNLO mt exp qg"->-144.434757,"NNLO top mass effects"->-343.685681,"Higgs XS"->6017.399948,"delta_tbc"->2654.622119,"delta_tbc %"->44.115767,"delta(1/m_t)"->60.173999,"delta(1/m_t) % "->1.000000,"delta EW"->60.173999,"delta EW %"->1.000000,"R_LO*eftnnlo (with NLO PDF)"->2015.125227,"delta PDF-TH %"->7.207908,"rEFT(low)"->1963.464489,"rEFT(high)"->1759.583684,"delta(scale)+"->35.920736,"delta(scale)-"->-167.960070,"delta(scale)+(%)"->1.863550,"delta(scale)-(%)"->-8.713684,"delta(scale)+ pure eft"->35.943991,"delta(scale)- pure eft"->-168.010857,"delta(scale)+(%) pure eft"->1.865130,"delta(scale)-(%) pure eft"->-8.718067,"R_LO*eftn3lo_central"->1932.874089,"deltaPDF+"->202.239710,"deltaPDF-"->202.239710,"deltaPDFsymm"->202.239710,"deltaPDF+(%)"->10.463160,"deltaPDF-(%)"->10.463160,"rEFT(as+)"->2046.741450,"rEFT(as-)"->1868.379858,"delta(as)+"->119.197697,"delta(as)-"->-59.163896,"delta(as)+(%)"->6.183917,"delta(as)-(%)"->-3.069393,"Theory Uncertainty  +"->3320.836025,"Theory Uncertainty  -"->-3733.035998,"Theory Uncertainty % +"->55.187225,"Theory Uncertainty % -"->-62.037359,"delta(PDF+a_s) +"->731.351886,"delta(PDF+a_s) -"->-656.141919,"delta(PDF+a_s) + %"->12.153952,"delta(PDF+a_s) - %"->-10.904077,"Total Uncertainty +"->4052.187911,"Total Uncertainty -"->-4389.177917,"Total Uncertainty + %"->67.341176,"Total Uncertainty - %"->-72.941436|>


------------------------------------------------------------------
User defined options (including command line ones)
Etot                        = 13000.0               #    COM energy of the collider in GeV
m_higgs                     = 10                    #    higgs mass in GeV
mur                         = 5.                    #    mur
muf                         = 5.                    #    muf
pdf_member                  = 0                     #    pdf member id (the range depends on the pdf set)
pdf_set                     = PDF4LHC15_nnlo_100    #    choose a specific pdf set name (LHAPDF6 list at lhapdf.hepforge.org/pdfsets.html). This set will be used irrespectively of order.
pdf_set_for_nlo             = PDF4LHC15_nlo_100     #    pdf set used when computing PDF-TH error.
with_eft                    = true                  #    compute the cross section in the EFT approximation
with_exact_qcd_corrections  = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_ew_corrections         = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_mt_expansion           = true                  #    include NNLO 1/mt terms
with_delta_pdf_th           = true                  #    compute PDF-TH uncertainty
with_scale_variation        = true                  #    estimate scale variation (mur and muf should be at mh/2)
with_indiv_mass_effects     = false                 #    compute separately light quark contributions
with_pdf_error              = true                  #    whether or not to compute error due to pdfs
with_a_s_error              = true                  #    compute a_s uncertainty
with_resummation            = false                 #    include threshold resummation
resummation_log_order       = 3                     #    0:LL, 1:NLL, 2:NNLL, 3:N3LL
resummation_matching_order  = 3                     #    0:L0, 1:NL0, 2:NNL0, 3:N3L0
resummation_type            = log                   #    variant of threshold resummation, i.e. log:classical, psi, AP2log, AP2psi 
with_scet                   = false                 #    include scet resummation
verbose                     = minimal               #    level of verbosity: minimal or medium. Medium shows channel breakdown EFT cross section.
input_filename              = pheno.card            #    filename to use as runcard
output_filename             = mHRuns/Run_mh10.txt   #    filename to write output
help                        = false                 #    print all options and help messages per option.
make_runcard                = false                 #    create default runcard file as default_card.
make_pheno_card             = false                 #    create pheno runcard file as pheno_card.
write_documentation         = false                 #    print the help message in a TeX form.
with_eft_channel_info       = false                 #    print eft cross section per channel per order
with_resummation_info       = false                 #    info from resummation: true, false
mt_msbar                    = 162.7                 #    MSbar top mass
mt_msbar_ref_scale          = 162.7                 #    reference scale for the top mass in MSbar
mt_on_shell                 = 172.5                 #    On Shell top mass
mb_msbar                    = 4.18                  #    MSbar bottom mass
mb_msbar_ref_scale          = 4.18                  #    reference scale for the bottom mass in MSbar
mb_on_shell                 = 4.92                  #    On Shell bottom mass
mc_msbar                    = 0.986                 #    MSbar charm mass
mc_msbar_ref_scale          = 3.0                   #    reference scale for the charm mass in MSbar
mc_on_shell                 = 1.67                  #    On Shell charm mass
top_scheme                  = msbar                 #    msbar or on-shell
bottom_scheme               = msbar                 #    msbar or on-shell
charm_scheme                = msbar                 #    msbar or on-shell
y_top                       = 1.0                   #    factor multiplying the Yt. Set to zero to remove the top quark
y_bot                       = 1.0                   #    factor multiplying the Yb. Set to zero to remove the bottom quark
y_charm                     = 1.0                   #    factor multiplying the Yc. Set to zero to remove the charm quark
gamma_top                   = 0.0                   #    width of top quark
gamma_bot                   = 0.0                   #    width of bottom quark
gamma_charm                 = 0.0                   #    width of charm quark
qcd_perturbative_order      = N3LO                  #    LO, NLO, NNLO, N3LO : ihixs will compute up to this order in a_s
with_fixed_as_at_mz         = 0.0                   #    set the value of a_s(mZ) by hand. Beware: this might not be compatible with your pdf choice.
qcd_order_evol              = 3                     #    used for a_s and quark mass evolution 0:L0, 1:NL0, 2:NNL0, 3:N3L0
with_lower_ord_scale_var    = false                 #    also compute scale variation for lower than the current order
epsrel                      = 0.0001                #    cuba argument: target relative error
epsabs                      = 0.0                   #    cuba argument: target absolute error
mineval                     = 50000                 #    cuba argument: minimum points to be evaluated
maxeval                     = 50000000              #    cuba argument: maximum points to be evaluated
nstart                      = 10000                 #    cuba argument: number of points for first iteration
nincrease                   = 1000                  #    cuba argument: number of points for step increase
cuba_verbose                = 0                     #    cuba argument: verbosity level: 0=silent, 2=iterations printed out


------------------------------------------------------------------
Runcard used (options may have been overwritten by command line ones in runtime)
runcard_name="pheno.card"
