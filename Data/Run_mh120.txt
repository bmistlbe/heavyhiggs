ihixs results 
Result
mh                        = 120.000000
Etot                      = 13000
PDF set                   = PDF4LHC15_nnlo_100
PDF member                = 0
mur                       = 60.000000
muf                       = 60.000000
as_at_mz                  = 0.118002
as_at_mur                 = 0.125988
mt_used                   = 177.081247
mb_used                   = 2.972040
mc_used                   = 0.656555
eftlo                     = 16.162483 [0.001541]
eftnlo                    = 37.468101 [0.002034]
eftnnlo                   = 47.300236 [0.004883]
eftn3lo                   = 48.958710 [0.012330]
R_LO                      = 1.057038
R_LO*eftlo                = 17.084353 [0.001629]
R_LO*eftnlo               = 39.605192 [0.002150]
R_LO*eftnnlo              = 49.998128 [0.005162]
R_LO*eftn3lo              = 51.751198 [0.013034]
ggnlo/eftnlo              = 0.958484 [0.000075]
qgnlo/eftnlo              = 0.040864 [0.000005]
ggnnlo/eftn2lo            = 0.946167 [0.000142]
qgnnlo/eftn2lo            = 0.051121 [0.000007]
ggn3lo/eftn3lo            = 0.946490 [0.000263]
qgn3lo/eftn3lo            = 0.049557 [0.000013]
R_LO*gg channel           = 48.981991 [0.005801]
R_LO*qg channel           = 2.564622 [0.000259]
R_LO*qqbar channel        = 0.050639 [0.000514]
R_LO*qq channel           = 0.038840 [0.000294]
R_LO*q1q2 channel         = 0.115106 [0.011654]
ew rescaled as^2          = 0.858696 [0.000082]
ew rescaled as^3          = 1.076752 [0.000071]
ew rescaled as^4          = 0.455779 [0.000236]
ew rescaled as^5          = 0.061832 [0.000602]
mixed EW-QCD              = 1.594364 [0.000660]
ew rescaled               = 2.453059 [0.000655]
hard ratio from eft       = 0.456663 [0.000049]
WC                        = 1.110410 [0.000000]
WC^2                      = 1.233010 [0.000000]
WC^2_trunc                = 1.232973 [0.000000]
n                         = 39.803428 [0.012307]
sigma factorized          = 49.078009 [0.012603]
exact LO t+b+c            = 15.759979 [0.001503]
exact NLO t+b+c           = 37.307261 [0.005063]
exact LO t                = 17.084353 [0.001629]
exact NLO t               = 39.348295 [0.004886]
NLO quark mass effects    = -2.297930 [0.005500]
NLO quark mass effects / eft % = -5.802094
NNLO mt exp gg            = 0.431014 [0.004869]
NNLO mt exp qg            = -0.017303 [0.000173]
NNLO top mass effects     = 0.413711 [0.004872]
Higgs XS                  = 52.320038 [0.014977]
delta_tbc                 = 0.452215 [0.004666]
delta_tbc %               = 0.864326 [0.008922]
delta(1/m_t)              = 0.523200 [0.000150]
delta(1/m_t) %            = 1.000000 [0.000000]
delta EW                  = 0.523200 [0.000150]
delta EW %                = 1.000000 [0.000000]
R_LO*eftnnlo (with NLO PDF) = 51.186940 [0.005234]
delta PDF-TH %            = 1.188857 [0.007353]
rEFT(low)                 = 51.910074 [0.013756]
rEFT(high)                = 50.413180 [0.018675]
delta(scale)+             = 0.158876
delta(scale)-             = -1.338018
delta(scale)+(%)          = 0.307000
delta(scale)-(%)          = -2.585482
delta(scale)+ pure eft    = 0.266817
delta(scale)- pure eft    = -1.617769
delta(scale)+(%) pure eft = 0.544984
delta(scale)-(%) pure eft = -3.304354
R_LO*eftn3lo_central      = 51.754491 [0.005568]
deltaPDF+                 = 0.964358
deltaPDF-                 = 0.964358
deltaPDFsymm              = 0.964358
deltaPDF+(%)              = 1.863331
deltaPDF-(%)              = 1.863331
rEFT(as+)                 = 53.105318 [0.013908]
rEFT(as-)                 = 50.385267 [0.011771]
delta(as)+                = 1.354120
delta(as)-                = -1.365930
delta(as)+(%)             = 2.616597
delta(as)-(%)             = -2.639418
Theory Uncertainty  +     = 2.281249 [0.006084]
Theory Uncertainty  -     = -3.473352 [0.006130]
Theory Uncertainty % +    = 4.360182 [0.011561]
Theory Uncertainty % -    = -6.638664 [0.011561]
delta(PDF+a_s) +          = 1.680653 [0.000481]
delta(PDF+a_s) -          = -1.690393 [-0.000484]
delta(PDF+a_s) + %        = 3.212255 [0.000000]
delta(PDF+a_s) - %        = -3.230872 [0.000000]
Total Uncertainty +       = 3.961902 [0.006103]
Total Uncertainty -       = -5.163745 [0.006149]
Total Uncertainty + %     = 7.572437 [0.011561]
Total Uncertainty - %     = -9.869536 [0.011561]





------------------------------------------------------------------
Results in Mathematica format

<|"mh"->120.000000,"Etot"->13000,"PDF set"->PDF4LHC15_nnlo_100,"PDF member"->0,"mur"->60.000000,"muf"->60.000000,"as_at_mz"->0.118002,"as_at_mur"->0.125988,"mt_used"->177.081247,"mb_used"->2.972040,"mc_used"->0.656555,"eftlo"->16.162483,"eftnlo"->37.468101,"eftnnlo"->47.300236,"eftn3lo"->48.958710,"R_LO"->1.057038,"R_LO*eftlo"->17.084353,"R_LO*eftnlo"->39.605192,"R_LO*eftnnlo"->49.998128,"R_LO*eftn3lo"->51.751198,"ggnlo/eftnlo"->0.958484,"qgnlo/eftnlo"->0.040864,"ggnnlo/eftn2lo"->0.946167,"qgnnlo/eftn2lo"->0.051121,"ggn3lo/eftn3lo"->0.946490,"qgn3lo/eftn3lo"->0.049557,"gg at order 4"->9.345643,"qg at order 4"->0.937562,"R_LO*gg channel"->48.981991,"R_LO*qg channel"->2.564622,"R_LO*qqbar channel"->0.050639,"R_LO*qq channel"->0.038840,"R_LO*q1q2 channel"->0.115106,"ew rescaled as^2"->0.858696,"ew rescaled as^3"->1.076752,"ew rescaled as^4"->0.455779,"ew rescaled as^5"->0.061832,"mixed EW-QCD"->1.594364,"ew rescaled"->2.453059,"hard ratio from eft"->0.456663,"WC"->1.110410,"WC^2"->1.233010,"WC^2_trunc"->1.232973,"n"->39.803428,"sigma factorized"->49.078009,"exact LO t+b+c"->15.759979,"exact NLO t+b+c"->37.307261,"exact LO t"->17.084353,"exact NLO t"->39.348295,"NLO quark mass effects"->-2.297930,"NLO quark mass effects / eft %"->-5.802094,"delta sigma t NLO"->22.263942,"delta sigma t+b+c NLO"->21.547282,"delta tbc ratio"->0.032189,"NNLO mt exp gg"->0.431014,"NNLO mt exp qg"->-0.017303,"NNLO top mass effects"->0.413711,"Higgs XS"->52.320038,"delta_tbc"->0.452215,"delta_tbc %"->0.864326,"delta(1/m_t)"->0.523200,"delta(1/m_t) % "->1.000000,"delta EW"->0.523200,"delta EW %"->1.000000,"R_LO*eftnnlo (with NLO PDF)"->51.186940,"delta PDF-TH %"->1.188857,"rEFT(low)"->51.910074,"rEFT(high)"->50.413180,"delta(scale)+"->0.158876,"delta(scale)-"->-1.338018,"delta(scale)+(%)"->0.307000,"delta(scale)-(%)"->-2.585482,"delta(scale)+ pure eft"->0.266817,"delta(scale)- pure eft"->-1.617769,"delta(scale)+(%) pure eft"->0.544984,"delta(scale)-(%) pure eft"->-3.304354,"R_LO*eftn3lo_central"->51.754491,"deltaPDF+"->0.964358,"deltaPDF-"->0.964358,"deltaPDFsymm"->0.964358,"deltaPDF+(%)"->1.863331,"deltaPDF-(%)"->1.863331,"rEFT(as+)"->53.105318,"rEFT(as-)"->50.385267,"delta(as)+"->1.354120,"delta(as)-"->-1.365930,"delta(as)+(%)"->2.616597,"delta(as)-(%)"->-2.639418,"Theory Uncertainty  +"->2.281249,"Theory Uncertainty  -"->-3.473352,"Theory Uncertainty % +"->4.360182,"Theory Uncertainty % -"->-6.638664,"delta(PDF+a_s) +"->1.680653,"delta(PDF+a_s) -"->-1.690393,"delta(PDF+a_s) + %"->3.212255,"delta(PDF+a_s) - %"->-3.230872,"Total Uncertainty +"->3.961902,"Total Uncertainty -"->-5.163745,"Total Uncertainty + %"->7.572437,"Total Uncertainty - %"->-9.869536|>


------------------------------------------------------------------
User defined options (including command line ones)
Etot                        = 13000.0               #    COM energy of the collider in GeV
m_higgs                     = 120                   #    higgs mass in GeV
mur                         = 60.                   #    mur
muf                         = 60.                   #    muf
pdf_member                  = 0                     #    pdf member id (the range depends on the pdf set)
pdf_set                     = PDF4LHC15_nnlo_100    #    choose a specific pdf set name (LHAPDF6 list at lhapdf.hepforge.org/pdfsets.html). This set will be used irrespectively of order.
pdf_set_for_nlo             = PDF4LHC15_nlo_100     #    pdf set used when computing PDF-TH error.
with_eft                    = true                  #    compute the cross section in the EFT approximation
with_exact_qcd_corrections  = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_ew_corrections         = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_mt_expansion           = true                  #    include NNLO 1/mt terms
with_delta_pdf_th           = true                  #    compute PDF-TH uncertainty
with_scale_variation        = true                  #    estimate scale variation (mur and muf should be at mh/2)
with_indiv_mass_effects     = false                 #    compute separately light quark contributions
with_pdf_error              = true                  #    whether or not to compute error due to pdfs
with_a_s_error              = true                  #    compute a_s uncertainty
with_resummation            = false                 #    include threshold resummation
resummation_log_order       = 3                     #    0:LL, 1:NLL, 2:NNLL, 3:N3LL
resummation_matching_order  = 3                     #    0:L0, 1:NL0, 2:NNL0, 3:N3L0
resummation_type            = log                   #    variant of threshold resummation, i.e. log:classical, psi, AP2log, AP2psi 
with_scet                   = false                 #    include scet resummation
verbose                     = minimal               #    level of verbosity: minimal or medium. Medium shows channel breakdown EFT cross section.
input_filename              = pheno.card            #    filename to use as runcard
output_filename             = mHRuns/Run_mh120.txt  #    filename to write output
help                        = false                 #    print all options and help messages per option.
make_runcard                = false                 #    create default runcard file as default_card.
make_pheno_card             = false                 #    create pheno runcard file as pheno_card.
write_documentation         = false                 #    print the help message in a TeX form.
with_eft_channel_info       = false                 #    print eft cross section per channel per order
with_resummation_info       = false                 #    info from resummation: true, false
mt_msbar                    = 162.7                 #    MSbar top mass
mt_msbar_ref_scale          = 162.7                 #    reference scale for the top mass in MSbar
mt_on_shell                 = 172.5                 #    On Shell top mass
mb_msbar                    = 4.18                  #    MSbar bottom mass
mb_msbar_ref_scale          = 4.18                  #    reference scale for the bottom mass in MSbar
mb_on_shell                 = 4.92                  #    On Shell bottom mass
mc_msbar                    = 0.986                 #    MSbar charm mass
mc_msbar_ref_scale          = 3.0                   #    reference scale for the charm mass in MSbar
mc_on_shell                 = 1.67                  #    On Shell charm mass
top_scheme                  = msbar                 #    msbar or on-shell
bottom_scheme               = msbar                 #    msbar or on-shell
charm_scheme                = msbar                 #    msbar or on-shell
y_top                       = 1.0                   #    factor multiplying the Yt. Set to zero to remove the top quark
y_bot                       = 1.0                   #    factor multiplying the Yb. Set to zero to remove the bottom quark
y_charm                     = 1.0                   #    factor multiplying the Yc. Set to zero to remove the charm quark
gamma_top                   = 0.0                   #    width of top quark
gamma_bot                   = 0.0                   #    width of bottom quark
gamma_charm                 = 0.0                   #    width of charm quark
qcd_perturbative_order      = N3LO                  #    LO, NLO, NNLO, N3LO : ihixs will compute up to this order in a_s
with_fixed_as_at_mz         = 0.0                   #    set the value of a_s(mZ) by hand. Beware: this might not be compatible with your pdf choice.
qcd_order_evol              = 3                     #    used for a_s and quark mass evolution 0:L0, 1:NL0, 2:NNL0, 3:N3L0
with_lower_ord_scale_var    = false                 #    also compute scale variation for lower than the current order
epsrel                      = 0.0001                #    cuba argument: target relative error
epsabs                      = 0.0                   #    cuba argument: target absolute error
mineval                     = 50000                 #    cuba argument: minimum points to be evaluated
maxeval                     = 50000000              #    cuba argument: maximum points to be evaluated
nstart                      = 10000                 #    cuba argument: number of points for first iteration
nincrease                   = 1000                  #    cuba argument: number of points for step increase
cuba_verbose                = 0                     #    cuba argument: verbosity level: 0=silent, 2=iterations printed out


------------------------------------------------------------------
Runcard used (options may have been overwritten by command line ones in runtime)
runcard_name="pheno.card"
