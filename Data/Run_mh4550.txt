ihixs results 
Result
mh                        = 4.55000000e+03
Etot                      = 13000
PDF set                   = PDF4LHC15_nnlo_100
PDF member                = 0
mur                       = 2.27500000e+03
muf                       = 2.27500000e+03
as_at_mz                  = 1.18002301e-01
as_at_mur                 = 7.96314296e-02
mt_used                   = 1.36889608e+02
mb_used                   = 2.29748317e+00
mc_used                   = 5.07538497e-01
eftlo                     = 1.61421938e-05 [1.51231812e-09]
eftnlo                    = 2.91402439e-05 [2.35843974e-09]
eftnnlo                   = 3.46552674e-05 [3.96096772e-09]
eftn3lo                   = 3.62986441e-05 [5.69575924e-09]
R_LO                      = 5.81264226e-03
R_LO*eftlo                = 9.38287978e-08 [8.79056421e-12]
R_LO*eftnlo               = 1.69381813e-07 [1.37087665e-11]
R_LO*eftnnlo              = 2.01438672e-07 [2.30236883e-11]
R_LO*eftn3lo              = 2.10991032e-07 [3.31074108e-11]
ggnlo/eftnlo              = 1.10225901e+00 [1.20019811e-04]
qgnlo/eftnlo              = -1.02398862e-01 [1.30703329e-05]
ggnnlo/eftn2lo            = 1.17123966e+00 [1.73426057e-04]
qgnnlo/eftn2lo            = -1.75205569e-01 [2.90476275e-05]
ggn3lo/eftn3lo            = 1.20667315e+00 [2.37844946e-04]
qgn3lo/eftn3lo            = -2.14412899e-01 [5.19291463e-05]
R_LO*gg channel           = 2.54597213e-07 [3.03703838e-11]
R_LO*qg channel           = -4.52391989e-08 [8.34600631e-12]
R_LO*qqbar channel        = 8.69574419e-11 [9.19913821e-13]
R_LO*qq channel           = 8.75751931e-10 [8.60527874e-12]
R_LO*q1q2 channel         = 6.70308897e-10 [5.40255036e-12]
ew rescaled as^2          = 0.00000000e+00 [0.00000000e+00]
ew rescaled as^3          = 0.00000000e+00 [0.00000000e+00]
ew rescaled as^4          = 0.00000000e+00 [0.00000000e+00]
ew rescaled as^5          = 0.00000000e+00 [0.00000000e+00]
mixed EW-QCD              = 0.00000000e+00 [0.00000000e+00]
ew rescaled               = 0.00000000e+00 [0.00000000e+00]
hard ratio from eft       = 1.01679462e-01 [2.97651209e-05]
WC                        = 1.08795605e+00 [0.00000000e+00]
WC^2                      = 1.18364836e+00 [0.00000000e+00]
WC^2_trunc                = 1.18275911e+00 [0.00000000e+00]
n2                        = 1.61421938e-05 [1.51231812e-09]
n3                        = 1.07476492e-05 [1.80358156e-09]
n4                        = 3.47783982e-06 [3.17717361e-09]
n5                        = 6.38870314e-07 [4.08078863e-09]
n                         = 3.10065532e-05 [5.68218976e-09]
sigma factorized          = 3.67008559e-05 [5.74055766e-09]
exact LO t+b+c            = 9.40396414e-08 [8.81031757e-12]
exact NLO t+b+c           = 1.99754946e-07 [3.10673220e-11]
exact LO t                = 9.38287978e-08 [8.79056421e-12]
exact NLO t               = 1.99205530e-07 [3.10961924e-11]
NLO quark mass effects    = 3.03731328e-08 [3.39574554e-11]
NLO quark mass effects / eft % = 1.79317556e+01
Higgs XS                  = 2.41364165e-07 [4.74258308e-11]
delta EW                  = 2.41364165e-09 [4.74258308e-13]
delta EW %                = 1.00000000e+00 [0.00000000e+00]
R_LO*eftnnlo (with NLO PDF) = 1.69189225e-07 [2.00500443e-11]
delta PDF-TH %            = 8.00478037e+00 [7.63307439e-03]
rEFT(low)                 = 2.20675336e-07 [4.02609342e-11]
rEFT(high)                = 1.80907834e-07 [3.62767116e-11]
delta(scale)+             = 9.68430315e-09
delta(scale)-             = -3.00831990e-08
delta(scale)+(%)          = 4.58991220e+00
delta(scale)-(%)          = -1.42580462e+01
delta(scale)+ pure eft    = 2.59813259e-07
delta(scale)- pure eft    = -1.49350107e-06
delta(scale)+(%) pure eft = 7.15765742e-01
delta(scale)-(%) pure eft = -4.11448171e+00
R_LO*eftn3lo_central      = 2.10987374e-07 [4.97011184e-11]
deltaPDF+                 = 6.73351758e-08
deltaPDF-                 = 6.73351758e-08
deltaPDFsymm              = 6.73351758e-08
deltaPDF+(%)              = 3.19143154e+01
deltaPDF-(%)              = 3.19143154e+01
rEFT(as+)                 = 2.37106090e-07 [3.65134314e-11]
rEFT(as-)                 = 1.94700440e-07 [3.07255370e-11]
delta(as)+                = 2.61150578e-08
delta(as)-                = -1.62905924e-08
delta(as)+(%)             = 1.23773307e+01
delta(as)-(%)             = -7.72098804e+00
Theory Uncertainty  +     = 3.28127163e-08 [1.95190803e-11]
Theory Uncertainty  -     = -5.61481273e-08 [2.14742543e-11]
Theory Uncertainty % +    = 1.35946926e+01 [7.63307439e-03]
Theory Uncertainty % -    = -2.32628266e+01 [7.63307439e-03]
delta(PDF+a_s) +          = 8.26199741e-08 [1.62340624e-11]
delta(PDF+a_s) -          = -7.92519221e-08 [-1.55722712e-11]
delta(PDF+a_s) + %        = 3.42304227e+01 [0.00000000e+00]
delta(PDF+a_s) - %        = -3.28349993e+01 [0.00000000e+00]
Total Uncertainty +       = 1.15432690e-07 [2.53877781e-11]
Total Uncertainty -       = -1.35400049e-07 [2.65261989e-11]
Total Uncertainty + %     = 4.78251153e+01 [7.63307439e-03]
Total Uncertainty - %     = -5.60978259e+01 [7.63307439e-03]

note: delta_tbc was requested (an estimate of the uncertainty due to the lack of light quark mass effects at NNLO) but either the with_mt_expansion is set to false or the qcd perturbative order is less than NNLO. In either case we cannot estimate delta_tbc, so it is set to zero




------------------------------------------------------------------
Results in Mathematica format

<|"mh"->4.55000000e+03,"Etot"->13000,"PDF set"->PDF4LHC15_nnlo_100,"PDF member"->0,"mur"->2.27500000e+03,"muf"->2.27500000e+03,"as_at_mz"->1.18002301e-01,"as_at_mur"->7.96314296e-02,"mt_used"->1.36889608e+02,"mb_used"->2.29748317e+00,"mc_used"->5.07538497e-01,"eftlo"->1.61421938e-05,"eftnlo"->2.91402439e-05,"eftnnlo"->3.46552674e-05,"eftn3lo"->3.62986441e-05,"R_LO"->5.81264226e-03,"R_LO*eftlo"->9.38287978e-08,"R_LO*eftnlo"->1.69381813e-07,"R_LO*eftnnlo"->2.01438672e-07,"R_LO*eftn3lo"->2.10991032e-07,"ggnlo/eftnlo"->1.10225901e+00,"qgnlo/eftnlo"->-1.02398862e-01,"ggnnlo/eftn2lo"->1.17123966e+00,"qgnnlo/eftn2lo"->-1.75205569e-01,"ggn3lo/eftn3lo"->1.20667315e+00,"qgn3lo/eftn3lo"->-2.14412899e-01,"R_LO*gg channel"->2.54597213e-07,"R_LO*qg channel"->-4.52391989e-08,"R_LO*qqbar channel"->8.69574419e-11,"R_LO*qq channel"->8.75751931e-10,"R_LO*q1q2 channel"->6.70308897e-10,"ew rescaled as^2"->0.00000000e+00,"ew rescaled as^3"->0.00000000e+00,"ew rescaled as^4"->0.00000000e+00,"ew rescaled as^5"->0.00000000e+00,"mixed EW-QCD"->0.00000000e+00,"ew rescaled"->0.00000000e+00,"hard ratio from eft"->1.01679462e-01,"WC"->1.08795605e+00,"WC^2"->1.18364836e+00,"WC^2_trunc"->1.18275911e+00,"n2"->1.61421938e-05,"n3"->1.07476492e-05,"n4"->3.47783982e-06,"n5"->6.38870314e-07,"n"->3.10065532e-05,"sigma factorized"->3.67008559e-05,"exact LO t+b+c"->9.40396414e-08,"exact NLO t+b+c"->1.99754946e-07,"exact LO t"->9.38287978e-08,"exact NLO t"->1.99205530e-07,"NLO quark mass effects"->3.03731328e-08,"NLO quark mass effects / eft %"->1.79317556e+01,"delta sigma t NLO"->1.05376732e-07,"delta sigma t+b+c NLO"->1.05715304e-07,"delta tbc ratio"->3.21296892e-03,"Higgs XS"->2.41364165e-07,"delta EW"->2.41364165e-09,"delta EW %"->1.00000000e+00,"R_LO*eftnnlo (with NLO PDF)"->1.69189225e-07,"delta PDF-TH %"->8.00478037e+00,"rEFT(low)"->2.20675336e-07,"rEFT(high)"->1.80907834e-07,"delta(scale)+"->9.68430315e-09,"delta(scale)-"->-3.00831990e-08,"delta(scale)+(%)"->4.58991220e+00,"delta(scale)-(%)"->-1.42580462e+01,"delta(scale)+ pure eft"->2.59813259e-07,"delta(scale)- pure eft"->-1.49350107e-06,"delta(scale)+(%) pure eft"->7.15765742e-01,"delta(scale)-(%) pure eft"->-4.11448171e+00,"R_LO*eftn3lo_central"->2.10987374e-07,"deltaPDF+"->6.73351758e-08,"deltaPDF-"->6.73351758e-08,"deltaPDFsymm"->6.73351758e-08,"deltaPDF+(%)"->3.19143154e+01,"deltaPDF-(%)"->3.19143154e+01,"rEFT(as+)"->2.37106090e-07,"rEFT(as-)"->1.94700440e-07,"delta(as)+"->2.61150578e-08,"delta(as)-"->-1.62905924e-08,"delta(as)+(%)"->1.23773307e+01,"delta(as)-(%)"->-7.72098804e+00,"Theory Uncertainty  +"->3.28127163e-08,"Theory Uncertainty  -"->-5.61481273e-08,"Theory Uncertainty % +"->1.35946926e+01,"Theory Uncertainty % -"->-2.32628266e+01,"delta(PDF+a_s) +"->8.26199741e-08,"delta(PDF+a_s) -"->-7.92519221e-08,"delta(PDF+a_s) + %"->3.42304227e+01,"delta(PDF+a_s) - %"->-3.28349993e+01,"Total Uncertainty +"->1.15432690e-07,"Total Uncertainty -"->-1.35400049e-07,"Total Uncertainty + %"->4.78251153e+01,"Total Uncertainty - %"->-5.60978259e+01|>


------------------------------------------------------------------
User defined options (including command line ones)
Etot                        = 13000.0               #    COM energy of the collider in GeV
m_higgs                     = 4550                  #    higgs mass in GeV
mur                         = 2275.                 #    mur
muf                         = 2275.                 #    muf
pdf_member                  = 0                     #    pdf member id (the range depends on the pdf set)
pdf_set                     = PDF4LHC15_nnlo_100    #    choose a specific pdf set name (LHAPDF6 list at lhapdf.hepforge.org/pdfsets.html). This set will be used irrespectively of order.
pdf_set_for_nlo             = PDF4LHC15_nlo_100     #    pdf set used when computing PDF-TH error.
with_eft                    = true                  #    compute the cross section in the EFT approximation
with_exact_qcd_corrections  = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_ew_corrections         = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_mt_expansion           = false                 #    include NNLO 1/mt terms
with_delta_pdf_th           = true                  #    compute PDF-TH uncertainty
with_scale_variation        = true                  #    estimate scale variation (mur and muf should be at mh/2)
with_indiv_mass_effects     = false                 #    compute separately light quark contributions
with_pdf_error              = true                  #    whether or not to compute error due to pdfs
with_a_s_error              = true                  #    compute a_s uncertainty
with_resummation            = false                 #    include threshold resummation
resummation_log_order       = 3                     #    0:LL, 1:NLL, 2:NNLL, 3:N3LL
resummation_matching_order  = 3                     #    0:L0, 1:NL0, 2:NNL0, 3:N3L0
resummation_type            = log                   #    variant of threshold resummation, i.e. log:classical, psi, AP2log, AP2psi 
with_scet                   = false                 #    include scet resummation
verbose                     = minimal               #    level of verbosity: minimal or medium. Medium shows channel breakdown EFT cross section.
input_filename              = pheno.card            #    filename to use as runcard
output_filename             = mHRuns/Run_mh4550.txt #    filename to write output
help                        = false                 #    print all options and help messages per option.
make_runcard                = false                 #    create default runcard file as default_card.
make_pheno_card             = false                 #    create pheno runcard file as pheno_card.
write_documentation         = false                 #    print the help message in a TeX form.
with_eft_channel_info       = false                 #    print eft cross section per channel per order
with_resummation_info       = false                 #    info from resummation: true, false
mt_msbar                    = 162.7                 #    MSbar top mass
mt_msbar_ref_scale          = 162.7                 #    reference scale for the top mass in MSbar
mt_on_shell                 = 172.5                 #    On Shell top mass
mb_msbar                    = 4.18                  #    MSbar bottom mass
mb_msbar_ref_scale          = 4.18                  #    reference scale for the bottom mass in MSbar
mb_on_shell                 = 4.92                  #    On Shell bottom mass
mc_msbar                    = 0.986                 #    MSbar charm mass
mc_msbar_ref_scale          = 3.0                   #    reference scale for the charm mass in MSbar
mc_on_shell                 = 1.67                  #    On Shell charm mass
top_scheme                  = msbar                 #    msbar or on-shell
bottom_scheme               = msbar                 #    msbar or on-shell
charm_scheme                = msbar                 #    msbar or on-shell
y_top                       = 1.0                   #    factor multiplying the Yt. Set to zero to remove the top quark
y_bot                       = 1.0                   #    factor multiplying the Yb. Set to zero to remove the bottom quark
y_charm                     = 1.0                   #    factor multiplying the Yc. Set to zero to remove the charm quark
gamma_top                   = 0.0                   #    width of top quark
gamma_bot                   = 0.0                   #    width of bottom quark
gamma_charm                 = 0.0                   #    width of charm quark
qcd_perturbative_order      = N3LO                  #    LO, NLO, NNLO, N3LO : ihixs will compute up to this order in a_s
with_fixed_as_at_mz         = 0.0                   #    set the value of a_s(mZ) by hand. Beware: this might not be compatible with your pdf choice.
qcd_order_evol              = 3                     #    used for a_s and quark mass evolution 0:L0, 1:NL0, 2:NNL0, 3:N3L0
with_lower_ord_scale_var    = false                 #    also compute scale variation for lower than the current order
epsrel                      = 0.0001                #    cuba argument: target relative error
epsabs                      = 0.0                   #    cuba argument: target absolute error
mineval                     = 50000                 #    cuba argument: minimum points to be evaluated
maxeval                     = 50000000              #    cuba argument: maximum points to be evaluated
nstart                      = 10000                 #    cuba argument: number of points for first iteration
nincrease                   = 1000                  #    cuba argument: number of points for step increase
cuba_verbose                = 0                     #    cuba argument: verbosity level: 0=silent, 2=iterations printed out


------------------------------------------------------------------
Runcard used (options may have been overwritten by command line ones in runtime)
runcard_name="pheno.card"
