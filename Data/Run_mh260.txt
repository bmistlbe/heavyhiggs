ihixs results 
Result
mh                        = 2.60000000e+02
Etot                      = 13000
PDF set                   = PDF4LHC15_nnlo_100
PDF member                = 0
mur                       = 1.30000000e+02
muf                       = 1.30000000e+02
as_at_mz                  = 1.18002301e-01
as_at_mur                 = 1.12005480e-01
mt_used                   = 1.65628772e+02
mb_used                   = 2.77982683e+00
mc_used                   = 6.14093250e-01
eftlo                     = 3.54938988e+00 [3.47572916e-04]
eftnlo                    = 7.37120933e+00 [4.34691827e-04]
eftnnlo                   = 8.98272063e+00 [8.75216565e-04]
eftn3lo                   = 9.27868819e+00 [1.37854146e-03]
R_LO                      = 1.43710712e+00
R_LO*eftlo                = 5.10085347e+00 [4.99499513e-04]
R_LO*eftnlo               = 1.05932174e+01 [6.24698720e-04]
R_LO*eftnnlo              = 1.29091318e+01 [1.25777996e-03]
R_LO*eftn3lo              = 1.33344689e+01 [1.98111175e-03]
ggnlo/eftnlo              = 9.73011977e-01 [8.19914759e-05]
qgnlo/eftnlo              = 2.62979509e-02 [3.04324498e-06]
ggnnlo/eftn2lo            = 9.69095290e-01 [1.35448562e-04]
qgnnlo/eftn2lo            = 2.89416592e-02 [3.64246449e-06]
ggn3lo/eftn3lo            = 9.71483315e-01 [1.80147633e-04]
qgn3lo/eftn3lo            = 2.57224563e-02 [5.54664820e-06]
R_LO*gg channel           = 1.29542140e+01 [1.43745765e-03]
R_LO*qg channel           = 3.42995293e-01 [5.36049773e-05]
R_LO*qqbar channel        = 1.21733552e-02 [1.05294389e-04]
R_LO*qq channel           = 7.40030000e-03 [5.72990408e-05]
R_LO*q1q2 channel         = 1.76858957e-02 [1.35693614e-03]
ew rescaled as^2          = 1.36058781e-01 [1.33235144e-05]
ew rescaled as^3          = 1.38770801e-01 [1.00085555e-05]
ew rescaled as^4          = 5.33801008e-02 [2.91192225e-05]
ew rescaled as^5          = 7.82203513e-03 [4.08289895e-05]
mixed EW-QCD              = 1.99972937e-01 [5.44989794e-05]
ew rescaled               = 3.36031717e-01 [5.28452715e-05]
hard ratio from eft       = 3.77621100e-01 [4.36000520e-05]
WC                        = 1.10405165e+00 [0.00000000e+00]
WC^2                      = 1.21893004e+00 [0.00000000e+00]
WC^2_trunc                = 1.21890491e+00 [0.00000000e+00]
n2                        = 3.54938988e+00 [3.47572916e-04]
n3                        = 3.12582496e+00 [2.56568699e-04]
n4                        = 9.21411810e-01 [7.58789463e-04]
n5                        = 4.35129293e-02 [1.05985485e-03]
n                         = 7.64013958e+00 [1.37320362e-03]
sigma factorized          = 9.31279561e+00 [1.39967409e-03]
exact LO t+b+c            = 4.99247124e+00 [4.88886216e-04]
exact NLO t+b+c           = 1.01023054e+01 [1.05076631e-03]
exact LO t                = 5.10085347e+00 [4.99499513e-04]
exact NLO t               = 1.02877964e+01 [1.05030837e-03]
NLO quark mass effects    = -4.90912038e-01 [1.22243950e-03]
NLO quark mass effects / eft % = -4.63421092e+00
Higgs XS                  = 1.31795886e+01 [2.32850912e-03]
delta EW                  = 1.31795886e-01 [2.32850912e-05]
delta EW %                = 1.00000000e+00 [0.00000000e+00]
R_LO*eftnnlo (with NLO PDF) = 1.30861489e+01 [1.26271134e-03]
delta PDF-TH %            = 6.85627354e-01 [6.90342069e-03]
rEFT(low)                 = 1.31701429e+01 [1.83585437e-03]
rEFT(high)                = 1.36748846e+01 [2.94531054e-03]
delta(scale)+             = -1.64325998e-01
delta(scale)-             = 3.40415721e-01
delta(scale)+(%)          = -1.23234003e+00
delta(scale)-(%)          = 2.55290048e+00
delta(scale)+ pure eft    = 4.26786054e-02
delta(scale)- pure eft    = -2.81028915e-01
delta(scale)+(%) pure eft = 4.59963785e-01
delta(scale)-(%) pure eft = -3.02875696e+00
R_LO*eftn3lo_central      = 1.33346451e+01 [1.30105878e-03]
deltaPDF+                 = 2.36033746e-01
deltaPDF-                 = 2.36033746e-01
deltaPDFsymm              = 2.36033746e-01
deltaPDF+(%)              = 1.77007896e+00
deltaPDF-(%)              = 1.77007896e+00
rEFT(as+)                 = 1.36466748e+01 [2.07811997e-03]
rEFT(as-)                 = 1.30214784e+01 [1.89063943e-03]
delta(as)+                = 3.12205888e-01
delta(as)-                = -3.12990475e-01
delta(as)+(%)             = 2.34134476e+00
delta(as)-(%)             = -2.34722866e+00
Theory Uncertainty  +     = 5.97414039e-02 [9.09903664e-04]
Theory Uncertainty  -     = 1.14303030e-01 [9.10066532e-04]
Theory Uncertainty % +    = 4.53287321e-01 [6.90342069e-03]
Theory Uncertainty % -    = 8.67273130e-01 [6.90342069e-03]
delta(PDF+a_s) +          = 3.86840003e-01 [6.83451135e-05]
delta(PDF+a_s) -          = -3.87458876e-01 [-6.84544531e-05]
delta(PDF+a_s) + %        = 2.93514476e+00 [0.00000000e+00]
delta(PDF+a_s) - %        = -2.93984045e+00 [0.00000000e+00]
Total Uncertainty +       = 4.46581407e-01 [9.12466839e-04]
Total Uncertainty -       = -2.73155846e-01 [9.12637444e-04]
Total Uncertainty + %     = 3.38843208e+00 [6.90342069e-03]
Total Uncertainty - %     = -2.07256732e+00 [6.90342069e-03]

note: delta_tbc was requested (an estimate of the uncertainty due to the lack of light quark mass effects at NNLO) but either the with_mt_expansion is set to false or the qcd perturbative order is less than NNLO. In either case we cannot estimate delta_tbc, so it is set to zero




------------------------------------------------------------------
Results in Mathematica format

<|"mh"->2.60000000e+02,"Etot"->13000,"PDF set"->PDF4LHC15_nnlo_100,"PDF member"->0,"mur"->1.30000000e+02,"muf"->1.30000000e+02,"as_at_mz"->1.18002301e-01,"as_at_mur"->1.12005480e-01,"mt_used"->1.65628772e+02,"mb_used"->2.77982683e+00,"mc_used"->6.14093250e-01,"eftlo"->3.54938988e+00,"eftnlo"->7.37120933e+00,"eftnnlo"->8.98272063e+00,"eftn3lo"->9.27868819e+00,"R_LO"->1.43710712e+00,"R_LO*eftlo"->5.10085347e+00,"R_LO*eftnlo"->1.05932174e+01,"R_LO*eftnnlo"->1.29091318e+01,"R_LO*eftn3lo"->1.33344689e+01,"ggnlo/eftnlo"->9.73011977e-01,"qgnlo/eftnlo"->2.62979509e-02,"ggnnlo/eftn2lo"->9.69095290e-01,"qgnnlo/eftn2lo"->2.89416592e-02,"ggn3lo/eftn3lo"->9.71483315e-01,"qgn3lo/eftn3lo"->2.57224563e-02,"R_LO*gg channel"->1.29542140e+01,"R_LO*qg channel"->3.42995293e-01,"R_LO*qqbar channel"->1.21733552e-02,"R_LO*qq channel"->7.40030000e-03,"R_LO*q1q2 channel"->1.76858957e-02,"ew rescaled as^2"->1.36058781e-01,"ew rescaled as^3"->1.38770801e-01,"ew rescaled as^4"->5.33801008e-02,"ew rescaled as^5"->7.82203513e-03,"mixed EW-QCD"->1.99972937e-01,"ew rescaled"->3.36031717e-01,"hard ratio from eft"->3.77621100e-01,"WC"->1.10405165e+00,"WC^2"->1.21893004e+00,"WC^2_trunc"->1.21890491e+00,"n2"->3.54938988e+00,"n3"->3.12582496e+00,"n4"->9.21411810e-01,"n5"->4.35129293e-02,"n"->7.64013958e+00,"sigma factorized"->9.31279561e+00,"exact LO t+b+c"->4.99247124e+00,"exact NLO t+b+c"->1.01023054e+01,"exact LO t"->5.10085347e+00,"exact NLO t"->1.02877964e+01,"NLO quark mass effects"->-4.90912038e-01,"NLO quark mass effects / eft %"->-4.63421092e+00,"delta sigma t NLO"->5.18694294e+00,"delta sigma t+b+c NLO"->5.10983415e+00,"delta tbc ratio"->1.48659425e-02,"Higgs XS"->1.31795886e+01,"delta EW"->1.31795886e-01,"delta EW %"->1.00000000e+00,"R_LO*eftnnlo (with NLO PDF)"->1.30861489e+01,"delta PDF-TH %"->6.85627354e-01,"rEFT(low)"->1.31701429e+01,"rEFT(high)"->1.36748846e+01,"delta(scale)+"->-1.64325998e-01,"delta(scale)-"->3.40415721e-01,"delta(scale)+(%)"->-1.23234003e+00,"delta(scale)-(%)"->2.55290048e+00,"delta(scale)+ pure eft"->4.26786054e-02,"delta(scale)- pure eft"->-2.81028915e-01,"delta(scale)+(%) pure eft"->4.59963785e-01,"delta(scale)-(%) pure eft"->-3.02875696e+00,"R_LO*eftn3lo_central"->1.33346451e+01,"deltaPDF+"->2.36033746e-01,"deltaPDF-"->2.36033746e-01,"deltaPDFsymm"->2.36033746e-01,"deltaPDF+(%)"->1.77007896e+00,"deltaPDF-(%)"->1.77007896e+00,"rEFT(as+)"->1.36466748e+01,"rEFT(as-)"->1.30214784e+01,"delta(as)+"->3.12205888e-01,"delta(as)-"->-3.12990475e-01,"delta(as)+(%)"->2.34134476e+00,"delta(as)-(%)"->-2.34722866e+00,"Theory Uncertainty  +"->5.97414039e-02,"Theory Uncertainty  -"->1.14303030e-01,"Theory Uncertainty % +"->4.53287321e-01,"Theory Uncertainty % -"->8.67273130e-01,"delta(PDF+a_s) +"->3.86840003e-01,"delta(PDF+a_s) -"->-3.87458876e-01,"delta(PDF+a_s) + %"->2.93514476e+00,"delta(PDF+a_s) - %"->-2.93984045e+00,"Total Uncertainty +"->4.46581407e-01,"Total Uncertainty -"->-2.73155846e-01,"Total Uncertainty + %"->3.38843208e+00,"Total Uncertainty - %"->-2.07256732e+00|>


------------------------------------------------------------------
User defined options (including command line ones)
Etot                        = 13000.0               #    COM energy of the collider in GeV
m_higgs                     = 260                   #    higgs mass in GeV
mur                         = 130.                  #    mur
muf                         = 130.                  #    muf
pdf_member                  = 0                     #    pdf member id (the range depends on the pdf set)
pdf_set                     = PDF4LHC15_nnlo_100    #    choose a specific pdf set name (LHAPDF6 list at lhapdf.hepforge.org/pdfsets.html). This set will be used irrespectively of order.
pdf_set_for_nlo             = PDF4LHC15_nlo_100     #    pdf set used when computing PDF-TH error.
with_eft                    = true                  #    compute the cross section in the EFT approximation
with_exact_qcd_corrections  = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_ew_corrections         = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_mt_expansion           = false                 #    include NNLO 1/mt terms
with_delta_pdf_th           = true                  #    compute PDF-TH uncertainty
with_scale_variation        = true                  #    estimate scale variation (mur and muf should be at mh/2)
with_indiv_mass_effects     = false                 #    compute separately light quark contributions
with_pdf_error              = true                  #    whether or not to compute error due to pdfs
with_a_s_error              = true                  #    compute a_s uncertainty
with_resummation            = false                 #    include threshold resummation
resummation_log_order       = 3                     #    0:LL, 1:NLL, 2:NNLL, 3:N3LL
resummation_matching_order  = 3                     #    0:L0, 1:NL0, 2:NNL0, 3:N3L0
resummation_type            = log                   #    variant of threshold resummation, i.e. log:classical, psi, AP2log, AP2psi 
with_scet                   = false                 #    include scet resummation
verbose                     = minimal               #    level of verbosity: minimal or medium. Medium shows channel breakdown EFT cross section.
input_filename              = pheno.card            #    filename to use as runcard
output_filename             = mHRuns/Run_mh260.txt  #    filename to write output
help                        = false                 #    print all options and help messages per option.
make_runcard                = false                 #    create default runcard file as default_card.
make_pheno_card             = false                 #    create pheno runcard file as pheno_card.
write_documentation         = false                 #    print the help message in a TeX form.
with_eft_channel_info       = false                 #    print eft cross section per channel per order
with_resummation_info       = false                 #    info from resummation: true, false
mt_msbar                    = 162.7                 #    MSbar top mass
mt_msbar_ref_scale          = 162.7                 #    reference scale for the top mass in MSbar
mt_on_shell                 = 172.5                 #    On Shell top mass
mb_msbar                    = 4.18                  #    MSbar bottom mass
mb_msbar_ref_scale          = 4.18                  #    reference scale for the bottom mass in MSbar
mb_on_shell                 = 4.92                  #    On Shell bottom mass
mc_msbar                    = 0.986                 #    MSbar charm mass
mc_msbar_ref_scale          = 3.0                   #    reference scale for the charm mass in MSbar
mc_on_shell                 = 1.67                  #    On Shell charm mass
top_scheme                  = msbar                 #    msbar or on-shell
bottom_scheme               = msbar                 #    msbar or on-shell
charm_scheme                = msbar                 #    msbar or on-shell
y_top                       = 1.0                   #    factor multiplying the Yt. Set to zero to remove the top quark
y_bot                       = 1.0                   #    factor multiplying the Yb. Set to zero to remove the bottom quark
y_charm                     = 1.0                   #    factor multiplying the Yc. Set to zero to remove the charm quark
gamma_top                   = 0.0                   #    width of top quark
gamma_bot                   = 0.0                   #    width of bottom quark
gamma_charm                 = 0.0                   #    width of charm quark
qcd_perturbative_order      = N3LO                  #    LO, NLO, NNLO, N3LO : ihixs will compute up to this order in a_s
with_fixed_as_at_mz         = 0.0                   #    set the value of a_s(mZ) by hand. Beware: this might not be compatible with your pdf choice.
qcd_order_evol              = 3                     #    used for a_s and quark mass evolution 0:L0, 1:NL0, 2:NNL0, 3:N3L0
with_lower_ord_scale_var    = false                 #    also compute scale variation for lower than the current order
epsrel                      = 0.0001                #    cuba argument: target relative error
epsabs                      = 0.0                   #    cuba argument: target absolute error
mineval                     = 50000                 #    cuba argument: minimum points to be evaluated
maxeval                     = 50000000              #    cuba argument: maximum points to be evaluated
nstart                      = 10000                 #    cuba argument: number of points for first iteration
nincrease                   = 1000                  #    cuba argument: number of points for step increase
cuba_verbose                = 0                     #    cuba argument: verbosity level: 0=silent, 2=iterations printed out


------------------------------------------------------------------
Runcard used (options may have been overwritten by command line ones in runtime)
runcard_name="pheno.card"
