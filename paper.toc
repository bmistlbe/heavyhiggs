\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Production Mechanisms}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Production via a virtual top quark loop}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Production via a dim. 5 effective theory}{2}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Improved predictions for production via a virtual top quark loop }{4}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Electro-Weak Corrections }{5}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Summary}{5}{subsection.2.5}
\contentsline {section}{\numberline {3}Uncertainties for Cross Sections}{6}{section.3}
\contentsline {section}{\numberline {4}Tools}{6}{section.4}
\contentsline {section}{\numberline {5}Cross Section Predictions}{6}{section.5}
\contentsline {subsection}{\numberline {5.1}Predictions for the SM Higgs Boson Production Cross Section}{6}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Predictions for the dimension 5 Higgs Boson Production Cross Section}{7}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Predictions for the SM Higgs Boson Electro-Weak Corrections}{7}{subsection.5.3}
