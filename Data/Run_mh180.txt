ihixs results 
Result
mh                        = 1.80000000e+02
Etot                      = 13000
PDF set                   = PDF4LHC15_nnlo_100
PDF member                = 0
mur                       = 9.00000000e+01
muf                       = 9.00000000e+01
as_at_mz                  = 1.18002301e-01
as_at_mur                 = 1.18236639e-01
mt_used                   = 1.70790194e+02
mb_used                   = 2.86645374e+00
mc_used                   = 6.33230039e-01
eftlo                     = 7.64457586e+00 [7.44953431e-04]
eftnlo                    = 1.66516308e+01 [9.48372771e-04]
eftnnlo                   = 2.05951629e+01 [2.05745944e-03]
eftn3lo                   = 2.12881912e+01 [4.14700587e-03]
R_LO                      = 1.15221728e+00
R_LO*eftlo                = 8.80821238e+00 [8.58348214e-04]
R_LO*eftnlo               = 1.91862967e+01 [1.09273149e-03]
R_LO*eftnnlo              = 2.37301026e+01 [2.37064031e-03]
R_LO*eftn3lo              = 2.45286217e+01 [4.77825181e-03]
ggnlo/eftnlo              = 9.66118077e-01 [7.88650362e-05]
qgnlo/eftnlo              = 3.32183983e-02 [3.81242803e-06]
ggnnlo/eftn2lo            = 9.58468996e-01 [1.38075420e-04]
qgnnlo/eftn2lo            = 3.92710208e-02 [5.06297484e-06]
ggn3lo/eftn3lo            = 9.59906465e-01 [2.16749409e-04]
qgn3lo/eftn3lo            = 3.68326346e-02 [8.37020333e-06]
R_LO*gg channel           = 2.35451826e+01 [2.68854420e-03]
R_LO*qg channel           = 9.03453763e-01 [1.05723978e-04]
R_LO*qqbar channel        = 2.27022442e-02 [2.17998314e-04]
R_LO*qq channel           = 1.56149821e-02 [1.19890753e-04]
R_LO*q1q2 channel         = 4.16681639e-02 [3.94085598e-03]
ew rescaled as^2          = 7.21142434e-02 [7.02743410e-06]
ew rescaled as^3          = 8.06609769e-02 [5.53768474e-06]
ew rescaled as^4          = 3.23180731e-02 [1.72240746e-05]
ew rescaled as^5          = 4.54911732e-03 [3.39672530e-05]
mixed EW-QCD              = 1.17528167e-01 [3.97476871e-05]
ew rescaled               = 1.89642411e-01 [3.91215261e-05]
hard ratio from eft       = 4.15236894e-01 [4.62847057e-05]
WC                        = 1.10685929e+00 [0.00000000e+00]
WC^2                      = 1.22513748e+00 [0.00000000e+00]
WC^2_trunc                = 1.22516235e+00 [0.00000000e+00]
n2                        = 7.64457586e+00 [7.44953431e-04]
n3                        = 7.42464761e+00 [5.76685313e-04]
n4                        = 2.27070908e+00 [1.82387664e-03]
n5                        = 8.80449525e-02 [3.59071083e-03]
n                         = 1.74279775e+01 [4.13609137e-03]
sigma factorized          = 2.13516685e+01 [4.22441407e-03]
exact LO t+b+c            = 8.43997472e+00 [8.22463959e-04]
exact NLO t+b+c           = 1.83573192e+01 [2.14327399e-03]
exact LO t                = 8.80821238e+00 [8.58348214e-04]
exact NLO t               = 1.89648257e+01 [2.13001652e-03]
NLO quark mass effects    = -8.28977506e-01 [2.40576090e-03]
NLO quark mass effects / eft % = -4.32067490e+00
Higgs XS                  = 2.38892866e+01 [5.34985106e-03]
delta EW                  = 2.38892866e-01 [5.34985106e-05]
delta EW %                = 1.00000000e+00 [0.00000000e+00]
R_LO*eftnnlo (with NLO PDF) = 2.41583943e+01 [2.40609098e-03]
delta PDF-TH %            = 9.02422867e-01 [7.11759057e-03]
rEFT(low)                 = 2.45021761e+01 [4.35301266e-03]
rEFT(high)                = 2.42119147e+01 [6.86483804e-03]
delta(scale)+             = -2.64456103e-02
delta(scale)-             = -3.16707057e-01
delta(scale)+(%)          = -1.07815313e-01
delta(scale)-(%)          = -1.29117347e+00
delta(scale)+ pure eft    = 1.04098805e-01
delta(scale)- pure eft    = -6.66586401e-01
delta(scale)+(%) pure eft = 4.88997884e-01
delta(scale)-(%) pure eft = -3.13124959e+00
R_LO*eftn3lo_central      = 2.45285223e+01 [2.46745829e-03]
deltaPDF+                 = 4.35012182e-01
deltaPDF-                 = 4.35012182e-01
deltaPDFsymm              = 4.35012182e-01
deltaPDF+(%)              = 1.77349527e+00
deltaPDF-(%)              = 1.77349527e+00
rEFT(as+)                 = 2.51242022e+01 [5.04692635e-03]
rEFT(as-)                 = 2.39236053e+01 [4.45220350e-03]
delta(as)+                = 5.95580494e-01
delta(as)-                = -6.05016417e-01
delta(as)+(%)             = 2.42810420e+00
delta(as)-(%)             = -2.46657323e+00
Theory Uncertainty  +     = 4.28718943e-01 [1.70305000e-03]
Theory Uncertainty  -     = -7.62927383e-01 [1.70890381e-03]
Theory Uncertainty % +    = 1.79460755e+00 [7.11759057e-03]
Theory Uncertainty % -    = -3.19359634e+00 [7.11759057e-03]
delta(PDF+a_s) +          = 7.18308204e-01 [1.60860471e-04]
delta(PDF+a_s) -          = -7.25749646e-01 [-1.62526934e-04]
delta(PDF+a_s) + %        = 3.00682149e+00 [0.00000000e+00]
delta(PDF+a_s) - %        = -3.03797119e+00 [0.00000000e+00]
Total Uncertainty +       = 1.14702715e+00 [1.71063012e-03]
Total Uncertainty -       = -1.48867703e+00 [1.71661505e-03]
Total Uncertainty + %     = 4.80142904e+00 [7.11759057e-03]
Total Uncertainty - %     = -6.23156753e+00 [7.11759057e-03]

note: delta_tbc was requested (an estimate of the uncertainty due to the lack of light quark mass effects at NNLO) but either the with_mt_expansion is set to false or the qcd perturbative order is less than NNLO. In either case we cannot estimate delta_tbc, so it is set to zero




------------------------------------------------------------------
Results in Mathematica format

<|"mh"->1.80000000e+02,"Etot"->13000,"PDF set"->PDF4LHC15_nnlo_100,"PDF member"->0,"mur"->9.00000000e+01,"muf"->9.00000000e+01,"as_at_mz"->1.18002301e-01,"as_at_mur"->1.18236639e-01,"mt_used"->1.70790194e+02,"mb_used"->2.86645374e+00,"mc_used"->6.33230039e-01,"eftlo"->7.64457586e+00,"eftnlo"->1.66516308e+01,"eftnnlo"->2.05951629e+01,"eftn3lo"->2.12881912e+01,"R_LO"->1.15221728e+00,"R_LO*eftlo"->8.80821238e+00,"R_LO*eftnlo"->1.91862967e+01,"R_LO*eftnnlo"->2.37301026e+01,"R_LO*eftn3lo"->2.45286217e+01,"ggnlo/eftnlo"->9.66118077e-01,"qgnlo/eftnlo"->3.32183983e-02,"ggnnlo/eftn2lo"->9.58468996e-01,"qgnnlo/eftn2lo"->3.92710208e-02,"ggn3lo/eftn3lo"->9.59906465e-01,"qgn3lo/eftn3lo"->3.68326346e-02,"R_LO*gg channel"->2.35451826e+01,"R_LO*qg channel"->9.03453763e-01,"R_LO*qqbar channel"->2.27022442e-02,"R_LO*qq channel"->1.56149821e-02,"R_LO*q1q2 channel"->4.16681639e-02,"ew rescaled as^2"->7.21142434e-02,"ew rescaled as^3"->8.06609769e-02,"ew rescaled as^4"->3.23180731e-02,"ew rescaled as^5"->4.54911732e-03,"mixed EW-QCD"->1.17528167e-01,"ew rescaled"->1.89642411e-01,"hard ratio from eft"->4.15236894e-01,"WC"->1.10685929e+00,"WC^2"->1.22513748e+00,"WC^2_trunc"->1.22516235e+00,"n2"->7.64457586e+00,"n3"->7.42464761e+00,"n4"->2.27070908e+00,"n5"->8.80449525e-02,"n"->1.74279775e+01,"sigma factorized"->2.13516685e+01,"exact LO t+b+c"->8.43997472e+00,"exact NLO t+b+c"->1.83573192e+01,"exact LO t"->8.80821238e+00,"exact NLO t"->1.89648257e+01,"NLO quark mass effects"->-8.28977506e-01,"NLO quark mass effects / eft %"->-4.32067490e+00,"delta sigma t NLO"->1.01566133e+01,"delta sigma t+b+c NLO"->9.91734448e+00,"delta tbc ratio"->2.35579354e-02,"Higgs XS"->2.38892866e+01,"delta EW"->2.38892866e-01,"delta EW %"->1.00000000e+00,"R_LO*eftnnlo (with NLO PDF)"->2.41583943e+01,"delta PDF-TH %"->9.02422867e-01,"rEFT(low)"->2.45021761e+01,"rEFT(high)"->2.42119147e+01,"delta(scale)+"->-2.64456103e-02,"delta(scale)-"->-3.16707057e-01,"delta(scale)+(%)"->-1.07815313e-01,"delta(scale)-(%)"->-1.29117347e+00,"delta(scale)+ pure eft"->1.04098805e-01,"delta(scale)- pure eft"->-6.66586401e-01,"delta(scale)+(%) pure eft"->4.88997884e-01,"delta(scale)-(%) pure eft"->-3.13124959e+00,"R_LO*eftn3lo_central"->2.45285223e+01,"deltaPDF+"->4.35012182e-01,"deltaPDF-"->4.35012182e-01,"deltaPDFsymm"->4.35012182e-01,"deltaPDF+(%)"->1.77349527e+00,"deltaPDF-(%)"->1.77349527e+00,"rEFT(as+)"->2.51242022e+01,"rEFT(as-)"->2.39236053e+01,"delta(as)+"->5.95580494e-01,"delta(as)-"->-6.05016417e-01,"delta(as)+(%)"->2.42810420e+00,"delta(as)-(%)"->-2.46657323e+00,"Theory Uncertainty  +"->4.28718943e-01,"Theory Uncertainty  -"->-7.62927383e-01,"Theory Uncertainty % +"->1.79460755e+00,"Theory Uncertainty % -"->-3.19359634e+00,"delta(PDF+a_s) +"->7.18308204e-01,"delta(PDF+a_s) -"->-7.25749646e-01,"delta(PDF+a_s) + %"->3.00682149e+00,"delta(PDF+a_s) - %"->-3.03797119e+00,"Total Uncertainty +"->1.14702715e+00,"Total Uncertainty -"->-1.48867703e+00,"Total Uncertainty + %"->4.80142904e+00,"Total Uncertainty - %"->-6.23156753e+00|>


------------------------------------------------------------------
User defined options (including command line ones)
Etot                        = 13000.0               #    COM energy of the collider in GeV
m_higgs                     = 180                   #    higgs mass in GeV
mur                         = 90.                   #    mur
muf                         = 90.                   #    muf
pdf_member                  = 0                     #    pdf member id (the range depends on the pdf set)
pdf_set                     = PDF4LHC15_nnlo_100    #    choose a specific pdf set name (LHAPDF6 list at lhapdf.hepforge.org/pdfsets.html). This set will be used irrespectively of order.
pdf_set_for_nlo             = PDF4LHC15_nlo_100     #    pdf set used when computing PDF-TH error.
with_eft                    = true                  #    compute the cross section in the EFT approximation
with_exact_qcd_corrections  = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_ew_corrections         = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_mt_expansion           = false                 #    include NNLO 1/mt terms
with_delta_pdf_th           = true                  #    compute PDF-TH uncertainty
with_scale_variation        = true                  #    estimate scale variation (mur and muf should be at mh/2)
with_indiv_mass_effects     = false                 #    compute separately light quark contributions
with_pdf_error              = true                  #    whether or not to compute error due to pdfs
with_a_s_error              = true                  #    compute a_s uncertainty
with_resummation            = false                 #    include threshold resummation
resummation_log_order       = 3                     #    0:LL, 1:NLL, 2:NNLL, 3:N3LL
resummation_matching_order  = 3                     #    0:L0, 1:NL0, 2:NNL0, 3:N3L0
resummation_type            = log                   #    variant of threshold resummation, i.e. log:classical, psi, AP2log, AP2psi 
with_scet                   = false                 #    include scet resummation
verbose                     = minimal               #    level of verbosity: minimal or medium. Medium shows channel breakdown EFT cross section.
input_filename              = pheno.card            #    filename to use as runcard
output_filename             = mHRuns/Run_mh180.txt  #    filename to write output
help                        = false                 #    print all options and help messages per option.
make_runcard                = false                 #    create default runcard file as default_card.
make_pheno_card             = false                 #    create pheno runcard file as pheno_card.
write_documentation         = false                 #    print the help message in a TeX form.
with_eft_channel_info       = false                 #    print eft cross section per channel per order
with_resummation_info       = false                 #    info from resummation: true, false
mt_msbar                    = 162.7                 #    MSbar top mass
mt_msbar_ref_scale          = 162.7                 #    reference scale for the top mass in MSbar
mt_on_shell                 = 172.5                 #    On Shell top mass
mb_msbar                    = 4.18                  #    MSbar bottom mass
mb_msbar_ref_scale          = 4.18                  #    reference scale for the bottom mass in MSbar
mb_on_shell                 = 4.92                  #    On Shell bottom mass
mc_msbar                    = 0.986                 #    MSbar charm mass
mc_msbar_ref_scale          = 3.0                   #    reference scale for the charm mass in MSbar
mc_on_shell                 = 1.67                  #    On Shell charm mass
top_scheme                  = msbar                 #    msbar or on-shell
bottom_scheme               = msbar                 #    msbar or on-shell
charm_scheme                = msbar                 #    msbar or on-shell
y_top                       = 1.0                   #    factor multiplying the Yt. Set to zero to remove the top quark
y_bot                       = 1.0                   #    factor multiplying the Yb. Set to zero to remove the bottom quark
y_charm                     = 1.0                   #    factor multiplying the Yc. Set to zero to remove the charm quark
gamma_top                   = 0.0                   #    width of top quark
gamma_bot                   = 0.0                   #    width of bottom quark
gamma_charm                 = 0.0                   #    width of charm quark
qcd_perturbative_order      = N3LO                  #    LO, NLO, NNLO, N3LO : ihixs will compute up to this order in a_s
with_fixed_as_at_mz         = 0.0                   #    set the value of a_s(mZ) by hand. Beware: this might not be compatible with your pdf choice.
qcd_order_evol              = 3                     #    used for a_s and quark mass evolution 0:L0, 1:NL0, 2:NNL0, 3:N3L0
with_lower_ord_scale_var    = false                 #    also compute scale variation for lower than the current order
epsrel                      = 0.0001                #    cuba argument: target relative error
epsabs                      = 0.0                   #    cuba argument: target absolute error
mineval                     = 50000                 #    cuba argument: minimum points to be evaluated
maxeval                     = 50000000              #    cuba argument: maximum points to be evaluated
nstart                      = 10000                 #    cuba argument: number of points for first iteration
nincrease                   = 1000                  #    cuba argument: number of points for step increase
cuba_verbose                = 0                     #    cuba argument: verbosity level: 0=silent, 2=iterations printed out


------------------------------------------------------------------
Runcard used (options may have been overwritten by command line ones in runtime)
runcard_name="pheno.card"
