ihixs results 
Result
mh                        = 3.60000000e+02
Etot                      = 13000
PDF set                   = PDF4LHC15_nnlo_100
PDF member                = 0
mur                       = 1.80000000e+02
muf                       = 1.80000000e+02
as_at_mz                  = 1.18002301e-01
as_at_mur                 = 1.07025872e-01
mt_used                   = 1.61430676e+02
mb_used                   = 2.70936799e+00
mc_used                   = 5.98528156e-01
eftlo                     = 1.66362660e+00 [1.57621058e-04]
eftnlo                    = 3.33236337e+00 [1.97355394e-04]
eftnnlo                   = 4.01768671e+00 [3.84490840e-04]
eftn3lo                   = 4.14933456e+00 [5.46919961e-04]
R_LO                      = 3.13683136e+00
R_LO*eftlo                = 5.21851610e+00 [4.94430679e-04]
R_LO*eftnlo               = 1.04530619e+01 [6.19070590e-04]
R_LO*eftnnlo              = 1.26028057e+01 [1.20608292e-03]
R_LO*eftn3lo              = 1.30157628e+01 [1.71559569e-03]
ggnlo/eftnlo              = 9.79378766e-01 [8.26063856e-05]
qgnlo/eftnlo              = 1.99211860e-02 [2.31255701e-06]
ggnnlo/eftn2lo            = 9.78609500e-01 [1.33698796e-04]
qgnnlo/eftn2lo            = 1.96484207e-02 [2.51097727e-06]
ggn3lo/eftn3lo            = 9.81843224e-01 [1.67744195e-04]
qgn3lo/eftn3lo            = 1.56802325e-02 [4.56819721e-06]
R_LO*gg channel           = 1.27794385e+01 [1.38907235e-03]
R_LO*qg channel           = 2.04090187e-01 [5.30250994e-05]
R_LO*qqbar channel        = 1.17479043e-02 [9.71478973e-05]
R_LO*qq channel           = 6.39509061e-03 [5.03038783e-05]
R_LO*q1q2 channel         = 1.40911098e-02 [9.99483222e-04]
ew rescaled as^2          = 3.27150261e-01 [3.09960001e-05]
ew rescaled as^3          = 3.10240449e-01 [2.33553459e-05]
ew rescaled as^4          = 1.15879907e-01 [6.48889813e-05]
ew rescaled as^5          = 1.77339833e-02 [7.64887460e-05]
mixed EW-QCD              = 4.43854339e-01 [1.11928926e-04]
ew rescaled               = 7.71004600e-01 [1.07551534e-04]
hard ratio from eft       = 3.43746132e-01 [4.12562236e-05]
WC                        = 1.10179761e+00 [0.00000000e+00]
WC^2                      = 1.21395797e+00 [0.00000000e+00]
WC^2_trunc                = 1.21384530e+00 [0.00000000e+00]
n2                        = 1.66362660e+00 [1.57621058e-04]
n3                        = 1.35702195e+00 [1.16912950e-04]
n4                        = 3.90296246e-01 [3.29604507e-04]
n5                        = 2.19860848e-02 [3.86495054e-04]
n                         = 3.43293088e+00 [5.44546227e-04]
sigma factorized          = 4.16743380e+00 [5.54184346e-04]
exact LO t+b+c            = 5.18147983e+00 [4.90921661e-04]
exact NLO t+b+c           = 9.73538341e+00 [8.78238168e-04]
exact LO t                = 5.21851610e+00 [4.94430679e-04]
exact NLO t               = 9.79759340e+00 [8.80935240e-04]
NLO quark mass effects    = -7.17678512e-01 [1.07450020e-03]
NLO quark mass effects / eft % = -6.86572525e+00
Higgs XS                  = 1.30690889e+01 [2.02716220e-03]
delta EW                  = 1.30690889e-01 [2.02716220e-05]
delta EW %                = 1.00000000e+00 [0.00000000e+00]
R_LO*eftnnlo (with NLO PDF) = 1.27311966e+01 [1.22669051e-03]
delta PDF-TH %            = 5.09374139e-01 [6.82521111e-03]
rEFT(low)                 = 1.27417802e+01 [1.64393542e-03]
rEFT(high)                = 1.31643863e+01 [2.40303861e-03]
delta(scale)+             = -2.73982581e-01
delta(scale)-             = 1.48623543e-01
delta(scale)+(%)          = -2.10500595e+00
delta(scale)-(%)          = 1.14187348e+00
delta(scale)+ pure eft    = 1.86250968e-02
delta(scale)- pure eft    = -1.23293318e-01
delta(scale)+(%) pure eft = 4.48869487e-01
delta(scale)-(%) pure eft = -2.97139978e+00
R_LO*eftn3lo_central      = 1.30158186e+01 [1.26119417e-03]
deltaPDF+                 = 2.45958710e-01
deltaPDF-                 = 2.45958710e-01
deltaPDFsymm              = 2.45958710e-01
deltaPDF+(%)              = 1.88969068e+00
deltaPDF-(%)              = 1.88969068e+00
rEFT(as+)                 = 1.33244268e+01 [1.78779298e-03]
rEFT(as-)                 = 1.27143535e+01 [1.64390460e-03]
delta(as)+                = 3.08664013e-01
delta(as)-                = -3.01409281e-01
delta(as)+(%)             = 2.37146311e+00
delta(as)-(%)             = -2.31572506e+00
Theory Uncertainty  +     = -7.78436504e-02 [8.92074626e-04]
Theory Uncertainty  -     = -4.80289879e-02 [8.92024017e-04]
Theory Uncertainty % +    = -5.95631808e-01 [6.82521111e-03]
Theory Uncertainty % -    = -3.67500660e-01 [6.82521111e-03]
delta(PDF+a_s) +          = 3.96292362e-01 [6.14693880e-05]
delta(PDF+a_s) -          = -3.90621784e-01 [-6.05898177e-05]
delta(PDF+a_s) + %        = 3.03228761e+00 [0.00000000e+00]
delta(PDF+a_s) - %        = -2.98889836e+00 [0.00000000e+00]
Total Uncertainty +       = 3.18448712e-01 [8.94189926e-04]
Total Uncertainty -       = -4.38650772e-01 [8.94079399e-04]
Total Uncertainty + %     = 2.43665580e+00 [6.82521111e-03]
Total Uncertainty - %     = -3.35639902e+00 [6.82521111e-03]

note: delta_tbc was requested (an estimate of the uncertainty due to the lack of light quark mass effects at NNLO) but either the with_mt_expansion is set to false or the qcd perturbative order is less than NNLO. In either case we cannot estimate delta_tbc, so it is set to zero




------------------------------------------------------------------
Results in Mathematica format

<|"mh"->3.60000000e+02,"Etot"->13000,"PDF set"->PDF4LHC15_nnlo_100,"PDF member"->0,"mur"->1.80000000e+02,"muf"->1.80000000e+02,"as_at_mz"->1.18002301e-01,"as_at_mur"->1.07025872e-01,"mt_used"->1.61430676e+02,"mb_used"->2.70936799e+00,"mc_used"->5.98528156e-01,"eftlo"->1.66362660e+00,"eftnlo"->3.33236337e+00,"eftnnlo"->4.01768671e+00,"eftn3lo"->4.14933456e+00,"R_LO"->3.13683136e+00,"R_LO*eftlo"->5.21851610e+00,"R_LO*eftnlo"->1.04530619e+01,"R_LO*eftnnlo"->1.26028057e+01,"R_LO*eftn3lo"->1.30157628e+01,"ggnlo/eftnlo"->9.79378766e-01,"qgnlo/eftnlo"->1.99211860e-02,"ggnnlo/eftn2lo"->9.78609500e-01,"qgnnlo/eftn2lo"->1.96484207e-02,"ggn3lo/eftn3lo"->9.81843224e-01,"qgn3lo/eftn3lo"->1.56802325e-02,"R_LO*gg channel"->1.27794385e+01,"R_LO*qg channel"->2.04090187e-01,"R_LO*qqbar channel"->1.17479043e-02,"R_LO*qq channel"->6.39509061e-03,"R_LO*q1q2 channel"->1.40911098e-02,"ew rescaled as^2"->3.27150261e-01,"ew rescaled as^3"->3.10240449e-01,"ew rescaled as^4"->1.15879907e-01,"ew rescaled as^5"->1.77339833e-02,"mixed EW-QCD"->4.43854339e-01,"ew rescaled"->7.71004600e-01,"hard ratio from eft"->3.43746132e-01,"WC"->1.10179761e+00,"WC^2"->1.21395797e+00,"WC^2_trunc"->1.21384530e+00,"n2"->1.66362660e+00,"n3"->1.35702195e+00,"n4"->3.90296246e-01,"n5"->2.19860848e-02,"n"->3.43293088e+00,"sigma factorized"->4.16743380e+00,"exact LO t+b+c"->5.18147983e+00,"exact NLO t+b+c"->9.73538341e+00,"exact LO t"->5.21851610e+00,"exact NLO t"->9.79759340e+00,"NLO quark mass effects"->-7.17678512e-01,"NLO quark mass effects / eft %"->-6.86572525e+00,"delta sigma t NLO"->4.57907730e+00,"delta sigma t+b+c NLO"->4.55390358e+00,"delta tbc ratio"->5.49755449e-03,"Higgs XS"->1.30690889e+01,"delta EW"->1.30690889e-01,"delta EW %"->1.00000000e+00,"R_LO*eftnnlo (with NLO PDF)"->1.27311966e+01,"delta PDF-TH %"->5.09374139e-01,"rEFT(low)"->1.27417802e+01,"rEFT(high)"->1.31643863e+01,"delta(scale)+"->-2.73982581e-01,"delta(scale)-"->1.48623543e-01,"delta(scale)+(%)"->-2.10500595e+00,"delta(scale)-(%)"->1.14187348e+00,"delta(scale)+ pure eft"->1.86250968e-02,"delta(scale)- pure eft"->-1.23293318e-01,"delta(scale)+(%) pure eft"->4.48869487e-01,"delta(scale)-(%) pure eft"->-2.97139978e+00,"R_LO*eftn3lo_central"->1.30158186e+01,"deltaPDF+"->2.45958710e-01,"deltaPDF-"->2.45958710e-01,"deltaPDFsymm"->2.45958710e-01,"deltaPDF+(%)"->1.88969068e+00,"deltaPDF-(%)"->1.88969068e+00,"rEFT(as+)"->1.33244268e+01,"rEFT(as-)"->1.27143535e+01,"delta(as)+"->3.08664013e-01,"delta(as)-"->-3.01409281e-01,"delta(as)+(%)"->2.37146311e+00,"delta(as)-(%)"->-2.31572506e+00,"Theory Uncertainty  +"->-7.78436504e-02,"Theory Uncertainty  -"->-4.80289879e-02,"Theory Uncertainty % +"->-5.95631808e-01,"Theory Uncertainty % -"->-3.67500660e-01,"delta(PDF+a_s) +"->3.96292362e-01,"delta(PDF+a_s) -"->-3.90621784e-01,"delta(PDF+a_s) + %"->3.03228761e+00,"delta(PDF+a_s) - %"->-2.98889836e+00,"Total Uncertainty +"->3.18448712e-01,"Total Uncertainty -"->-4.38650772e-01,"Total Uncertainty + %"->2.43665580e+00,"Total Uncertainty - %"->-3.35639902e+00|>


------------------------------------------------------------------
User defined options (including command line ones)
Etot                        = 13000.0               #    COM energy of the collider in GeV
m_higgs                     = 360                   #    higgs mass in GeV
mur                         = 180.                  #    mur
muf                         = 180.                  #    muf
pdf_member                  = 0                     #    pdf member id (the range depends on the pdf set)
pdf_set                     = PDF4LHC15_nnlo_100    #    choose a specific pdf set name (LHAPDF6 list at lhapdf.hepforge.org/pdfsets.html). This set will be used irrespectively of order.
pdf_set_for_nlo             = PDF4LHC15_nlo_100     #    pdf set used when computing PDF-TH error.
with_eft                    = true                  #    compute the cross section in the EFT approximation
with_exact_qcd_corrections  = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_ew_corrections         = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_mt_expansion           = false                 #    include NNLO 1/mt terms
with_delta_pdf_th           = true                  #    compute PDF-TH uncertainty
with_scale_variation        = true                  #    estimate scale variation (mur and muf should be at mh/2)
with_indiv_mass_effects     = false                 #    compute separately light quark contributions
with_pdf_error              = true                  #    whether or not to compute error due to pdfs
with_a_s_error              = true                  #    compute a_s uncertainty
with_resummation            = false                 #    include threshold resummation
resummation_log_order       = 3                     #    0:LL, 1:NLL, 2:NNLL, 3:N3LL
resummation_matching_order  = 3                     #    0:L0, 1:NL0, 2:NNL0, 3:N3L0
resummation_type            = log                   #    variant of threshold resummation, i.e. log:classical, psi, AP2log, AP2psi 
with_scet                   = false                 #    include scet resummation
verbose                     = minimal               #    level of verbosity: minimal or medium. Medium shows channel breakdown EFT cross section.
input_filename              = pheno.card            #    filename to use as runcard
output_filename             = mHRuns/Run_mh360.txt  #    filename to write output
help                        = false                 #    print all options and help messages per option.
make_runcard                = false                 #    create default runcard file as default_card.
make_pheno_card             = false                 #    create pheno runcard file as pheno_card.
write_documentation         = false                 #    print the help message in a TeX form.
with_eft_channel_info       = false                 #    print eft cross section per channel per order
with_resummation_info       = false                 #    info from resummation: true, false
mt_msbar                    = 162.7                 #    MSbar top mass
mt_msbar_ref_scale          = 162.7                 #    reference scale for the top mass in MSbar
mt_on_shell                 = 172.5                 #    On Shell top mass
mb_msbar                    = 4.18                  #    MSbar bottom mass
mb_msbar_ref_scale          = 4.18                  #    reference scale for the bottom mass in MSbar
mb_on_shell                 = 4.92                  #    On Shell bottom mass
mc_msbar                    = 0.986                 #    MSbar charm mass
mc_msbar_ref_scale          = 3.0                   #    reference scale for the charm mass in MSbar
mc_on_shell                 = 1.67                  #    On Shell charm mass
top_scheme                  = msbar                 #    msbar or on-shell
bottom_scheme               = msbar                 #    msbar or on-shell
charm_scheme                = msbar                 #    msbar or on-shell
y_top                       = 1.0                   #    factor multiplying the Yt. Set to zero to remove the top quark
y_bot                       = 1.0                   #    factor multiplying the Yb. Set to zero to remove the bottom quark
y_charm                     = 1.0                   #    factor multiplying the Yc. Set to zero to remove the charm quark
gamma_top                   = 0.0                   #    width of top quark
gamma_bot                   = 0.0                   #    width of bottom quark
gamma_charm                 = 0.0                   #    width of charm quark
qcd_perturbative_order      = N3LO                  #    LO, NLO, NNLO, N3LO : ihixs will compute up to this order in a_s
with_fixed_as_at_mz         = 0.0                   #    set the value of a_s(mZ) by hand. Beware: this might not be compatible with your pdf choice.
qcd_order_evol              = 3                     #    used for a_s and quark mass evolution 0:L0, 1:NL0, 2:NNL0, 3:N3L0
with_lower_ord_scale_var    = false                 #    also compute scale variation for lower than the current order
epsrel                      = 0.0001                #    cuba argument: target relative error
epsabs                      = 0.0                   #    cuba argument: target absolute error
mineval                     = 50000                 #    cuba argument: minimum points to be evaluated
maxeval                     = 50000000              #    cuba argument: maximum points to be evaluated
nstart                      = 10000                 #    cuba argument: number of points for first iteration
nincrease                   = 1000                  #    cuba argument: number of points for step increase
cuba_verbose                = 0                     #    cuba argument: verbosity level: 0=silent, 2=iterations printed out


------------------------------------------------------------------
Runcard used (options may have been overwritten by command line ones in runtime)
runcard_name="pheno.card"
