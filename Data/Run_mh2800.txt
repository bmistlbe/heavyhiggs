ihixs results 
Result
mh                        = 2.80000000e+03
Etot                      = 13000
PDF set                   = PDF4LHC15_nnlo_100
PDF member                = 0
mur                       = 1.40000000e+03
muf                       = 1.40000000e+03
as_at_mz                  = 1.18002301e-01
as_at_mur                 = 8.37146695e-02
mt_used                   = 1.40730128e+02
mb_used                   = 2.36194043e+00
mc_used                   = 5.21777779e-01
eftlo                     = 6.67695751e-04 [6.51606079e-08]
eftnlo                    = 1.18953185e-03 [8.90933373e-08]
eftnnlo                   = 1.39714728e-03 [1.44911916e-07]
eftn3lo                   = 1.45224988e-03 [1.91059393e-07]
R_LO                      = 2.65237291e-02
R_LO*eftlo                = 1.77097812e-05 [1.72830231e-09]
R_LO*eftnlo               = 3.15508205e-05 [2.36308754e-09]
R_LO*eftnnlo              = 3.70575560e-05 [3.84360439e-09]
R_LO*eftn3lo              = 3.85190825e-05 [5.06760757e-09]
ggnlo/eftnlo              = 1.05626440e+00 [1.08751437e-04]
qgnlo/eftnlo              = -5.66091539e-02 [7.02493683e-06]
ggnnlo/eftn2lo            = 1.09456336e+00 [1.53220146e-04]
qgnnlo/eftn2lo            = -9.62631316e-02 [1.50737149e-05]
ggn3lo/eftn3lo            = 1.11346584e+00 [1.92781961e-04]
qgn3lo/eftn3lo            = -1.16527310e-01 [2.50967994e-05]
R_LO*gg channel           = 4.28896825e-05 [4.82734377e-09]
R_LO*qg channel           = -4.48852507e-06 [7.65383756e-10]
R_LO*qqbar channel        = 2.52732248e-08 [1.94946591e-10]
R_LO*qq channel           = 4.03297416e-08 [3.86005159e-10]
R_LO*q1q2 channel         = 5.23220580e-08 [1.26672104e-09]
ew rescaled as^2          = 0.00000000e+00 [0.00000000e+00]
ew rescaled as^3          = 0.00000000e+00 [0.00000000e+00]
ew rescaled as^4          = 0.00000000e+00 [0.00000000e+00]
ew rescaled as^5          = 0.00000000e+00 [0.00000000e+00]
mixed EW-QCD              = 0.00000000e+00 [0.00000000e+00]
ew rescaled               = 0.00000000e+00 [0.00000000e+00]
hard ratio from eft       = 4.01732827e-02 [2.27013237e-05]
WC                        = 1.09027326e+00 [0.00000000e+00]
WC^2                      = 1.18869578e+00 [0.00000000e+00]
WC^2_trunc                = 1.18792153e+00 [0.00000000e+00]
n2                        = 6.67695751e-04 [6.51606079e-08]
n3                        = 4.23978851e-04 [6.03830895e-08]
n4                        = 1.23622279e-04 [1.14109465e-07]
n5                        = 1.73447268e-05 [1.23946815e-07]
n                         = 1.23264161e-03 [1.90462083e-07]
sigma factorized          = 1.46523588e-03 [1.92604733e-07]
exact LO t+b+c            = 1.77520707e-05 [1.73242936e-09]
exact NLO t+b+c           = 3.58580982e-05 [4.45277384e-09]
exact LO t                = 1.77097812e-05 [1.72830231e-09]
exact NLO t               = 3.57511903e-05 [4.39355712e-09]
NLO quark mass effects    = 4.30727762e-06 [5.04096991e-09]
NLO quark mass effects / eft % = 1.36518720e+01
Higgs XS                  = 4.28263601e-05 [7.14786850e-09]
delta EW                  = 4.28263601e-07 [7.14786850e-11]
delta EW %                = 1.00000000e+00 [0.00000000e+00]
R_LO*eftnnlo (with NLO PDF) = 3.51121849e-05 [3.66232437e-09]
delta PDF-TH %            = 2.62479684e+00 [7.16840813e-03]
rEFT(low)                 = 4.02511736e-05 [5.83983148e-09]
rEFT(high)                = 3.32352008e-05 [4.67193298e-09]
delta(scale)+             = 1.73209108e-06
delta(scale)-             = -5.28388164e-06
delta(scale)+(%)          = 4.49670908e+00
delta(scale)-(%)          = -1.37175688e+01
delta(scale)+ pure eft    = 8.19006031e-06
delta(scale)- pure eft    = -4.87613571e-05
delta(scale)+(%) pure eft = 5.63956686e-01
delta(scale)-(%) pure eft = -3.35764235e+00
R_LO*eftn3lo_central      = 3.85191014e-05 [5.89262149e-09]
deltaPDF+                 = 5.44002409e-06
deltaPDF-                 = 5.44002409e-06
deltaPDFsymm              = 5.44002409e-06
deltaPDF+(%)              = 1.41229258e+01
deltaPDF-(%)              = 1.41229258e+01
rEFT(as+)                 = 4.07487827e-05 [5.32287920e-09]
rEFT(as-)                 = 3.69850651e-05 [4.79631956e-09]
delta(as)+                = 2.22970019e-06
delta(as)-                = -1.53401738e-06
delta(as)+(%)             = 5.78855996e+00
delta(as)-(%)             = -3.98248682e+00
Theory Uncertainty  +     = 3.47814537e-06 [3.12437232e-09]
Theory Uncertainty  -     = -7.42710396e-06 [3.31079098e-09]
Theory Uncertainty % +    = 8.12150592e+00 [7.16840813e-03]
Theory Uncertainty % -    = -1.73423656e+01 [7.16840813e-03]
delta(PDF+a_s) +          = 6.53666155e-06 [1.09099156e-09]
delta(PDF+a_s) -          = -6.28420813e-06 [-1.04885620e-09]
delta(PDF+a_s) + %        = 1.52631733e+01 [0.00000000e+00]
delta(PDF+a_s) - %        = -1.46736919e+01 [0.00000000e+00]
Total Uncertainty +       = 1.00148069e-05 [3.30937532e-09]
Total Uncertainty -       = -1.37113121e-05 [3.47295786e-09]
Total Uncertainty + %     = 2.33846792e+01 [7.16840813e-03]
Total Uncertainty - %     = -3.20160575e+01 [7.16840813e-03]

note: delta_tbc was requested (an estimate of the uncertainty due to the lack of light quark mass effects at NNLO) but either the with_mt_expansion is set to false or the qcd perturbative order is less than NNLO. In either case we cannot estimate delta_tbc, so it is set to zero




------------------------------------------------------------------
Results in Mathematica format

<|"mh"->2.80000000e+03,"Etot"->13000,"PDF set"->PDF4LHC15_nnlo_100,"PDF member"->0,"mur"->1.40000000e+03,"muf"->1.40000000e+03,"as_at_mz"->1.18002301e-01,"as_at_mur"->8.37146695e-02,"mt_used"->1.40730128e+02,"mb_used"->2.36194043e+00,"mc_used"->5.21777779e-01,"eftlo"->6.67695751e-04,"eftnlo"->1.18953185e-03,"eftnnlo"->1.39714728e-03,"eftn3lo"->1.45224988e-03,"R_LO"->2.65237291e-02,"R_LO*eftlo"->1.77097812e-05,"R_LO*eftnlo"->3.15508205e-05,"R_LO*eftnnlo"->3.70575560e-05,"R_LO*eftn3lo"->3.85190825e-05,"ggnlo/eftnlo"->1.05626440e+00,"qgnlo/eftnlo"->-5.66091539e-02,"ggnnlo/eftn2lo"->1.09456336e+00,"qgnnlo/eftn2lo"->-9.62631316e-02,"ggn3lo/eftn3lo"->1.11346584e+00,"qgn3lo/eftn3lo"->-1.16527310e-01,"R_LO*gg channel"->4.28896825e-05,"R_LO*qg channel"->-4.48852507e-06,"R_LO*qqbar channel"->2.52732248e-08,"R_LO*qq channel"->4.03297416e-08,"R_LO*q1q2 channel"->5.23220580e-08,"ew rescaled as^2"->0.00000000e+00,"ew rescaled as^3"->0.00000000e+00,"ew rescaled as^4"->0.00000000e+00,"ew rescaled as^5"->0.00000000e+00,"mixed EW-QCD"->0.00000000e+00,"ew rescaled"->0.00000000e+00,"hard ratio from eft"->4.01732827e-02,"WC"->1.09027326e+00,"WC^2"->1.18869578e+00,"WC^2_trunc"->1.18792153e+00,"n2"->6.67695751e-04,"n3"->4.23978851e-04,"n4"->1.23622279e-04,"n5"->1.73447268e-05,"n"->1.23264161e-03,"sigma factorized"->1.46523588e-03,"exact LO t+b+c"->1.77520707e-05,"exact NLO t+b+c"->3.58580982e-05,"exact LO t"->1.77097812e-05,"exact NLO t"->3.57511903e-05,"NLO quark mass effects"->4.30727762e-06,"NLO quark mass effects / eft %"->1.36518720e+01,"delta sigma t NLO"->1.80414091e-05,"delta sigma t+b+c NLO"->1.81060274e-05,"delta tbc ratio"->3.58166728e-03,"Higgs XS"->4.28263601e-05,"delta EW"->4.28263601e-07,"delta EW %"->1.00000000e+00,"R_LO*eftnnlo (with NLO PDF)"->3.51121849e-05,"delta PDF-TH %"->2.62479684e+00,"rEFT(low)"->4.02511736e-05,"rEFT(high)"->3.32352008e-05,"delta(scale)+"->1.73209108e-06,"delta(scale)-"->-5.28388164e-06,"delta(scale)+(%)"->4.49670908e+00,"delta(scale)-(%)"->-1.37175688e+01,"delta(scale)+ pure eft"->8.19006031e-06,"delta(scale)- pure eft"->-4.87613571e-05,"delta(scale)+(%) pure eft"->5.63956686e-01,"delta(scale)-(%) pure eft"->-3.35764235e+00,"R_LO*eftn3lo_central"->3.85191014e-05,"deltaPDF+"->5.44002409e-06,"deltaPDF-"->5.44002409e-06,"deltaPDFsymm"->5.44002409e-06,"deltaPDF+(%)"->1.41229258e+01,"deltaPDF-(%)"->1.41229258e+01,"rEFT(as+)"->4.07487827e-05,"rEFT(as-)"->3.69850651e-05,"delta(as)+"->2.22970019e-06,"delta(as)-"->-1.53401738e-06,"delta(as)+(%)"->5.78855996e+00,"delta(as)-(%)"->-3.98248682e+00,"Theory Uncertainty  +"->3.47814537e-06,"Theory Uncertainty  -"->-7.42710396e-06,"Theory Uncertainty % +"->8.12150592e+00,"Theory Uncertainty % -"->-1.73423656e+01,"delta(PDF+a_s) +"->6.53666155e-06,"delta(PDF+a_s) -"->-6.28420813e-06,"delta(PDF+a_s) + %"->1.52631733e+01,"delta(PDF+a_s) - %"->-1.46736919e+01,"Total Uncertainty +"->1.00148069e-05,"Total Uncertainty -"->-1.37113121e-05,"Total Uncertainty + %"->2.33846792e+01,"Total Uncertainty - %"->-3.20160575e+01|>


------------------------------------------------------------------
User defined options (including command line ones)
Etot                        = 13000.0               #    COM energy of the collider in GeV
m_higgs                     = 2800                  #    higgs mass in GeV
mur                         = 1400.                 #    mur
muf                         = 1400.                 #    muf
pdf_member                  = 0                     #    pdf member id (the range depends on the pdf set)
pdf_set                     = PDF4LHC15_nnlo_100    #    choose a specific pdf set name (LHAPDF6 list at lhapdf.hepforge.org/pdfsets.html). This set will be used irrespectively of order.
pdf_set_for_nlo             = PDF4LHC15_nlo_100     #    pdf set used when computing PDF-TH error.
with_eft                    = true                  #    compute the cross section in the EFT approximation
with_exact_qcd_corrections  = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_ew_corrections         = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_mt_expansion           = false                 #    include NNLO 1/mt terms
with_delta_pdf_th           = true                  #    compute PDF-TH uncertainty
with_scale_variation        = true                  #    estimate scale variation (mur and muf should be at mh/2)
with_indiv_mass_effects     = false                 #    compute separately light quark contributions
with_pdf_error              = true                  #    whether or not to compute error due to pdfs
with_a_s_error              = true                  #    compute a_s uncertainty
with_resummation            = false                 #    include threshold resummation
resummation_log_order       = 3                     #    0:LL, 1:NLL, 2:NNLL, 3:N3LL
resummation_matching_order  = 3                     #    0:L0, 1:NL0, 2:NNL0, 3:N3L0
resummation_type            = log                   #    variant of threshold resummation, i.e. log:classical, psi, AP2log, AP2psi 
with_scet                   = false                 #    include scet resummation
verbose                     = minimal               #    level of verbosity: minimal or medium. Medium shows channel breakdown EFT cross section.
input_filename              = pheno.card            #    filename to use as runcard
output_filename             = mHRuns/Run_mh2800.txt #    filename to write output
help                        = false                 #    print all options and help messages per option.
make_runcard                = false                 #    create default runcard file as default_card.
make_pheno_card             = false                 #    create pheno runcard file as pheno_card.
write_documentation         = false                 #    print the help message in a TeX form.
with_eft_channel_info       = false                 #    print eft cross section per channel per order
with_resummation_info       = false                 #    info from resummation: true, false
mt_msbar                    = 162.7                 #    MSbar top mass
mt_msbar_ref_scale          = 162.7                 #    reference scale for the top mass in MSbar
mt_on_shell                 = 172.5                 #    On Shell top mass
mb_msbar                    = 4.18                  #    MSbar bottom mass
mb_msbar_ref_scale          = 4.18                  #    reference scale for the bottom mass in MSbar
mb_on_shell                 = 4.92                  #    On Shell bottom mass
mc_msbar                    = 0.986                 #    MSbar charm mass
mc_msbar_ref_scale          = 3.0                   #    reference scale for the charm mass in MSbar
mc_on_shell                 = 1.67                  #    On Shell charm mass
top_scheme                  = msbar                 #    msbar or on-shell
bottom_scheme               = msbar                 #    msbar or on-shell
charm_scheme                = msbar                 #    msbar or on-shell
y_top                       = 1.0                   #    factor multiplying the Yt. Set to zero to remove the top quark
y_bot                       = 1.0                   #    factor multiplying the Yb. Set to zero to remove the bottom quark
y_charm                     = 1.0                   #    factor multiplying the Yc. Set to zero to remove the charm quark
gamma_top                   = 0.0                   #    width of top quark
gamma_bot                   = 0.0                   #    width of bottom quark
gamma_charm                 = 0.0                   #    width of charm quark
qcd_perturbative_order      = N3LO                  #    LO, NLO, NNLO, N3LO : ihixs will compute up to this order in a_s
with_fixed_as_at_mz         = 0.0                   #    set the value of a_s(mZ) by hand. Beware: this might not be compatible with your pdf choice.
qcd_order_evol              = 3                     #    used for a_s and quark mass evolution 0:L0, 1:NL0, 2:NNL0, 3:N3L0
with_lower_ord_scale_var    = false                 #    also compute scale variation for lower than the current order
epsrel                      = 0.0001                #    cuba argument: target relative error
epsabs                      = 0.0                   #    cuba argument: target absolute error
mineval                     = 50000                 #    cuba argument: minimum points to be evaluated
maxeval                     = 50000000              #    cuba argument: maximum points to be evaluated
nstart                      = 10000                 #    cuba argument: number of points for first iteration
nincrease                   = 1000                  #    cuba argument: number of points for step increase
cuba_verbose                = 0                     #    cuba argument: verbosity level: 0=silent, 2=iterations printed out


------------------------------------------------------------------
Runcard used (options may have been overwritten by command line ones in runtime)
runcard_name="pheno.card"
