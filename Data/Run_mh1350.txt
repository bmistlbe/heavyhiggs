ihixs results 
Result
mh                        = 1.35000000e+03
Etot                      = 13000
PDF set                   = PDF4LHC15_nnlo_100
PDF member                = 0
mur                       = 6.75000000e+02
muf                       = 6.75000000e+02
as_at_mz                  = 1.18002301e-01
as_at_mur                 = 9.07205151e-02
mt_used                   = 1.47155749e+02
mb_used                   = 2.46978470e+00
mc_used                   = 5.45601707e-01
eftlo                     = 2.58791070e-02 [2.37362991e-06]
eftnlo                    = 4.70372427e-02 [3.08398312e-06]
eftnnlo                   = 5.52996465e-02 [5.31721712e-06]
eftn3lo                   = 5.72398556e-02 [6.73279510e-06]
R_LO                      = 2.29408587e-01
R_LO*eftlo                = 5.93688935e-03 [5.44531084e-07]
R_LO*eftnlo               = 1.07907474e-02 [7.07492209e-07]
R_LO*eftnnlo              = 1.26862138e-02 [1.21981526e-06]
R_LO*eftn3lo              = 1.31313144e-02 [1.54456101e-06]
ggnlo/eftnlo              = 1.01571773e+00 [9.32610058e-05]
qgnlo/eftnlo              = -1.63471166e-02 [1.95352370e-06]
ggnnlo/eftn2lo            = 1.03193838e+00 [1.37984054e-04]
qgnnlo/eftn2lo            = -3.31707583e-02 [5.50491157e-06]
ggn3lo/eftn3lo            = 1.04107364e+00 [1.65651570e-04]
qgn3lo/eftn3lo            = -4.29174744e-02 [1.02578934e-05]
R_LO*gg channel           = 1.36706653e-02 [1.46489753e-06]
R_LO*qg channel           = -5.63562848e-04 [1.17259552e-07]
R_LO*qqbar channel        = 1.17037143e-05 [8.77315013e-08]
R_LO*qq channel           = 4.00891866e-06 [3.88991901e-08]
R_LO*q1q2 channel         = 8.49932197e-06 [4.65600871e-07]
ew rescaled as^2          = 0.00000000e+00 [0.00000000e+00]
ew rescaled as^3          = 0.00000000e+00 [0.00000000e+00]
ew rescaled as^4          = 0.00000000e+00 [0.00000000e+00]
ew rescaled as^5          = 0.00000000e+00 [0.00000000e+00]
mixed EW-QCD              = 0.00000000e+00 [0.00000000e+00]
ew rescaled               = 0.00000000e+00 [0.00000000e+00]
hard ratio from eft       = 1.81761474e-01 [3.04792626e-05]
WC                        = 1.09399524e+00 [0.00000000e+00]
WC^2                      = 1.19682559e+00 [0.00000000e+00]
WC^2_trunc                = 1.19626331e+00 [0.00000000e+00]
n2                        = 2.58791070e-02 [2.37362991e-06]
n3                        = 1.70478917e-02 [1.95083872e-06]
n4                        = 4.75072779e-03 [4.32572463e-06]
n5                        = 4.91181030e-04 [4.10126098e-06]
n                         = 4.81689074e-02 [6.70612599e-06]
sigma factorized          = 5.76497811e-02 [6.79351233e-06]
exact LO t+b+c            = 5.95087440e-03 [5.45813792e-07]
exact NLO t+b+c           = 1.15068385e-02 [1.09484636e-06]
exact LO t                = 5.93688935e-03 [5.44531084e-07]
exact NLO t               = 1.14710956e-02 [1.09208658e-06]
NLO quark mass effects    = 7.16091124e-04 [1.30354662e-06]
NLO quark mass effects / eft % = 6.63615874e+00
Higgs XS                  = 1.38474055e-02 [2.02111417e-06]
delta EW                  = 1.38474055e-04 [2.02111417e-08]
delta EW %                = 1.00000000e+00 [0.00000000e+00]
R_LO*eftnnlo (with NLO PDF) = 1.25661205e-02 [1.20501308e-06]
delta PDF-TH %            = 4.73321972e-01 [6.75805810e-03]
rEFT(low)                 = 1.37113971e-02 [1.70473746e-06]
rEFT(high)                = 1.13711397e-02 [1.48886677e-06]
delta(scale)+             = 5.80082729e-04
delta(scale)-             = -1.76017467e-03
delta(scale)+(%)          = 4.41755267e+00
delta(scale)-(%)          = -1.34044058e+01
delta(scale)+ pure eft    = 2.76191987e-04
delta(scale)- pure eft    = -1.72938650e-03
delta(scale)+(%) pure eft = 4.82516918e-01
delta(scale)-(%) pure eft = -3.02129780e+00
R_LO*eftn3lo_central      = 1.31322739e-02 [1.43093816e-06]
deltaPDF+                 = 7.57221198e-04
deltaPDF-                 = 7.57221198e-04
deltaPDFsymm              = 7.57221198e-04
deltaPDF+(%)              = 5.76610879e+00
deltaPDF-(%)              = 5.76610879e+00
rEFT(as+)                 = 1.35353759e-02 [1.59184226e-06]
rEFT(as-)                 = 1.28159618e-02 [1.50060809e-06]
delta(as)+                = 4.04061490e-04
delta(as)-                = -3.15352528e-04
delta(as)+(%)             = 3.07708336e+00
delta(as)-(%)             = -2.40153056e+00
Theory Uncertainty  +     = 8.15733300e-04 [9.43359229e-07]
Theory Uncertainty  -     = -2.06017929e-03 [9.82938983e-07]
Theory Uncertainty % +    = 5.89087465e+00 [6.75805810e-03]
Theory Uncertainty % -    = -1.48777278e+01 [6.75805810e-03]
delta(PDF+a_s) +          = 9.05036302e-04 [1.32095626e-07]
delta(PDF+a_s) -          = -8.64940468e-04 [-1.26243392e-07]
delta(PDF+a_s) + %        = 6.53578247e+00 [0.00000000e+00]
delta(PDF+a_s) - %        = -6.24622763e+00 [0.00000000e+00]
Total Uncertainty +       = 1.72076960e-03 [9.52562800e-07]
Total Uncertainty -       = -2.92511976e-03 [9.91012834e-07]
Total Uncertainty + %     = 1.24266571e+01 [6.75805810e-03]
Total Uncertainty - %     = -2.11239554e+01 [6.75805810e-03]

note: delta_tbc was requested (an estimate of the uncertainty due to the lack of light quark mass effects at NNLO) but either the with_mt_expansion is set to false or the qcd perturbative order is less than NNLO. In either case we cannot estimate delta_tbc, so it is set to zero




------------------------------------------------------------------
Results in Mathematica format

<|"mh"->1.35000000e+03,"Etot"->13000,"PDF set"->PDF4LHC15_nnlo_100,"PDF member"->0,"mur"->6.75000000e+02,"muf"->6.75000000e+02,"as_at_mz"->1.18002301e-01,"as_at_mur"->9.07205151e-02,"mt_used"->1.47155749e+02,"mb_used"->2.46978470e+00,"mc_used"->5.45601707e-01,"eftlo"->2.58791070e-02,"eftnlo"->4.70372427e-02,"eftnnlo"->5.52996465e-02,"eftn3lo"->5.72398556e-02,"R_LO"->2.29408587e-01,"R_LO*eftlo"->5.93688935e-03,"R_LO*eftnlo"->1.07907474e-02,"R_LO*eftnnlo"->1.26862138e-02,"R_LO*eftn3lo"->1.31313144e-02,"ggnlo/eftnlo"->1.01571773e+00,"qgnlo/eftnlo"->-1.63471166e-02,"ggnnlo/eftn2lo"->1.03193838e+00,"qgnnlo/eftn2lo"->-3.31707583e-02,"ggn3lo/eftn3lo"->1.04107364e+00,"qgn3lo/eftn3lo"->-4.29174744e-02,"R_LO*gg channel"->1.36706653e-02,"R_LO*qg channel"->-5.63562848e-04,"R_LO*qqbar channel"->1.17037143e-05,"R_LO*qq channel"->4.00891866e-06,"R_LO*q1q2 channel"->8.49932197e-06,"ew rescaled as^2"->0.00000000e+00,"ew rescaled as^3"->0.00000000e+00,"ew rescaled as^4"->0.00000000e+00,"ew rescaled as^5"->0.00000000e+00,"mixed EW-QCD"->0.00000000e+00,"ew rescaled"->0.00000000e+00,"hard ratio from eft"->1.81761474e-01,"WC"->1.09399524e+00,"WC^2"->1.19682559e+00,"WC^2_trunc"->1.19626331e+00,"n2"->2.58791070e-02,"n3"->1.70478917e-02,"n4"->4.75072779e-03,"n5"->4.91181030e-04,"n"->4.81689074e-02,"sigma factorized"->5.76497811e-02,"exact LO t+b+c"->5.95087440e-03,"exact NLO t+b+c"->1.15068385e-02,"exact LO t"->5.93688935e-03,"exact NLO t"->1.14710956e-02,"NLO quark mass effects"->7.16091124e-04,"NLO quark mass effects / eft %"->6.63615874e+00,"delta sigma t NLO"->5.53420628e-03,"delta sigma t+b+c NLO"->5.55596408e-03,"delta tbc ratio"->3.93151311e-03,"Higgs XS"->1.38474055e-02,"delta EW"->1.38474055e-04,"delta EW %"->1.00000000e+00,"R_LO*eftnnlo (with NLO PDF)"->1.25661205e-02,"delta PDF-TH %"->4.73321972e-01,"rEFT(low)"->1.37113971e-02,"rEFT(high)"->1.13711397e-02,"delta(scale)+"->5.80082729e-04,"delta(scale)-"->-1.76017467e-03,"delta(scale)+(%)"->4.41755267e+00,"delta(scale)-(%)"->-1.34044058e+01,"delta(scale)+ pure eft"->2.76191987e-04,"delta(scale)- pure eft"->-1.72938650e-03,"delta(scale)+(%) pure eft"->4.82516918e-01,"delta(scale)-(%) pure eft"->-3.02129780e+00,"R_LO*eftn3lo_central"->1.31322739e-02,"deltaPDF+"->7.57221198e-04,"deltaPDF-"->7.57221198e-04,"deltaPDFsymm"->7.57221198e-04,"deltaPDF+(%)"->5.76610879e+00,"deltaPDF-(%)"->5.76610879e+00,"rEFT(as+)"->1.35353759e-02,"rEFT(as-)"->1.28159618e-02,"delta(as)+"->4.04061490e-04,"delta(as)-"->-3.15352528e-04,"delta(as)+(%)"->3.07708336e+00,"delta(as)-(%)"->-2.40153056e+00,"Theory Uncertainty  +"->8.15733300e-04,"Theory Uncertainty  -"->-2.06017929e-03,"Theory Uncertainty % +"->5.89087465e+00,"Theory Uncertainty % -"->-1.48777278e+01,"delta(PDF+a_s) +"->9.05036302e-04,"delta(PDF+a_s) -"->-8.64940468e-04,"delta(PDF+a_s) + %"->6.53578247e+00,"delta(PDF+a_s) - %"->-6.24622763e+00,"Total Uncertainty +"->1.72076960e-03,"Total Uncertainty -"->-2.92511976e-03,"Total Uncertainty + %"->1.24266571e+01,"Total Uncertainty - %"->-2.11239554e+01|>


------------------------------------------------------------------
User defined options (including command line ones)
Etot                        = 13000.0               #    COM energy of the collider in GeV
m_higgs                     = 1350                  #    higgs mass in GeV
mur                         = 675.                  #    mur
muf                         = 675.                  #    muf
pdf_member                  = 0                     #    pdf member id (the range depends on the pdf set)
pdf_set                     = PDF4LHC15_nnlo_100    #    choose a specific pdf set name (LHAPDF6 list at lhapdf.hepforge.org/pdfsets.html). This set will be used irrespectively of order.
pdf_set_for_nlo             = PDF4LHC15_nlo_100     #    pdf set used when computing PDF-TH error.
with_eft                    = true                  #    compute the cross section in the EFT approximation
with_exact_qcd_corrections  = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_ew_corrections         = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_mt_expansion           = false                 #    include NNLO 1/mt terms
with_delta_pdf_th           = true                  #    compute PDF-TH uncertainty
with_scale_variation        = true                  #    estimate scale variation (mur and muf should be at mh/2)
with_indiv_mass_effects     = false                 #    compute separately light quark contributions
with_pdf_error              = true                  #    whether or not to compute error due to pdfs
with_a_s_error              = true                  #    compute a_s uncertainty
with_resummation            = false                 #    include threshold resummation
resummation_log_order       = 3                     #    0:LL, 1:NLL, 2:NNLL, 3:N3LL
resummation_matching_order  = 3                     #    0:L0, 1:NL0, 2:NNL0, 3:N3L0
resummation_type            = log                   #    variant of threshold resummation, i.e. log:classical, psi, AP2log, AP2psi 
with_scet                   = false                 #    include scet resummation
verbose                     = minimal               #    level of verbosity: minimal or medium. Medium shows channel breakdown EFT cross section.
input_filename              = pheno.card            #    filename to use as runcard
output_filename             = mHRuns/Run_mh1350.txt #    filename to write output
help                        = false                 #    print all options and help messages per option.
make_runcard                = false                 #    create default runcard file as default_card.
make_pheno_card             = false                 #    create pheno runcard file as pheno_card.
write_documentation         = false                 #    print the help message in a TeX form.
with_eft_channel_info       = false                 #    print eft cross section per channel per order
with_resummation_info       = false                 #    info from resummation: true, false
mt_msbar                    = 162.7                 #    MSbar top mass
mt_msbar_ref_scale          = 162.7                 #    reference scale for the top mass in MSbar
mt_on_shell                 = 172.5                 #    On Shell top mass
mb_msbar                    = 4.18                  #    MSbar bottom mass
mb_msbar_ref_scale          = 4.18                  #    reference scale for the bottom mass in MSbar
mb_on_shell                 = 4.92                  #    On Shell bottom mass
mc_msbar                    = 0.986                 #    MSbar charm mass
mc_msbar_ref_scale          = 3.0                   #    reference scale for the charm mass in MSbar
mc_on_shell                 = 1.67                  #    On Shell charm mass
top_scheme                  = msbar                 #    msbar or on-shell
bottom_scheme               = msbar                 #    msbar or on-shell
charm_scheme                = msbar                 #    msbar or on-shell
y_top                       = 1.0                   #    factor multiplying the Yt. Set to zero to remove the top quark
y_bot                       = 1.0                   #    factor multiplying the Yb. Set to zero to remove the bottom quark
y_charm                     = 1.0                   #    factor multiplying the Yc. Set to zero to remove the charm quark
gamma_top                   = 0.0                   #    width of top quark
gamma_bot                   = 0.0                   #    width of bottom quark
gamma_charm                 = 0.0                   #    width of charm quark
qcd_perturbative_order      = N3LO                  #    LO, NLO, NNLO, N3LO : ihixs will compute up to this order in a_s
with_fixed_as_at_mz         = 0.0                   #    set the value of a_s(mZ) by hand. Beware: this might not be compatible with your pdf choice.
qcd_order_evol              = 3                     #    used for a_s and quark mass evolution 0:L0, 1:NL0, 2:NNL0, 3:N3L0
with_lower_ord_scale_var    = false                 #    also compute scale variation for lower than the current order
epsrel                      = 0.0001                #    cuba argument: target relative error
epsabs                      = 0.0                   #    cuba argument: target absolute error
mineval                     = 50000                 #    cuba argument: minimum points to be evaluated
maxeval                     = 50000000              #    cuba argument: maximum points to be evaluated
nstart                      = 10000                 #    cuba argument: number of points for first iteration
nincrease                   = 1000                  #    cuba argument: number of points for step increase
cuba_verbose                = 0                     #    cuba argument: verbosity level: 0=silent, 2=iterations printed out


------------------------------------------------------------------
Runcard used (options may have been overwritten by command line ones in runtime)
runcard_name="pheno.card"
