ihixs results 
Result
mh                        = 4.25000000e+03
Etot                      = 13000
PDF set                   = PDF4LHC15_nnlo_100
PDF member                = 0
mur                       = 2.12500000e+03
muf                       = 2.12500000e+03
as_at_mz                  = 1.18002301e-01
as_at_mur                 = 8.01804623e-02
mt_used                   = 1.37410381e+02
mb_used                   = 2.30622355e+00
mc_used                   = 5.09469340e-01
eftlo                     = 3.02583664e-05 [2.77777783e-09]
eftnlo                    = 5.44012150e-05 [4.28864979e-09]
eftnnlo                   = 6.45036240e-05 [7.18351059e-09]
eftn3lo                   = 6.74533296e-05 [1.04202157e-08]
R_LO                      = 7.21750303e-03
R_LO*eftlo                = 2.18389851e-07 [2.00486199e-11]
R_LO*eftnlo               = 3.92640934e-07 [3.09533429e-11]
R_LO*eftnnlo              = 4.65555102e-07 [5.18470095e-11]
R_LO*eftn3lo              = 4.86844611e-07 [7.52079384e-11]
ggnlo/eftnlo              = 1.09451351e+00 [1.16489318e-04]
qgnlo/eftnlo              = -9.46758692e-02 [1.19638792e-05]
ggnnlo/eftn2lo            = 1.15788051e+00 [1.68379481e-04]
qgnnlo/eftn2lo            = -1.61328012e-01 [2.62597308e-05]
ggn3lo/eftn3lo            = 1.19004085e+00 [2.31063188e-04]
qgn3lo/eftn3lo            = -1.96712577e-01 [4.66746957e-05]
R_LO*gg channel           = 5.79364977e-07 [6.81474679e-11]
R_LO*qg channel           = -9.57684577e-08 [1.72475141e-11]
R_LO*qqbar channel        = 2.15958066e-10 [2.00861419e-12]
R_LO*qq channel           = 1.66855198e-09 [1.63699045e-11]
R_LO*q1q2 channel         = 1.36358169e-09 [2.10397619e-11]
ew rescaled as^2          = 0.00000000e+00 [0.00000000e+00]
ew rescaled as^3          = 0.00000000e+00 [0.00000000e+00]
ew rescaled as^4          = 0.00000000e+00 [0.00000000e+00]
ew rescaled as^5          = 0.00000000e+00 [0.00000000e+00]
mixed EW-QCD              = 0.00000000e+00 [0.00000000e+00]
ew rescaled               = 0.00000000e+00 [0.00000000e+00]
hard ratio from eft       = 7.89683399e-02 [2.73061087e-05]
WC                        = 1.08827470e+00 [0.00000000e+00]
WC^2                      = 1.18434183e+00 [0.00000000e+00]
WC^2_trunc                = 1.18346747e+00 [0.00000000e+00]
n2                        = 3.02583664e-05 [2.77777783e-09]
n3                        = 1.98954126e-05 [3.25583283e-09]
n4                        = 6.30181733e-06 [5.75348371e-09]
n5                        = 1.10627807e-06 [7.52643088e-09]
n                         = 5.75618744e-05 [1.03954910e-08]
sigma factorized          = 6.81729354e-05 [1.05036143e-08]
exact LO t+b+c            = 2.18885368e-07 [2.00941093e-11]
exact NLO t+b+c           = 4.60845016e-07 [6.96922999e-11]
exact LO t                = 2.18389851e-07 [2.00486199e-11]
exact NLO t               = 4.59561474e-07 [6.90674769e-11]
NLO quark mass effects    = 6.82040821e-08 [7.62569741e-11]
NLO quark mass effects / eft % = 1.73705990e+01
Higgs XS                  = 5.55048693e-07 [1.07104436e-10]
delta EW                  = 5.55048693e-09 [1.07104436e-12]
delta EW %                = 1.00000000e+00 [0.00000000e+00]
R_LO*eftnnlo (with NLO PDF) = 4.02450243e-07 [4.60333253e-11]
delta PDF-TH %            = 6.77737805e+00 [7.48451517e-03]
rEFT(low)                 = 5.09081309e-07 [8.71601800e-11]
rEFT(high)                = 4.18051973e-07 [7.75377006e-11]
delta(scale)+             = 2.22366984e-08
delta(scale)-             = -6.87926373e-08
delta(scale)+(%)          = 4.56751454e+00
delta(scale)-(%)          = -1.41303068e+01
delta(scale)+ pure eft    = 4.61473790e-07
delta(scale)- pure eft    = -2.66204463e-06
delta(scale)+(%) pure eft = 6.84137896e-01
delta(scale)-(%) pure eft = -3.94649849e+00
R_LO*eftn3lo_central      = 4.86849243e-07 [1.02705220e-10]
deltaPDF+                 = 1.36113443e-07
deltaPDF-                 = 1.36113443e-07
deltaPDFsymm              = 1.36113443e-07
deltaPDF+(%)              = 2.79580270e+01
deltaPDF-(%)              = 2.79580270e+01
rEFT(as+)                 = 5.40025630e-07 [8.17454295e-11]
rEFT(as-)                 = 4.53152115e-07 [7.00870283e-11]
delta(as)+                = 5.31810197e-08
delta(as)-                = -3.36924957e-08
delta(as)+(%)             = 1.09236127e+01
delta(as)-(%)             = -6.92058513e+00
Theory Uncertainty  +     = 6.85201650e-08 [4.35960502e-11]
Theory Uncertainty  -     = -1.21598319e-07 [4.77112180e-11]
Theory Uncertainty % +    = 1.23448926e+01 [7.48451517e-03]
Theory Uncertainty % -    = -2.19076849e+01 [7.48451517e-03]
delta(PDF+a_s) +          = 1.66604926e-07 [3.21487588e-11]
delta(PDF+a_s) -          = -1.59864216e-07 [-3.08480442e-11]
delta(PDF+a_s) + %        = 3.00162721e+01 [0.00000000e+00]
delta(PDF+a_s) - %        = -2.88018363e+01 [0.00000000e+00]
Total Uncertainty +       = 2.35125091e-07 [5.41678713e-11]
Total Uncertainty -       = -2.81462534e-07 [5.68151577e-11]
Total Uncertainty + %     = 4.23611647e+01 [7.48451517e-03]
Total Uncertainty - %     = -5.07095212e+01 [7.48451517e-03]

note: delta_tbc was requested (an estimate of the uncertainty due to the lack of light quark mass effects at NNLO) but either the with_mt_expansion is set to false or the qcd perturbative order is less than NNLO. In either case we cannot estimate delta_tbc, so it is set to zero




------------------------------------------------------------------
Results in Mathematica format

<|"mh"->4.25000000e+03,"Etot"->13000,"PDF set"->PDF4LHC15_nnlo_100,"PDF member"->0,"mur"->2.12500000e+03,"muf"->2.12500000e+03,"as_at_mz"->1.18002301e-01,"as_at_mur"->8.01804623e-02,"mt_used"->1.37410381e+02,"mb_used"->2.30622355e+00,"mc_used"->5.09469340e-01,"eftlo"->3.02583664e-05,"eftnlo"->5.44012150e-05,"eftnnlo"->6.45036240e-05,"eftn3lo"->6.74533296e-05,"R_LO"->7.21750303e-03,"R_LO*eftlo"->2.18389851e-07,"R_LO*eftnlo"->3.92640934e-07,"R_LO*eftnnlo"->4.65555102e-07,"R_LO*eftn3lo"->4.86844611e-07,"ggnlo/eftnlo"->1.09451351e+00,"qgnlo/eftnlo"->-9.46758692e-02,"ggnnlo/eftn2lo"->1.15788051e+00,"qgnnlo/eftn2lo"->-1.61328012e-01,"ggn3lo/eftn3lo"->1.19004085e+00,"qgn3lo/eftn3lo"->-1.96712577e-01,"R_LO*gg channel"->5.79364977e-07,"R_LO*qg channel"->-9.57684577e-08,"R_LO*qqbar channel"->2.15958066e-10,"R_LO*qq channel"->1.66855198e-09,"R_LO*q1q2 channel"->1.36358169e-09,"ew rescaled as^2"->0.00000000e+00,"ew rescaled as^3"->0.00000000e+00,"ew rescaled as^4"->0.00000000e+00,"ew rescaled as^5"->0.00000000e+00,"mixed EW-QCD"->0.00000000e+00,"ew rescaled"->0.00000000e+00,"hard ratio from eft"->7.89683399e-02,"WC"->1.08827470e+00,"WC^2"->1.18434183e+00,"WC^2_trunc"->1.18346747e+00,"n2"->3.02583664e-05,"n3"->1.98954126e-05,"n4"->6.30181733e-06,"n5"->1.10627807e-06,"n"->5.75618744e-05,"sigma factorized"->6.81729354e-05,"exact LO t+b+c"->2.18885368e-07,"exact NLO t+b+c"->4.60845016e-07,"exact LO t"->2.18389851e-07,"exact NLO t"->4.59561474e-07,"NLO quark mass effects"->6.82040821e-08,"NLO quark mass effects / eft %"->1.73705990e+01,"delta sigma t NLO"->2.41171623e-07,"delta sigma t+b+c NLO"->2.41959648e-07,"delta tbc ratio"->3.26748643e-03,"Higgs XS"->5.55048693e-07,"delta EW"->5.55048693e-09,"delta EW %"->1.00000000e+00,"R_LO*eftnnlo (with NLO PDF)"->4.02450243e-07,"delta PDF-TH %"->6.77737805e+00,"rEFT(low)"->5.09081309e-07,"rEFT(high)"->4.18051973e-07,"delta(scale)+"->2.22366984e-08,"delta(scale)-"->-6.87926373e-08,"delta(scale)+(%)"->4.56751454e+00,"delta(scale)-(%)"->-1.41303068e+01,"delta(scale)+ pure eft"->4.61473790e-07,"delta(scale)- pure eft"->-2.66204463e-06,"delta(scale)+(%) pure eft"->6.84137896e-01,"delta(scale)-(%) pure eft"->-3.94649849e+00,"R_LO*eftn3lo_central"->4.86849243e-07,"deltaPDF+"->1.36113443e-07,"deltaPDF-"->1.36113443e-07,"deltaPDFsymm"->1.36113443e-07,"deltaPDF+(%)"->2.79580270e+01,"deltaPDF-(%)"->2.79580270e+01,"rEFT(as+)"->5.40025630e-07,"rEFT(as-)"->4.53152115e-07,"delta(as)+"->5.31810197e-08,"delta(as)-"->-3.36924957e-08,"delta(as)+(%)"->1.09236127e+01,"delta(as)-(%)"->-6.92058513e+00,"Theory Uncertainty  +"->6.85201650e-08,"Theory Uncertainty  -"->-1.21598319e-07,"Theory Uncertainty % +"->1.23448926e+01,"Theory Uncertainty % -"->-2.19076849e+01,"delta(PDF+a_s) +"->1.66604926e-07,"delta(PDF+a_s) -"->-1.59864216e-07,"delta(PDF+a_s) + %"->3.00162721e+01,"delta(PDF+a_s) - %"->-2.88018363e+01,"Total Uncertainty +"->2.35125091e-07,"Total Uncertainty -"->-2.81462534e-07,"Total Uncertainty + %"->4.23611647e+01,"Total Uncertainty - %"->-5.07095212e+01|>


------------------------------------------------------------------
User defined options (including command line ones)
Etot                        = 13000.0               #    COM energy of the collider in GeV
m_higgs                     = 4250                  #    higgs mass in GeV
mur                         = 2125.                 #    mur
muf                         = 2125.                 #    muf
pdf_member                  = 0                     #    pdf member id (the range depends on the pdf set)
pdf_set                     = PDF4LHC15_nnlo_100    #    choose a specific pdf set name (LHAPDF6 list at lhapdf.hepforge.org/pdfsets.html). This set will be used irrespectively of order.
pdf_set_for_nlo             = PDF4LHC15_nlo_100     #    pdf set used when computing PDF-TH error.
with_eft                    = true                  #    compute the cross section in the EFT approximation
with_exact_qcd_corrections  = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_ew_corrections         = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_mt_expansion           = false                 #    include NNLO 1/mt terms
with_delta_pdf_th           = true                  #    compute PDF-TH uncertainty
with_scale_variation        = true                  #    estimate scale variation (mur and muf should be at mh/2)
with_indiv_mass_effects     = false                 #    compute separately light quark contributions
with_pdf_error              = true                  #    whether or not to compute error due to pdfs
with_a_s_error              = true                  #    compute a_s uncertainty
with_resummation            = false                 #    include threshold resummation
resummation_log_order       = 3                     #    0:LL, 1:NLL, 2:NNLL, 3:N3LL
resummation_matching_order  = 3                     #    0:L0, 1:NL0, 2:NNL0, 3:N3L0
resummation_type            = log                   #    variant of threshold resummation, i.e. log:classical, psi, AP2log, AP2psi 
with_scet                   = false                 #    include scet resummation
verbose                     = minimal               #    level of verbosity: minimal or medium. Medium shows channel breakdown EFT cross section.
input_filename              = pheno.card            #    filename to use as runcard
output_filename             = mHRuns/Run_mh4250.txt #    filename to write output
help                        = false                 #    print all options and help messages per option.
make_runcard                = false                 #    create default runcard file as default_card.
make_pheno_card             = false                 #    create pheno runcard file as pheno_card.
write_documentation         = false                 #    print the help message in a TeX form.
with_eft_channel_info       = false                 #    print eft cross section per channel per order
with_resummation_info       = false                 #    info from resummation: true, false
mt_msbar                    = 162.7                 #    MSbar top mass
mt_msbar_ref_scale          = 162.7                 #    reference scale for the top mass in MSbar
mt_on_shell                 = 172.5                 #    On Shell top mass
mb_msbar                    = 4.18                  #    MSbar bottom mass
mb_msbar_ref_scale          = 4.18                  #    reference scale for the bottom mass in MSbar
mb_on_shell                 = 4.92                  #    On Shell bottom mass
mc_msbar                    = 0.986                 #    MSbar charm mass
mc_msbar_ref_scale          = 3.0                   #    reference scale for the charm mass in MSbar
mc_on_shell                 = 1.67                  #    On Shell charm mass
top_scheme                  = msbar                 #    msbar or on-shell
bottom_scheme               = msbar                 #    msbar or on-shell
charm_scheme                = msbar                 #    msbar or on-shell
y_top                       = 1.0                   #    factor multiplying the Yt. Set to zero to remove the top quark
y_bot                       = 1.0                   #    factor multiplying the Yb. Set to zero to remove the bottom quark
y_charm                     = 1.0                   #    factor multiplying the Yc. Set to zero to remove the charm quark
gamma_top                   = 0.0                   #    width of top quark
gamma_bot                   = 0.0                   #    width of bottom quark
gamma_charm                 = 0.0                   #    width of charm quark
qcd_perturbative_order      = N3LO                  #    LO, NLO, NNLO, N3LO : ihixs will compute up to this order in a_s
with_fixed_as_at_mz         = 0.0                   #    set the value of a_s(mZ) by hand. Beware: this might not be compatible with your pdf choice.
qcd_order_evol              = 3                     #    used for a_s and quark mass evolution 0:L0, 1:NL0, 2:NNL0, 3:N3L0
with_lower_ord_scale_var    = false                 #    also compute scale variation for lower than the current order
epsrel                      = 0.0001                #    cuba argument: target relative error
epsabs                      = 0.0                   #    cuba argument: target absolute error
mineval                     = 50000                 #    cuba argument: minimum points to be evaluated
maxeval                     = 50000000              #    cuba argument: maximum points to be evaluated
nstart                      = 10000                 #    cuba argument: number of points for first iteration
nincrease                   = 1000                  #    cuba argument: number of points for step increase
cuba_verbose                = 0                     #    cuba argument: verbosity level: 0=silent, 2=iterations printed out


------------------------------------------------------------------
Runcard used (options may have been overwritten by command line ones in runtime)
runcard_name="pheno.card"
