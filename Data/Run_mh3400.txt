ihixs results 
Result
mh                        = 3.40000000e+03
Etot                      = 13000
PDF set                   = PDF4LHC15_nnlo_100
PDF member                = 0
mur                       = 1.70000000e+03
muf                       = 1.70000000e+03
as_at_mz                  = 1.18002301e-01
as_at_mur                 = 8.20316871e-02
mt_used                   = 1.39156188e+02
mb_used                   = 2.33552424e+00
mc_used                   = 5.15942167e-01
eftlo                     = 1.81149792e-04 [1.72978548e-08]
eftnlo                    = 3.23171503e-04 [2.47725093e-08]
eftnnlo                   = 3.80654886e-04 [4.04505948e-08]
eftn3lo                   = 3.96519988e-04 [5.51246826e-08]
R_LO                      = 1.45499008e-02
R_LO*eftlo                = 2.63571151e-06 [2.51682072e-10]
R_LO*eftnlo               = 4.70211331e-06 [3.60437553e-10]
R_LO*eftnnlo              = 5.53849084e-06 [5.88552142e-10]
R_LO*eftn3lo              = 5.76932650e-06 [8.02058665e-10]
ggnlo/eftnlo              = 1.07226808e+00 [1.12129410e-04]
qgnlo/eftnlo              = -7.25218139e-02 [9.12874831e-06]
ggnnlo/eftn2lo            = 1.12054725e+00 [1.58654814e-04]
qgnnlo/eftn2lo            = -1.22801441e-01 [1.94507256e-05]
ggn3lo/eftn3lo            = 1.14445465e+00 [2.06042320e-04]
qgn3lo/eftn3lo            = -1.48690296e-01 [3.29138824e-05]
R_LO*gg channel           = 6.60273254e-06 [7.55308823e-10]
R_LO*qg channel           = -8.57842865e-07 [1.47770143e-10]
R_LO*qqbar channel        = 3.21898237e-09 [2.46060479e-11]
R_LO*qq channel           = 1.04583179e-08 [1.02552349e-10]
R_LO*q1q2 channel         = 1.07595314e-08 [1.99620206e-10]
ew rescaled as^2          = 0.00000000e+00 [0.00000000e+00]
ew rescaled as^3          = 0.00000000e+00 [0.00000000e+00]
ew rescaled as^4          = 0.00000000e+00 [0.00000000e+00]
ew rescaled as^5          = 0.00000000e+00 [0.00000000e+00]
mixed EW-QCD              = 0.00000000e+00 [0.00000000e+00]
ew rescaled               = 0.00000000e+00 [0.00000000e+00]
hard ratio from eft       = 1.10635381e-02 [2.31485021e-05]
WC                        = 1.08933265e+00 [0.00000000e+00]
WC^2                      = 1.18664562e+00 [0.00000000e+00]
WC^2_trunc                = 1.18582286e+00 [0.00000000e+00]
n2                        = 1.81149792e-04 [1.72978548e-08]
n3                        = 1.16006203e-04 [1.76458439e-08]
n4                        = 3.48403535e-05 [3.19255102e-08]
n5                        = 5.36685897e-06 [3.73072608e-08]
n                         = 3.37363208e-04 [5.49696416e-08]
sigma factorized          = 4.00330573e-04 [5.55655918e-08]
exact LO t+b+c            = 2.64187100e-06 [2.52270237e-10]
exact NLO t+b+c           = 5.42770659e-06 [7.32501547e-10]
exact LO t                = 2.63571151e-06 [2.51682072e-10]
exact NLO t               = 5.41202452e-06 [7.30816010e-10]
NLO quark mass effects    = 7.25593282e-07 [8.16378433e-10]
NLO quark mass effects / eft % = 1.54312164e+01
Higgs XS                  = 6.49491979e-06 [1.14445264e-09]
delta EW                  = 6.49491979e-08 [1.14445264e-11]
delta EW %                = 1.00000000e+00 [0.00000000e+00]
R_LO*eftnnlo (with NLO PDF) = 5.09159126e-06 [5.44758214e-10]
delta PDF-TH %            = 4.03448877e+00 [7.25264731e-03]
rEFT(low)                 = 6.02969123e-06 [9.38085421e-10]
rEFT(high)                = 4.96974433e-06 [7.73312142e-10]
delta(scale)+             = 2.60364729e-07
delta(scale)-             = -7.99582178e-07
delta(scale)+(%)          = 4.51291375e+00
delta(scale)-(%)          = -1.38591944e+01
delta(scale)+ pure eft    = 2.38470397e-06
delta(scale)- pure eft    = -1.41504599e-05
delta(scale)+(%) pure eft = 6.01408261e-01
delta(scale)-(%) pure eft = -3.56866245e+00
R_LO*eftn3lo_central      = 5.76920749e-06 [1.03866160e-09]
deltaPDF+                 = 1.09227041e-06
deltaPDF-                 = 1.09227041e-06
deltaPDFsymm              = 1.09227041e-06
deltaPDF+(%)              = 1.89327635e+01
deltaPDF-(%)              = 1.89327635e+01
rEFT(as+)                 = 6.20591166e-06 [8.57882208e-10]
rEFT(as-)                 = 5.47965277e-06 [7.62280945e-10]
delta(as)+                = 4.36585158e-07
delta(as)-                = -2.89673735e-07
delta(as)+(%)             = 7.56735050e+00
delta(as)-(%)             = -5.02092809e+00
Theory Uncertainty  +     = 6.20096136e-07 [4.83560201e-10]
Theory Uncertainty  -     = -1.22712957e-06 [5.18311306e-10]
Theory Uncertainty % +    = 9.54740252e+00 [7.25264731e-03]
Theory Uncertainty % -    = -1.88936832e+01 [7.25264731e-03]
delta(PDF+a_s) +          = 1.32425399e-06 [2.33343293e-10]
delta(PDF+a_s) -          = -1.27217434e-06 [-2.24166476e-10]
delta(PDF+a_s) + %        = 2.03890737e+01 [0.00000000e+00]
delta(PDF+a_s) - %        = -1.95872217e+01 [0.00000000e+00]
Total Uncertainty +       = 1.94435012e-06 [5.36916716e-10]
Total Uncertainty -       = -2.49930391e-06 [5.64709853e-10]
Total Uncertainty + %     = 2.99364763e+01 [7.25264731e-03]
Total Uncertainty - %     = -3.84809049e+01 [7.25264731e-03]

note: delta_tbc was requested (an estimate of the uncertainty due to the lack of light quark mass effects at NNLO) but either the with_mt_expansion is set to false or the qcd perturbative order is less than NNLO. In either case we cannot estimate delta_tbc, so it is set to zero




------------------------------------------------------------------
Results in Mathematica format

<|"mh"->3.40000000e+03,"Etot"->13000,"PDF set"->PDF4LHC15_nnlo_100,"PDF member"->0,"mur"->1.70000000e+03,"muf"->1.70000000e+03,"as_at_mz"->1.18002301e-01,"as_at_mur"->8.20316871e-02,"mt_used"->1.39156188e+02,"mb_used"->2.33552424e+00,"mc_used"->5.15942167e-01,"eftlo"->1.81149792e-04,"eftnlo"->3.23171503e-04,"eftnnlo"->3.80654886e-04,"eftn3lo"->3.96519988e-04,"R_LO"->1.45499008e-02,"R_LO*eftlo"->2.63571151e-06,"R_LO*eftnlo"->4.70211331e-06,"R_LO*eftnnlo"->5.53849084e-06,"R_LO*eftn3lo"->5.76932650e-06,"ggnlo/eftnlo"->1.07226808e+00,"qgnlo/eftnlo"->-7.25218139e-02,"ggnnlo/eftn2lo"->1.12054725e+00,"qgnnlo/eftn2lo"->-1.22801441e-01,"ggn3lo/eftn3lo"->1.14445465e+00,"qgn3lo/eftn3lo"->-1.48690296e-01,"R_LO*gg channel"->6.60273254e-06,"R_LO*qg channel"->-8.57842865e-07,"R_LO*qqbar channel"->3.21898237e-09,"R_LO*qq channel"->1.04583179e-08,"R_LO*q1q2 channel"->1.07595314e-08,"ew rescaled as^2"->0.00000000e+00,"ew rescaled as^3"->0.00000000e+00,"ew rescaled as^4"->0.00000000e+00,"ew rescaled as^5"->0.00000000e+00,"mixed EW-QCD"->0.00000000e+00,"ew rescaled"->0.00000000e+00,"hard ratio from eft"->1.10635381e-02,"WC"->1.08933265e+00,"WC^2"->1.18664562e+00,"WC^2_trunc"->1.18582286e+00,"n2"->1.81149792e-04,"n3"->1.16006203e-04,"n4"->3.48403535e-05,"n5"->5.36685897e-06,"n"->3.37363208e-04,"sigma factorized"->4.00330573e-04,"exact LO t+b+c"->2.64187100e-06,"exact NLO t+b+c"->5.42770659e-06,"exact LO t"->2.63571151e-06,"exact NLO t"->5.41202452e-06,"NLO quark mass effects"->7.25593282e-07,"NLO quark mass effects / eft %"->1.54312164e+01,"delta sigma t NLO"->2.77631301e-06,"delta sigma t+b+c NLO"->2.78583559e-06,"delta tbc ratio"->3.42993891e-03,"Higgs XS"->6.49491979e-06,"delta EW"->6.49491979e-08,"delta EW %"->1.00000000e+00,"R_LO*eftnnlo (with NLO PDF)"->5.09159126e-06,"delta PDF-TH %"->4.03448877e+00,"rEFT(low)"->6.02969123e-06,"rEFT(high)"->4.96974433e-06,"delta(scale)+"->2.60364729e-07,"delta(scale)-"->-7.99582178e-07,"delta(scale)+(%)"->4.51291375e+00,"delta(scale)-(%)"->-1.38591944e+01,"delta(scale)+ pure eft"->2.38470397e-06,"delta(scale)- pure eft"->-1.41504599e-05,"delta(scale)+(%) pure eft"->6.01408261e-01,"delta(scale)-(%) pure eft"->-3.56866245e+00,"R_LO*eftn3lo_central"->5.76920749e-06,"deltaPDF+"->1.09227041e-06,"deltaPDF-"->1.09227041e-06,"deltaPDFsymm"->1.09227041e-06,"deltaPDF+(%)"->1.89327635e+01,"deltaPDF-(%)"->1.89327635e+01,"rEFT(as+)"->6.20591166e-06,"rEFT(as-)"->5.47965277e-06,"delta(as)+"->4.36585158e-07,"delta(as)-"->-2.89673735e-07,"delta(as)+(%)"->7.56735050e+00,"delta(as)-(%)"->-5.02092809e+00,"Theory Uncertainty  +"->6.20096136e-07,"Theory Uncertainty  -"->-1.22712957e-06,"Theory Uncertainty % +"->9.54740252e+00,"Theory Uncertainty % -"->-1.88936832e+01,"delta(PDF+a_s) +"->1.32425399e-06,"delta(PDF+a_s) -"->-1.27217434e-06,"delta(PDF+a_s) + %"->2.03890737e+01,"delta(PDF+a_s) - %"->-1.95872217e+01,"Total Uncertainty +"->1.94435012e-06,"Total Uncertainty -"->-2.49930391e-06,"Total Uncertainty + %"->2.99364763e+01,"Total Uncertainty - %"->-3.84809049e+01|>


------------------------------------------------------------------
User defined options (including command line ones)
Etot                        = 13000.0               #    COM energy of the collider in GeV
m_higgs                     = 3400                  #    higgs mass in GeV
mur                         = 1700.                 #    mur
muf                         = 1700.                 #    muf
pdf_member                  = 0                     #    pdf member id (the range depends on the pdf set)
pdf_set                     = PDF4LHC15_nnlo_100    #    choose a specific pdf set name (LHAPDF6 list at lhapdf.hepforge.org/pdfsets.html). This set will be used irrespectively of order.
pdf_set_for_nlo             = PDF4LHC15_nlo_100     #    pdf set used when computing PDF-TH error.
with_eft                    = true                  #    compute the cross section in the EFT approximation
with_exact_qcd_corrections  = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_ew_corrections         = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_mt_expansion           = false                 #    include NNLO 1/mt terms
with_delta_pdf_th           = true                  #    compute PDF-TH uncertainty
with_scale_variation        = true                  #    estimate scale variation (mur and muf should be at mh/2)
with_indiv_mass_effects     = false                 #    compute separately light quark contributions
with_pdf_error              = true                  #    whether or not to compute error due to pdfs
with_a_s_error              = true                  #    compute a_s uncertainty
with_resummation            = false                 #    include threshold resummation
resummation_log_order       = 3                     #    0:LL, 1:NLL, 2:NNLL, 3:N3LL
resummation_matching_order  = 3                     #    0:L0, 1:NL0, 2:NNL0, 3:N3L0
resummation_type            = log                   #    variant of threshold resummation, i.e. log:classical, psi, AP2log, AP2psi 
with_scet                   = false                 #    include scet resummation
verbose                     = minimal               #    level of verbosity: minimal or medium. Medium shows channel breakdown EFT cross section.
input_filename              = pheno.card            #    filename to use as runcard
output_filename             = mHRuns/Run_mh3400.txt #    filename to write output
help                        = false                 #    print all options and help messages per option.
make_runcard                = false                 #    create default runcard file as default_card.
make_pheno_card             = false                 #    create pheno runcard file as pheno_card.
write_documentation         = false                 #    print the help message in a TeX form.
with_eft_channel_info       = false                 #    print eft cross section per channel per order
with_resummation_info       = false                 #    info from resummation: true, false
mt_msbar                    = 162.7                 #    MSbar top mass
mt_msbar_ref_scale          = 162.7                 #    reference scale for the top mass in MSbar
mt_on_shell                 = 172.5                 #    On Shell top mass
mb_msbar                    = 4.18                  #    MSbar bottom mass
mb_msbar_ref_scale          = 4.18                  #    reference scale for the bottom mass in MSbar
mb_on_shell                 = 4.92                  #    On Shell bottom mass
mc_msbar                    = 0.986                 #    MSbar charm mass
mc_msbar_ref_scale          = 3.0                   #    reference scale for the charm mass in MSbar
mc_on_shell                 = 1.67                  #    On Shell charm mass
top_scheme                  = msbar                 #    msbar or on-shell
bottom_scheme               = msbar                 #    msbar or on-shell
charm_scheme                = msbar                 #    msbar or on-shell
y_top                       = 1.0                   #    factor multiplying the Yt. Set to zero to remove the top quark
y_bot                       = 1.0                   #    factor multiplying the Yb. Set to zero to remove the bottom quark
y_charm                     = 1.0                   #    factor multiplying the Yc. Set to zero to remove the charm quark
gamma_top                   = 0.0                   #    width of top quark
gamma_bot                   = 0.0                   #    width of bottom quark
gamma_charm                 = 0.0                   #    width of charm quark
qcd_perturbative_order      = N3LO                  #    LO, NLO, NNLO, N3LO : ihixs will compute up to this order in a_s
with_fixed_as_at_mz         = 0.0                   #    set the value of a_s(mZ) by hand. Beware: this might not be compatible with your pdf choice.
qcd_order_evol              = 3                     #    used for a_s and quark mass evolution 0:L0, 1:NL0, 2:NNL0, 3:N3L0
with_lower_ord_scale_var    = false                 #    also compute scale variation for lower than the current order
epsrel                      = 0.0001                #    cuba argument: target relative error
epsabs                      = 0.0                   #    cuba argument: target absolute error
mineval                     = 50000                 #    cuba argument: minimum points to be evaluated
maxeval                     = 50000000              #    cuba argument: maximum points to be evaluated
nstart                      = 10000                 #    cuba argument: number of points for first iteration
nincrease                   = 1000                  #    cuba argument: number of points for step increase
cuba_verbose                = 0                     #    cuba argument: verbosity level: 0=silent, 2=iterations printed out


------------------------------------------------------------------
Runcard used (options may have been overwritten by command line ones in runtime)
runcard_name="pheno.card"
