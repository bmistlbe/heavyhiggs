ihixs results 
Result
mh                        = 135.000000
Etot                      = 13000
PDF set                   = PDF4LHC15_nnlo_100
PDF member                = 0
mur                       = 67.500000
muf                       = 67.500000
as_at_mz                  = 0.118002
as_at_mur                 = 0.123631
mt_used                   = 175.182495
mb_used                   = 2.940172
mc_used                   = 0.649515
eftlo                     = 13.133417 [0.001277]
eftnlo                    = 29.865990 [0.001660]
eftnnlo                   = 37.460245 [0.003823]
eftn3lo                   = 38.754826 [0.008528]
R_LO                      = 1.075201
R_LO*eftlo                = 14.121069 [0.001373]
R_LO*eftnlo               = 32.111956 [0.001785]
R_LO*eftnnlo              = 40.277310 [0.004110]
R_LO*eftn3lo              = 41.669246 [0.009169]
ggnlo/eftnlo              = 0.960725 [0.000077]
qgnlo/eftnlo              = 0.038620 [0.000004]
ggnnlo/eftn2lo            = 0.949843 [0.000140]
qgnnlo/eftn2lo            = 0.047589 [0.000006]
ggn3lo/eftn3lo            = 0.950505 [0.000237]
qgn3lo/eftn3lo            = 0.045765 [0.000011]
R_LO*gg channel           = 39.606815 [0.004632]
R_LO*qg channel           = 1.906999 [0.000199]
R_LO*qqbar channel        = 0.039980 [0.000410]
R_LO*qq channel           = 0.029768 [0.000227]
R_LO*q1q2 channel         = 0.085683 [0.007896]
ew rescaled as^2          = 0.798312 [0.000078]
ew rescaled as^3          = 0.966660 [0.000064]
ew rescaled as^4          = 0.401982 [0.000209]
ew rescaled as^5          = 0.054962 [0.000463]
mixed EW-QCD              = 1.423604 [0.000524]
ew rescaled               = 2.221916 [0.000518]
hard ratio from eft       = 0.444602 [0.000049]
WC                        = 1.109318 [0.000000]
WC^2                      = 1.230586 [0.000000]
WC^2_trunc                = 1.230584 [0.000000]
n                         = 31.573462 [0.008508]
sigma factorized          = 38.853845 [0.008705]
exact LO t+b+c            = 13.195898 [0.001283]
exact NLO t+b+c           = 30.422407 [0.003951]
exact LO t                = 14.121069 [0.001373]
exact NLO t               = 31.881520 [0.003847]
NLO quark mass effects    = -1.689549 [0.004335]
NLO quark mass effects / eft % = -5.261434
NNLO mt exp gg            = 0.326918 [0.003852]
NNLO mt exp qg            = -0.081177 [0.000119]
NNLO top mass effects     = 0.245742 [0.003854]
Higgs XS                  = 42.447354 [0.010862]
delta_tbc                 = 0.328727 [0.003595]
delta_tbc %               = 0.774436 [0.008471]
delta(1/m_t)              = 0.424474 [0.000109]
delta(1/m_t) %            = 1.000000 [0.000000]
delta EW                  = 0.424474 [0.000109]
delta EW %                = 1.000000 [0.000000]
R_LO*eftnnlo (with NLO PDF) = 41.162003 [0.004155]
delta PDF-TH %            = 1.098252 [0.007257]
rEFT(low)                 = 41.760145 [0.009600]
rEFT(high)                = 40.704513 [0.014492]
delta(scale)+             = 0.090899
delta(scale)-             = -0.964733
delta(scale)+(%)          = 0.218144
delta(scale)-(%)          = -2.315215
delta(scale)+ pure eft    = 0.203740
delta(scale)- pure eft    = -1.258833
delta(scale)+(%) pure eft = 0.525714
delta(scale)-(%) pure eft = -3.248196
R_LO*eftn3lo_central      = 41.671120 [0.004361]
deltaPDF+                 = 0.762777
deltaPDF-                 = 0.762777
deltaPDFsymm              = 0.762777
deltaPDF+(%)              = 1.830468
deltaPDF-(%)              = 1.830468
rEFT(as+)                 = 42.733580 [0.009751]
rEFT(as-)                 = 40.591507 [0.008585]
delta(as)+                = 1.064334
delta(as)-                = -1.077739
delta(as)+(%)             = 2.554244
delta(as)-(%)             = -2.586413
Theory Uncertainty  +     = 1.736450 [0.004756]
Theory Uncertainty  -     = -2.626601 [0.004782]
Theory Uncertainty % +    = 4.090831 [0.011154]
Theory Uncertainty % -    = -6.187902 [0.011154]
delta(PDF+a_s) +          = 1.333872 [0.000341]
delta(PDF+a_s) -          = -1.344995 [-0.000344]
delta(PDF+a_s) + %        = 3.142416 [0.000000]
delta(PDF+a_s) - %        = -3.168619 [0.000000]
Total Uncertainty +       = 3.070322 [0.004768]
Total Uncertainty -       = -3.971596 [0.004795]
Total Uncertainty + %     = 7.233247 [0.011154]
Total Uncertainty - %     = -9.356521 [0.011154]





------------------------------------------------------------------
Results in Mathematica format

<|"mh"->135.000000,"Etot"->13000,"PDF set"->PDF4LHC15_nnlo_100,"PDF member"->0,"mur"->67.500000,"muf"->67.500000,"as_at_mz"->0.118002,"as_at_mur"->0.123631,"mt_used"->175.182495,"mb_used"->2.940172,"mc_used"->0.649515,"eftlo"->13.133417,"eftnlo"->29.865990,"eftnnlo"->37.460245,"eftn3lo"->38.754826,"R_LO"->1.075201,"R_LO*eftlo"->14.121069,"R_LO*eftnlo"->32.111956,"R_LO*eftnnlo"->40.277310,"R_LO*eftn3lo"->41.669246,"ggnlo/eftnlo"->0.960725,"qgnlo/eftnlo"->0.038620,"ggnnlo/eftn2lo"->0.949843,"qgnnlo/eftn2lo"->0.047589,"ggn3lo/eftn3lo"->0.950505,"qgn3lo/eftn3lo"->0.045765,"gg at order 4"->7.406384,"qg at order 4"->0.676604,"R_LO*gg channel"->39.606815,"R_LO*qg channel"->1.906999,"R_LO*qqbar channel"->0.039980,"R_LO*qq channel"->0.029768,"R_LO*q1q2 channel"->0.085683,"ew rescaled as^2"->0.798312,"ew rescaled as^3"->0.966660,"ew rescaled as^4"->0.401982,"ew rescaled as^5"->0.054962,"mixed EW-QCD"->1.423604,"ew rescaled"->2.221916,"hard ratio from eft"->0.444602,"WC"->1.109318,"WC^2"->1.230586,"WC^2_trunc"->1.230584,"n"->31.573462,"sigma factorized"->38.853845,"exact LO t+b+c"->13.195898,"exact NLO t+b+c"->30.422407,"exact LO t"->14.121069,"exact NLO t"->31.881520,"NLO quark mass effects"->-1.689549,"NLO quark mass effects / eft %"->-5.261434,"delta sigma t NLO"->17.760451,"delta sigma t+b+c NLO"->17.226509,"delta tbc ratio"->0.030064,"NNLO mt exp gg"->0.326918,"NNLO mt exp qg"->-0.081177,"NNLO top mass effects"->0.245742,"Higgs XS"->42.447354,"delta_tbc"->0.328727,"delta_tbc %"->0.774436,"delta(1/m_t)"->0.424474,"delta(1/m_t) % "->1.000000,"delta EW"->0.424474,"delta EW %"->1.000000,"R_LO*eftnnlo (with NLO PDF)"->41.162003,"delta PDF-TH %"->1.098252,"rEFT(low)"->41.760145,"rEFT(high)"->40.704513,"delta(scale)+"->0.090899,"delta(scale)-"->-0.964733,"delta(scale)+(%)"->0.218144,"delta(scale)-(%)"->-2.315215,"delta(scale)+ pure eft"->0.203740,"delta(scale)- pure eft"->-1.258833,"delta(scale)+(%) pure eft"->0.525714,"delta(scale)-(%) pure eft"->-3.248196,"R_LO*eftn3lo_central"->41.671120,"deltaPDF+"->0.762777,"deltaPDF-"->0.762777,"deltaPDFsymm"->0.762777,"deltaPDF+(%)"->1.830468,"deltaPDF-(%)"->1.830468,"rEFT(as+)"->42.733580,"rEFT(as-)"->40.591507,"delta(as)+"->1.064334,"delta(as)-"->-1.077739,"delta(as)+(%)"->2.554244,"delta(as)-(%)"->-2.586413,"Theory Uncertainty  +"->1.736450,"Theory Uncertainty  -"->-2.626601,"Theory Uncertainty % +"->4.090831,"Theory Uncertainty % -"->-6.187902,"delta(PDF+a_s) +"->1.333872,"delta(PDF+a_s) -"->-1.344995,"delta(PDF+a_s) + %"->3.142416,"delta(PDF+a_s) - %"->-3.168619,"Total Uncertainty +"->3.070322,"Total Uncertainty -"->-3.971596,"Total Uncertainty + %"->7.233247,"Total Uncertainty - %"->-9.356521|>


------------------------------------------------------------------
User defined options (including command line ones)
Etot                        = 13000.0               #    COM energy of the collider in GeV
m_higgs                     = 135                   #    higgs mass in GeV
mur                         = 67.5                  #    mur
muf                         = 67.5                  #    muf
pdf_member                  = 0                     #    pdf member id (the range depends on the pdf set)
pdf_set                     = PDF4LHC15_nnlo_100    #    choose a specific pdf set name (LHAPDF6 list at lhapdf.hepforge.org/pdfsets.html). This set will be used irrespectively of order.
pdf_set_for_nlo             = PDF4LHC15_nlo_100     #    pdf set used when computing PDF-TH error.
with_eft                    = true                  #    compute the cross section in the EFT approximation
with_exact_qcd_corrections  = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_ew_corrections         = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_mt_expansion           = true                  #    include NNLO 1/mt terms
with_delta_pdf_th           = true                  #    compute PDF-TH uncertainty
with_scale_variation        = true                  #    estimate scale variation (mur and muf should be at mh/2)
with_indiv_mass_effects     = false                 #    compute separately light quark contributions
with_pdf_error              = true                  #    whether or not to compute error due to pdfs
with_a_s_error              = true                  #    compute a_s uncertainty
with_resummation            = false                 #    include threshold resummation
resummation_log_order       = 3                     #    0:LL, 1:NLL, 2:NNLL, 3:N3LL
resummation_matching_order  = 3                     #    0:L0, 1:NL0, 2:NNL0, 3:N3L0
resummation_type            = log                   #    variant of threshold resummation, i.e. log:classical, psi, AP2log, AP2psi 
with_scet                   = false                 #    include scet resummation
verbose                     = minimal               #    level of verbosity: minimal or medium. Medium shows channel breakdown EFT cross section.
input_filename              = pheno.card            #    filename to use as runcard
output_filename             = mHRuns/Run_mh135.txt  #    filename to write output
help                        = false                 #    print all options and help messages per option.
make_runcard                = false                 #    create default runcard file as default_card.
make_pheno_card             = false                 #    create pheno runcard file as pheno_card.
write_documentation         = false                 #    print the help message in a TeX form.
with_eft_channel_info       = false                 #    print eft cross section per channel per order
with_resummation_info       = false                 #    info from resummation: true, false
mt_msbar                    = 162.7                 #    MSbar top mass
mt_msbar_ref_scale          = 162.7                 #    reference scale for the top mass in MSbar
mt_on_shell                 = 172.5                 #    On Shell top mass
mb_msbar                    = 4.18                  #    MSbar bottom mass
mb_msbar_ref_scale          = 4.18                  #    reference scale for the bottom mass in MSbar
mb_on_shell                 = 4.92                  #    On Shell bottom mass
mc_msbar                    = 0.986                 #    MSbar charm mass
mc_msbar_ref_scale          = 3.0                   #    reference scale for the charm mass in MSbar
mc_on_shell                 = 1.67                  #    On Shell charm mass
top_scheme                  = msbar                 #    msbar or on-shell
bottom_scheme               = msbar                 #    msbar or on-shell
charm_scheme                = msbar                 #    msbar or on-shell
y_top                       = 1.0                   #    factor multiplying the Yt. Set to zero to remove the top quark
y_bot                       = 1.0                   #    factor multiplying the Yb. Set to zero to remove the bottom quark
y_charm                     = 1.0                   #    factor multiplying the Yc. Set to zero to remove the charm quark
gamma_top                   = 0.0                   #    width of top quark
gamma_bot                   = 0.0                   #    width of bottom quark
gamma_charm                 = 0.0                   #    width of charm quark
qcd_perturbative_order      = N3LO                  #    LO, NLO, NNLO, N3LO : ihixs will compute up to this order in a_s
with_fixed_as_at_mz         = 0.0                   #    set the value of a_s(mZ) by hand. Beware: this might not be compatible with your pdf choice.
qcd_order_evol              = 3                     #    used for a_s and quark mass evolution 0:L0, 1:NL0, 2:NNL0, 3:N3L0
with_lower_ord_scale_var    = false                 #    also compute scale variation for lower than the current order
epsrel                      = 0.0001                #    cuba argument: target relative error
epsabs                      = 0.0                   #    cuba argument: target absolute error
mineval                     = 50000                 #    cuba argument: minimum points to be evaluated
maxeval                     = 50000000              #    cuba argument: maximum points to be evaluated
nstart                      = 10000                 #    cuba argument: number of points for first iteration
nincrease                   = 1000                  #    cuba argument: number of points for step increase
cuba_verbose                = 0                     #    cuba argument: verbosity level: 0=silent, 2=iterations printed out


------------------------------------------------------------------
Runcard used (options may have been overwritten by command line ones in runtime)
runcard_name="pheno.card"
