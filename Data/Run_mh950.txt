ihixs results 
Result
mh                        = 9.50000000e+02
Etot                      = 13000
PDF set                   = PDF4LHC15_nnlo_100
PDF member                = 0
mur                       = 4.75000000e+02
muf                       = 4.75000000e+02
as_at_mz                  = 1.18002301e-01
as_at_mur                 = 9.45403139e-02
mt_used                   = 1.50579800e+02
mb_used                   = 2.52725223e+00
mc_used                   = 5.58296883e-01
eftlo                     = 9.75781602e-02 [9.70965251e-06]
eftnlo                    = 1.80704345e-01 [1.20795717e-05]
eftnnlo                   = 2.13322205e-01 [2.06240702e-05]
eftn3lo                   = 2.20574721e-01 [2.57916687e-05]
R_LO                      = 5.99785756e-01
R_LO*eftlo                = 5.85259906e-02 [5.82371128e-06]
R_LO*eftnlo               = 1.08383892e-01 [7.24515505e-06]
R_LO*eftnnlo              = 1.27947620e-01 [1.23700236e-05]
R_LO*eftn3lo              = 1.32297576e-01 [1.54694755e-05]
ggnlo/eftnlo              = 1.00322270e+00 [9.44638294e-05]
qgnlo/eftnlo              = -3.91562705e-03 [4.70598563e-07]
ggnnlo/eftn2lo            = 1.01345314e+00 [1.37484622e-04]
qgnnlo/eftn2lo            = -1.47693743e-02 [3.09103739e-06]
ggn3lo/eftn3lo            = 1.02028814e+00 [1.62337822e-04]
qgn3lo/eftn3lo            = -2.21802249e-02 [7.07916361e-06]
R_LO*gg channel           = 1.34981648e-01 [1.45651632e-05]
R_LO*qg channel           = -2.93438998e-03 [8.71440526e-07]
R_LO*qqbar channel        = 1.21837629e-04 [9.27382021e-07]
R_LO*qq channel           = 4.26392848e-05 [3.80043408e-07]
R_LO*q1q2 channel         = 8.58408484e-05 [5.03952531e-06]
ew rescaled as^2          = 0.00000000e+00 [0.00000000e+00]
ew rescaled as^3          = 0.00000000e+00 [0.00000000e+00]
ew rescaled as^4          = 0.00000000e+00 [0.00000000e+00]
ew rescaled as^5          = 0.00000000e+00 [0.00000000e+00]
mixed EW-QCD              = 0.00000000e+00 [0.00000000e+00]
ew rescaled               = 0.00000000e+00 [0.00000000e+00]
hard ratio from eft       = 2.31672225e-01 [3.41596134e-05]
WC                        = 1.09591199e+00 [0.00000000e+00]
WC^2                      = 1.20102308e+00 [0.00000000e+00]
WC^2_trunc                = 1.20057770e+00 [0.00000000e+00]
n2                        = 9.75781602e-02 [9.70965251e-06]
n3                        = 6.69758148e-02 [7.09558813e-06]
n4                        = 1.86178939e-02 [1.66948256e-05]
n5                        = 1.66348616e-03 [1.53630690e-05]
n                         = 1.84835355e-01 [2.56781194e-05]
sigma factorized          = 2.21991527e-01 [2.60391025e-05]
exact LO t+b+c            = 5.86421366e-02 [5.83526855e-06]
exact NLO t+b+c           = 1.11854190e-01 [1.03614036e-05]
exact LO t                = 5.85259906e-02 [5.82371128e-06]
exact NLO t               = 1.11537488e-01 [1.03309319e-05]
NLO quark mass effects    = 3.47029758e-03 [1.26432178e-05]
NLO quark mass effects / eft % = 3.20185731e+00
Higgs XS                  = 1.35767873e-01 [1.99788796e-05]
delta EW                  = 1.35767873e-03 [1.99788796e-07]
delta EW %                = 1.00000000e+00 [0.00000000e+00]
R_LO*eftnnlo (with NLO PDF) = 1.27700016e-01 [1.23648975e-05]
delta PDF-TH %            = 9.67601241e-02 [6.83492476e-03]
rEFT(low)                 = 1.37962996e-01 [1.66701395e-05]
rEFT(high)                = 1.14999605e-01 [1.53210061e-05]
delta(scale)+             = 5.66542059e-03
delta(scale)-             = -1.72979704e-02
delta(scale)+(%)          = 4.28233137e+00
delta(scale)-(%)          = -1.30750471e+01
delta(scale)+ pure eft    = 1.01884593e-03
delta(scale)- pure eft    = -6.52741663e-03
delta(scale)+(%) pure eft = 4.61905121e-01
delta(scale)-(%) pure eft = -2.95927684e+00
R_LO*eftn3lo_central      = 1.32304349e-01 [1.33388267e-05]
deltaPDF+                 = 5.20145123e-03
deltaPDF-                 = 5.20145123e-03
deltaPDFsymm              = 5.20145123e-03
deltaPDF+(%)              = 3.93142876e+00
deltaPDF-(%)              = 3.93142876e+00
rEFT(as+)                 = 1.35794908e-01 [1.58254009e-05]
rEFT(as-)                 = 1.29374671e-01 [1.48732810e-05]
delta(as)+                = 3.49733261e-03
delta(as)-                = -2.92290502e-03
delta(as)+(%)             = 2.64353492e+00
delta(as)-(%)             = -2.20934133e+00
Theory Uncertainty  +     = 7.30307812e-03 [9.34165466e-06]
Theory Uncertainty  -     = -1.92407613e-02 [9.70196971e-06]
Theory Uncertainty % +    = 5.37909149e+00 [6.83492476e-03]
Theory Uncertainty % -    = -1.41718072e+01 [6.83492476e-03]
delta(PDF+a_s) +          = 6.43207503e-03 [9.46510021e-07]
delta(PDF+a_s) -          = -6.12271281e-03 [-9.00985918e-07]
delta(PDF+a_s) + %        = 4.73755306e+00 [0.00000000e+00]
delta(PDF+a_s) - %        = -4.50969192e+00 [0.00000000e+00]
Total Uncertainty +       = 1.37351532e-02 [9.38948310e-06]
Total Uncertainty -       = -2.53634741e-02 [9.74371551e-06]
Total Uncertainty + %     = 1.01166446e+01 [6.83492476e-03]
Total Uncertainty - %     = -1.86814992e+01 [6.83492476e-03]

note: delta_tbc was requested (an estimate of the uncertainty due to the lack of light quark mass effects at NNLO) but either the with_mt_expansion is set to false or the qcd perturbative order is less than NNLO. In either case we cannot estimate delta_tbc, so it is set to zero




------------------------------------------------------------------
Results in Mathematica format

<|"mh"->9.50000000e+02,"Etot"->13000,"PDF set"->PDF4LHC15_nnlo_100,"PDF member"->0,"mur"->4.75000000e+02,"muf"->4.75000000e+02,"as_at_mz"->1.18002301e-01,"as_at_mur"->9.45403139e-02,"mt_used"->1.50579800e+02,"mb_used"->2.52725223e+00,"mc_used"->5.58296883e-01,"eftlo"->9.75781602e-02,"eftnlo"->1.80704345e-01,"eftnnlo"->2.13322205e-01,"eftn3lo"->2.20574721e-01,"R_LO"->5.99785756e-01,"R_LO*eftlo"->5.85259906e-02,"R_LO*eftnlo"->1.08383892e-01,"R_LO*eftnnlo"->1.27947620e-01,"R_LO*eftn3lo"->1.32297576e-01,"ggnlo/eftnlo"->1.00322270e+00,"qgnlo/eftnlo"->-3.91562705e-03,"ggnnlo/eftn2lo"->1.01345314e+00,"qgnnlo/eftn2lo"->-1.47693743e-02,"ggn3lo/eftn3lo"->1.02028814e+00,"qgn3lo/eftn3lo"->-2.21802249e-02,"R_LO*gg channel"->1.34981648e-01,"R_LO*qg channel"->-2.93438998e-03,"R_LO*qqbar channel"->1.21837629e-04,"R_LO*qq channel"->4.26392848e-05,"R_LO*q1q2 channel"->8.58408484e-05,"ew rescaled as^2"->0.00000000e+00,"ew rescaled as^3"->0.00000000e+00,"ew rescaled as^4"->0.00000000e+00,"ew rescaled as^5"->0.00000000e+00,"mixed EW-QCD"->0.00000000e+00,"ew rescaled"->0.00000000e+00,"hard ratio from eft"->2.31672225e-01,"WC"->1.09591199e+00,"WC^2"->1.20102308e+00,"WC^2_trunc"->1.20057770e+00,"n2"->9.75781602e-02,"n3"->6.69758148e-02,"n4"->1.86178939e-02,"n5"->1.66348616e-03,"n"->1.84835355e-01,"sigma factorized"->2.21991527e-01,"exact LO t+b+c"->5.86421366e-02,"exact NLO t+b+c"->1.11854190e-01,"exact LO t"->5.85259906e-02,"exact NLO t"->1.11537488e-01,"NLO quark mass effects"->3.47029758e-03,"NLO quark mass effects / eft %"->3.20185731e+00,"delta sigma t NLO"->5.30114979e-02,"delta sigma t+b+c NLO"->5.32120534e-02,"delta tbc ratio"->3.78324599e-03,"Higgs XS"->1.35767873e-01,"delta EW"->1.35767873e-03,"delta EW %"->1.00000000e+00,"R_LO*eftnnlo (with NLO PDF)"->1.27700016e-01,"delta PDF-TH %"->9.67601241e-02,"rEFT(low)"->1.37962996e-01,"rEFT(high)"->1.14999605e-01,"delta(scale)+"->5.66542059e-03,"delta(scale)-"->-1.72979704e-02,"delta(scale)+(%)"->4.28233137e+00,"delta(scale)-(%)"->-1.30750471e+01,"delta(scale)+ pure eft"->1.01884593e-03,"delta(scale)- pure eft"->-6.52741663e-03,"delta(scale)+(%) pure eft"->4.61905121e-01,"delta(scale)-(%) pure eft"->-2.95927684e+00,"R_LO*eftn3lo_central"->1.32304349e-01,"deltaPDF+"->5.20145123e-03,"deltaPDF-"->5.20145123e-03,"deltaPDFsymm"->5.20145123e-03,"deltaPDF+(%)"->3.93142876e+00,"deltaPDF-(%)"->3.93142876e+00,"rEFT(as+)"->1.35794908e-01,"rEFT(as-)"->1.29374671e-01,"delta(as)+"->3.49733261e-03,"delta(as)-"->-2.92290502e-03,"delta(as)+(%)"->2.64353492e+00,"delta(as)-(%)"->-2.20934133e+00,"Theory Uncertainty  +"->7.30307812e-03,"Theory Uncertainty  -"->-1.92407613e-02,"Theory Uncertainty % +"->5.37909149e+00,"Theory Uncertainty % -"->-1.41718072e+01,"delta(PDF+a_s) +"->6.43207503e-03,"delta(PDF+a_s) -"->-6.12271281e-03,"delta(PDF+a_s) + %"->4.73755306e+00,"delta(PDF+a_s) - %"->-4.50969192e+00,"Total Uncertainty +"->1.37351532e-02,"Total Uncertainty -"->-2.53634741e-02,"Total Uncertainty + %"->1.01166446e+01,"Total Uncertainty - %"->-1.86814992e+01|>


------------------------------------------------------------------
User defined options (including command line ones)
Etot                        = 13000.0               #    COM energy of the collider in GeV
m_higgs                     = 950                   #    higgs mass in GeV
mur                         = 475.                  #    mur
muf                         = 475.                  #    muf
pdf_member                  = 0                     #    pdf member id (the range depends on the pdf set)
pdf_set                     = PDF4LHC15_nnlo_100    #    choose a specific pdf set name (LHAPDF6 list at lhapdf.hepforge.org/pdfsets.html). This set will be used irrespectively of order.
pdf_set_for_nlo             = PDF4LHC15_nlo_100     #    pdf set used when computing PDF-TH error.
with_eft                    = true                  #    compute the cross section in the EFT approximation
with_exact_qcd_corrections  = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_ew_corrections         = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_mt_expansion           = false                 #    include NNLO 1/mt terms
with_delta_pdf_th           = true                  #    compute PDF-TH uncertainty
with_scale_variation        = true                  #    estimate scale variation (mur and muf should be at mh/2)
with_indiv_mass_effects     = false                 #    compute separately light quark contributions
with_pdf_error              = true                  #    whether or not to compute error due to pdfs
with_a_s_error              = true                  #    compute a_s uncertainty
with_resummation            = false                 #    include threshold resummation
resummation_log_order       = 3                     #    0:LL, 1:NLL, 2:NNLL, 3:N3LL
resummation_matching_order  = 3                     #    0:L0, 1:NL0, 2:NNL0, 3:N3L0
resummation_type            = log                   #    variant of threshold resummation, i.e. log:classical, psi, AP2log, AP2psi 
with_scet                   = false                 #    include scet resummation
verbose                     = minimal               #    level of verbosity: minimal or medium. Medium shows channel breakdown EFT cross section.
input_filename              = pheno.card            #    filename to use as runcard
output_filename             = mHRuns/Run_mh950.txt  #    filename to write output
help                        = false                 #    print all options and help messages per option.
make_runcard                = false                 #    create default runcard file as default_card.
make_pheno_card             = false                 #    create pheno runcard file as pheno_card.
write_documentation         = false                 #    print the help message in a TeX form.
with_eft_channel_info       = false                 #    print eft cross section per channel per order
with_resummation_info       = false                 #    info from resummation: true, false
mt_msbar                    = 162.7                 #    MSbar top mass
mt_msbar_ref_scale          = 162.7                 #    reference scale for the top mass in MSbar
mt_on_shell                 = 172.5                 #    On Shell top mass
mb_msbar                    = 4.18                  #    MSbar bottom mass
mb_msbar_ref_scale          = 4.18                  #    reference scale for the bottom mass in MSbar
mb_on_shell                 = 4.92                  #    On Shell bottom mass
mc_msbar                    = 0.986                 #    MSbar charm mass
mc_msbar_ref_scale          = 3.0                   #    reference scale for the charm mass in MSbar
mc_on_shell                 = 1.67                  #    On Shell charm mass
top_scheme                  = msbar                 #    msbar or on-shell
bottom_scheme               = msbar                 #    msbar or on-shell
charm_scheme                = msbar                 #    msbar or on-shell
y_top                       = 1.0                   #    factor multiplying the Yt. Set to zero to remove the top quark
y_bot                       = 1.0                   #    factor multiplying the Yb. Set to zero to remove the bottom quark
y_charm                     = 1.0                   #    factor multiplying the Yc. Set to zero to remove the charm quark
gamma_top                   = 0.0                   #    width of top quark
gamma_bot                   = 0.0                   #    width of bottom quark
gamma_charm                 = 0.0                   #    width of charm quark
qcd_perturbative_order      = N3LO                  #    LO, NLO, NNLO, N3LO : ihixs will compute up to this order in a_s
with_fixed_as_at_mz         = 0.0                   #    set the value of a_s(mZ) by hand. Beware: this might not be compatible with your pdf choice.
qcd_order_evol              = 3                     #    used for a_s and quark mass evolution 0:L0, 1:NL0, 2:NNL0, 3:N3L0
with_lower_ord_scale_var    = false                 #    also compute scale variation for lower than the current order
epsrel                      = 0.0001                #    cuba argument: target relative error
epsabs                      = 0.0                   #    cuba argument: target absolute error
mineval                     = 50000                 #    cuba argument: minimum points to be evaluated
maxeval                     = 50000000              #    cuba argument: maximum points to be evaluated
nstart                      = 10000                 #    cuba argument: number of points for first iteration
nincrease                   = 1000                  #    cuba argument: number of points for step increase
cuba_verbose                = 0                     #    cuba argument: verbosity level: 0=silent, 2=iterations printed out


------------------------------------------------------------------
Runcard used (options may have been overwritten by command line ones in runtime)
runcard_name="pheno.card"
