ihixs results 
Result
mh                        = 4.05000000e+03
Etot                      = 13000
PDF set                   = PDF4LHC15_nnlo_100
PDF member                = 0
mur                       = 2.02500000e+03
muf                       = 2.02500000e+03
as_at_mz                  = 1.18002301e-01
as_at_mur                 = 8.05731238e-02
mt_used                   = 1.37781981e+02
mb_used                   = 2.31246028e+00
mc_used                   = 5.10847097e-01
eftlo                     = 4.59906508e-05 [4.46343094e-09]
eftnlo                    = 8.24903410e-05 [6.61201923e-09]
eftnnlo                   = 9.76309002e-05 [1.07924502e-08]
eftn3lo                   = 1.01992635e-04 [1.52488316e-08]
R_LO                      = 8.40553067e-03
R_LO*eftlo                = 3.86575825e-07 [3.75175056e-11]
R_LO*eftnlo               = 6.93375090e-07 [5.55775304e-11]
R_LO*eftnnlo              = 8.20639526e-07 [9.07162710e-11]
R_LO*eftn3lo              = 8.57302221e-07 [1.28174521e-10]
ggnlo/eftnlo              = 1.08932185e+00 [1.18180605e-04]
qgnlo/eftnlo              = -8.95019226e-02 [1.14305084e-05]
ggnnlo/eftn2lo            = 1.14903234e+00 [1.66699450e-04]
qgnnlo/eftn2lo            = -1.52163564e-01 [2.46236749e-05]
ggn3lo/eftn3lo            = 1.17911964e+00 [2.23929805e-04]
qgn3lo/eftn3lo            = -1.85142354e-01 [4.29347747e-05]
R_LO*gg channel           = 1.01086188e-06 [1.18378155e-10]
R_LO*qg channel           = -1.58722951e-07 [2.81371003e-11]
R_LO*qqbar channel        = 4.00694732e-10 [3.48485409e-12]
R_LO*qq channel           = 2.56336282e-09 [2.44176218e-11]
R_LO*q1q2 channel         = 2.19923023e-09 [3.18631376e-11]
ew rescaled as^2          = 0.00000000e+00 [0.00000000e+00]
ew rescaled as^3          = 0.00000000e+00 [0.00000000e+00]
ew rescaled as^4          = 0.00000000e+00 [0.00000000e+00]
ew rescaled as^5          = 0.00000000e+00 [0.00000000e+00]
mixed EW-QCD              = 0.00000000e+00 [0.00000000e+00]
ew rescaled               = 0.00000000e+00 [0.00000000e+00]
hard ratio from eft       = 6.34700629e-02 [2.60982559e-05]
WC                        = 1.08850121e+00 [0.00000000e+00]
WC^2                      = 1.18483488e+00 [0.00000000e+00]
WC^2_trunc                = 1.18397130e+00 [0.00000000e+00]
n2                        = 4.59906508e-05 [4.46343094e-09]
n3                        = 3.00122619e-05 [4.85781617e-09]
n4                        = 9.37774992e-06 [8.51557081e-09]
n5                        = 1.59669650e-06 [1.07386123e-08]
n                         = 8.69773590e-05 [1.52103035e-08]
sigma factorized          = 1.03053809e-04 [1.53699151e-08]
exact LO t+b+c            = 3.87458831e-07 [3.76032020e-11]
exact NLO t+b+c           = 8.10998154e-07 [1.20740850e-10]
exact LO t                = 3.86575825e-07 [3.75175056e-11]
exact NLO t               = 8.08719728e-07 [1.19487484e-10]
NLO quark mass effects    = 1.17623063e-07 [1.32918075e-10]
NLO quark mass effects / eft % = 1.69638432e+01
Higgs XS                  = 9.74925285e-07 [1.84650812e-10]
delta EW                  = 9.74925285e-09 [1.84650812e-12]
delta EW %                = 1.00000000e+00 [0.00000000e+00]
R_LO*eftnnlo (with NLO PDF) = 7.21496486e-07 [8.19692397e-11]
delta PDF-TH %            = 6.04059620e+00 [7.47915863e-03]
rEFT(low)                 = 8.96338874e-07 [1.52651070e-10]
rEFT(high)                = 7.36792936e-07 [1.26111285e-10]
delta(scale)+             = 3.90366528e-08
delta(scale)-             = -1.20509285e-07
delta(scale)+(%)          = 4.55342956e+00
delta(scale)-(%)          = -1.40568031e+01
delta(scale)+ pure eft    = 6.76908820e-07
delta(scale)- pure eft    = -3.92378732e-06
delta(scale)+(%) pure eft = 6.63684020e-01
delta(scale)-(%) pure eft = -3.84712809e+00
R_LO*eftn3lo_central      = 8.57262464e-07 [1.83711837e-10]
deltaPDF+                 = 2.19153244e-07
deltaPDF-                 = 2.19153244e-07
deltaPDFsymm              = 2.19153244e-07
deltaPDF+(%)              = 2.55643112e+01
deltaPDF-(%)              = 2.55643112e+01
rEFT(as+)                 = 9.43342922e-07 [1.38992736e-10]
rEFT(as-)                 = 8.02199665e-07 [1.19746613e-10]
delta(as)+                = 8.60407006e-08
delta(as)-                = -5.51025568e-08
delta(as)+(%)             = 1.00362158e+01
delta(as)-(%)             = -6.42743660e+00
Theory Uncertainty  +     = 1.13033089e-07 [7.59940507e-11]
Theory Uncertainty  -     = -2.05683880e-07 [8.26703324e-11]
Theory Uncertainty % +    = 1.15940258e+01 [7.47915863e-03]
Theory Uncertainty % -    = -2.10973993e+01 [7.47915863e-03]
delta(PDF+a_s) +          = 2.67751410e-07 [5.07121071e-11]
delta(PDF+a_s) -          = -2.56989630e-07 [-4.86738263e-11]
delta(PDF+a_s) + %        = 2.74637877e+01 [0.00000000e+00]
delta(PDF+a_s) - %        = -2.63599307e+01 [0.00000000e+00]
Total Uncertainty +       = 3.80784499e-07 [9.13608973e-11]
Total Uncertainty -       = -4.62673510e-07 [9.59350052e-11]
Total Uncertainty + %     = 3.90578134e+01 [7.47915863e-03]
Total Uncertainty - %     = -4.74573300e+01 [7.47915863e-03]

note: delta_tbc was requested (an estimate of the uncertainty due to the lack of light quark mass effects at NNLO) but either the with_mt_expansion is set to false or the qcd perturbative order is less than NNLO. In either case we cannot estimate delta_tbc, so it is set to zero




------------------------------------------------------------------
Results in Mathematica format

<|"mh"->4.05000000e+03,"Etot"->13000,"PDF set"->PDF4LHC15_nnlo_100,"PDF member"->0,"mur"->2.02500000e+03,"muf"->2.02500000e+03,"as_at_mz"->1.18002301e-01,"as_at_mur"->8.05731238e-02,"mt_used"->1.37781981e+02,"mb_used"->2.31246028e+00,"mc_used"->5.10847097e-01,"eftlo"->4.59906508e-05,"eftnlo"->8.24903410e-05,"eftnnlo"->9.76309002e-05,"eftn3lo"->1.01992635e-04,"R_LO"->8.40553067e-03,"R_LO*eftlo"->3.86575825e-07,"R_LO*eftnlo"->6.93375090e-07,"R_LO*eftnnlo"->8.20639526e-07,"R_LO*eftn3lo"->8.57302221e-07,"ggnlo/eftnlo"->1.08932185e+00,"qgnlo/eftnlo"->-8.95019226e-02,"ggnnlo/eftn2lo"->1.14903234e+00,"qgnnlo/eftn2lo"->-1.52163564e-01,"ggn3lo/eftn3lo"->1.17911964e+00,"qgn3lo/eftn3lo"->-1.85142354e-01,"R_LO*gg channel"->1.01086188e-06,"R_LO*qg channel"->-1.58722951e-07,"R_LO*qqbar channel"->4.00694732e-10,"R_LO*qq channel"->2.56336282e-09,"R_LO*q1q2 channel"->2.19923023e-09,"ew rescaled as^2"->0.00000000e+00,"ew rescaled as^3"->0.00000000e+00,"ew rescaled as^4"->0.00000000e+00,"ew rescaled as^5"->0.00000000e+00,"mixed EW-QCD"->0.00000000e+00,"ew rescaled"->0.00000000e+00,"hard ratio from eft"->6.34700629e-02,"WC"->1.08850121e+00,"WC^2"->1.18483488e+00,"WC^2_trunc"->1.18397130e+00,"n2"->4.59906508e-05,"n3"->3.00122619e-05,"n4"->9.37774992e-06,"n5"->1.59669650e-06,"n"->8.69773590e-05,"sigma factorized"->1.03053809e-04,"exact LO t+b+c"->3.87458831e-07,"exact NLO t+b+c"->8.10998154e-07,"exact LO t"->3.86575825e-07,"exact NLO t"->8.08719728e-07,"NLO quark mass effects"->1.17623063e-07,"NLO quark mass effects / eft %"->1.69638432e+01,"delta sigma t NLO"->4.22143903e-07,"delta sigma t+b+c NLO"->4.23539323e-07,"delta tbc ratio"->3.30555582e-03,"Higgs XS"->9.74925285e-07,"delta EW"->9.74925285e-09,"delta EW %"->1.00000000e+00,"R_LO*eftnnlo (with NLO PDF)"->7.21496486e-07,"delta PDF-TH %"->6.04059620e+00,"rEFT(low)"->8.96338874e-07,"rEFT(high)"->7.36792936e-07,"delta(scale)+"->3.90366528e-08,"delta(scale)-"->-1.20509285e-07,"delta(scale)+(%)"->4.55342956e+00,"delta(scale)-(%)"->-1.40568031e+01,"delta(scale)+ pure eft"->6.76908820e-07,"delta(scale)- pure eft"->-3.92378732e-06,"delta(scale)+(%) pure eft"->6.63684020e-01,"delta(scale)-(%) pure eft"->-3.84712809e+00,"R_LO*eftn3lo_central"->8.57262464e-07,"deltaPDF+"->2.19153244e-07,"deltaPDF-"->2.19153244e-07,"deltaPDFsymm"->2.19153244e-07,"deltaPDF+(%)"->2.55643112e+01,"deltaPDF-(%)"->2.55643112e+01,"rEFT(as+)"->9.43342922e-07,"rEFT(as-)"->8.02199665e-07,"delta(as)+"->8.60407006e-08,"delta(as)-"->-5.51025568e-08,"delta(as)+(%)"->1.00362158e+01,"delta(as)-(%)"->-6.42743660e+00,"Theory Uncertainty  +"->1.13033089e-07,"Theory Uncertainty  -"->-2.05683880e-07,"Theory Uncertainty % +"->1.15940258e+01,"Theory Uncertainty % -"->-2.10973993e+01,"delta(PDF+a_s) +"->2.67751410e-07,"delta(PDF+a_s) -"->-2.56989630e-07,"delta(PDF+a_s) + %"->2.74637877e+01,"delta(PDF+a_s) - %"->-2.63599307e+01,"Total Uncertainty +"->3.80784499e-07,"Total Uncertainty -"->-4.62673510e-07,"Total Uncertainty + %"->3.90578134e+01,"Total Uncertainty - %"->-4.74573300e+01|>


------------------------------------------------------------------
User defined options (including command line ones)
Etot                        = 13000.0               #    COM energy of the collider in GeV
m_higgs                     = 4050                  #    higgs mass in GeV
mur                         = 2025.                 #    mur
muf                         = 2025.                 #    muf
pdf_member                  = 0                     #    pdf member id (the range depends on the pdf set)
pdf_set                     = PDF4LHC15_nnlo_100    #    choose a specific pdf set name (LHAPDF6 list at lhapdf.hepforge.org/pdfsets.html). This set will be used irrespectively of order.
pdf_set_for_nlo             = PDF4LHC15_nlo_100     #    pdf set used when computing PDF-TH error.
with_eft                    = true                  #    compute the cross section in the EFT approximation
with_exact_qcd_corrections  = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_ew_corrections         = true                  #    true to include the exact quark mass effects at NLO, false to omit them
with_mt_expansion           = false                 #    include NNLO 1/mt terms
with_delta_pdf_th           = true                  #    compute PDF-TH uncertainty
with_scale_variation        = true                  #    estimate scale variation (mur and muf should be at mh/2)
with_indiv_mass_effects     = false                 #    compute separately light quark contributions
with_pdf_error              = true                  #    whether or not to compute error due to pdfs
with_a_s_error              = true                  #    compute a_s uncertainty
with_resummation            = false                 #    include threshold resummation
resummation_log_order       = 3                     #    0:LL, 1:NLL, 2:NNLL, 3:N3LL
resummation_matching_order  = 3                     #    0:L0, 1:NL0, 2:NNL0, 3:N3L0
resummation_type            = log                   #    variant of threshold resummation, i.e. log:classical, psi, AP2log, AP2psi 
with_scet                   = false                 #    include scet resummation
verbose                     = minimal               #    level of verbosity: minimal or medium. Medium shows channel breakdown EFT cross section.
input_filename              = pheno.card            #    filename to use as runcard
output_filename             = mHRuns/Run_mh4050.txt #    filename to write output
help                        = false                 #    print all options and help messages per option.
make_runcard                = false                 #    create default runcard file as default_card.
make_pheno_card             = false                 #    create pheno runcard file as pheno_card.
write_documentation         = false                 #    print the help message in a TeX form.
with_eft_channel_info       = false                 #    print eft cross section per channel per order
with_resummation_info       = false                 #    info from resummation: true, false
mt_msbar                    = 162.7                 #    MSbar top mass
mt_msbar_ref_scale          = 162.7                 #    reference scale for the top mass in MSbar
mt_on_shell                 = 172.5                 #    On Shell top mass
mb_msbar                    = 4.18                  #    MSbar bottom mass
mb_msbar_ref_scale          = 4.18                  #    reference scale for the bottom mass in MSbar
mb_on_shell                 = 4.92                  #    On Shell bottom mass
mc_msbar                    = 0.986                 #    MSbar charm mass
mc_msbar_ref_scale          = 3.0                   #    reference scale for the charm mass in MSbar
mc_on_shell                 = 1.67                  #    On Shell charm mass
top_scheme                  = msbar                 #    msbar or on-shell
bottom_scheme               = msbar                 #    msbar or on-shell
charm_scheme                = msbar                 #    msbar or on-shell
y_top                       = 1.0                   #    factor multiplying the Yt. Set to zero to remove the top quark
y_bot                       = 1.0                   #    factor multiplying the Yb. Set to zero to remove the bottom quark
y_charm                     = 1.0                   #    factor multiplying the Yc. Set to zero to remove the charm quark
gamma_top                   = 0.0                   #    width of top quark
gamma_bot                   = 0.0                   #    width of bottom quark
gamma_charm                 = 0.0                   #    width of charm quark
qcd_perturbative_order      = N3LO                  #    LO, NLO, NNLO, N3LO : ihixs will compute up to this order in a_s
with_fixed_as_at_mz         = 0.0                   #    set the value of a_s(mZ) by hand. Beware: this might not be compatible with your pdf choice.
qcd_order_evol              = 3                     #    used for a_s and quark mass evolution 0:L0, 1:NL0, 2:NNL0, 3:N3L0
with_lower_ord_scale_var    = false                 #    also compute scale variation for lower than the current order
epsrel                      = 0.0001                #    cuba argument: target relative error
epsabs                      = 0.0                   #    cuba argument: target absolute error
mineval                     = 50000                 #    cuba argument: minimum points to be evaluated
maxeval                     = 50000000              #    cuba argument: maximum points to be evaluated
nstart                      = 10000                 #    cuba argument: number of points for first iteration
nincrease                   = 1000                  #    cuba argument: number of points for step increase
cuba_verbose                = 0                     #    cuba argument: verbosity level: 0=silent, 2=iterations printed out


------------------------------------------------------------------
Runcard used (options may have been overwritten by command line ones in runtime)
runcard_name="pheno.card"
